/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.enactor.executor;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.emf.common.util.EList;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.executor.Executor;
import de.ugoe.cs.rwm.docci.executor.ExecutorFactory;

public class DatalinkExecutor implements Runnable {
	protected static final Logger LOG = Logger.getLogger(TaskExecutor.class.getName());
	private Connector conn;
	private EList<Link> dLinks;

	public DatalinkExecutor(EList<Link> dLinks, Connector conn) {
		this.conn = conn;
		this.dLinks = dLinks;
	}

	public void executeTasks() {
		List<DatalinkExecutorSlave> slaves = new ArrayList<DatalinkExecutorSlave>();
		List<Thread> threads = new ArrayList<Thread>();
		slaves.addAll(createSubtasks(dLinks));
		LOG.debug("Slaves created: " + slaves.size());
		for (DatalinkExecutorSlave slave : slaves) {
			Thread thread = new Thread(slave);

			threads.add(thread);
			thread.start();
		}
		for (Thread t : threads) {
			LOG.debug("Joining Datalink execution Threads");
			try {
				t.join();
				LOG.info("Thread: " + t + "joined");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		LOG.debug("Datalink execution threads joined");

		// exec.executeOperation("POST", task, start);
	}

	private List<DatalinkExecutorSlave> createSubtasks(EList<Link> dLinks) {
		List<DatalinkExecutorSlave> slaves = new ArrayList<DatalinkExecutorSlave>();
		for (Link dLink : dLinks) {
			Executor exec = ExecutorFactory.getExecutor("Mart", conn);
			DatalinkExecutorSlave slave = new DatalinkExecutorSlave(dLink, exec);
			slaves.add(slave);
		}
		return slaves;
	}

	@Override
	public void run() {
		executeTasks();

	}
}
