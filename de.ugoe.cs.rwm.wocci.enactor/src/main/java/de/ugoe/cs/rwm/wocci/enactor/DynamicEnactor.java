/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.enactor;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.wocci.enactor.executor.DatalinkExecutor;
import de.ugoe.cs.rwm.wocci.enactor.executor.TaskExecutor;
import de.ugoe.cs.rwm.wocci.utility.WorkflowUtility;

import java.util.List;

public class DynamicEnactor extends AbsEnactor {
	public DynamicEnactor(Connector conn) {
		this.conn = conn;
	}

	@Override
	public void enactWorkflow(Resource runtimeModel) {
		EList<org.eclipse.cmf.occi.core.Resource> tasks = getTasksForExecution(runtimeModel);
		logTaskExecution(tasks);
		TaskExecutor exec = new TaskExecutor(tasks, conn, jobHistoryPath);
		DatalinkExecutor dExec = new DatalinkExecutor(WorkflowUtility.getDatalinskForExecution(runtimeModel), conn);
		Thread dExecThread = new Thread(dExec);
		dExecThread.start();
		Thread execThread = new Thread(exec);
		execThread.start();

		CachedResourceSet.getCache().clear();
	}
}
