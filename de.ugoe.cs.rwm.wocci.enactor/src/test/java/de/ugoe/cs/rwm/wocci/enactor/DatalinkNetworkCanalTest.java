/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/
package de.ugoe.cs.rwm.wocci.enactor;

import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.docci.MartDeployer;
import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.connector.LocalhostConnector;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.TransformatorFactory;
import de.ugoe.cs.rwm.wocci.utility.TestExtensionRegistry;

public class DatalinkNetworkCanalTest {
	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestExtensionRegistry.extensionRegistrySetup();
	}

	@Before
	public void deprovisionEverything() {
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/Empty.occic"));
		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);
		deployer.deploy(occiPath);
	}

	@Test
	public void enactNetworkCanal() throws EolRuntimeException {
		System.out.println(OcciRegistry.getInstance().getRegisteredExtensions());
		Logger.getRootLogger().setLevel(Level.FATAL);
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/datalinkNetworkCanalTest.occic"));
		Transformator trans = TransformatorFactory.getTransformator("OCCI2OCCI");
		trans.transform(occiPath, occiPath);
		Resource workflowModel = ModelUtility.loadOCCIintoEMFResource(occiPath);
		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);
		deployer.deploy(workflowModel);

		Resource runtimeModel = de.ugoe.cs.rwm.docci.ModelUtility
				.loadOCCIintoEMFResource(Paths.get(System.getProperty("user.home") + "/.rwm/runtime.occic"));

		DynamicEnactor enac = new DynamicEnactor(conn);
		enac.enactWorkflow(runtimeModel);

		assertTrue(true);
	}
}
