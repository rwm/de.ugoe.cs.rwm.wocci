/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/
package de.ugoe.cs.rwm.wocci.enactor;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.eclipse.cmf.occi.core.Configuration;

import de.ugoe.cs.rwm.cocci.Comparator;
import de.ugoe.cs.rwm.docci.Deployer;
import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.deprovisioner.Deprovisioner;
import de.ugoe.cs.rwm.docci.executor.Executor;
import de.ugoe.cs.rwm.docci.extractor.Extractor;
import de.ugoe.cs.rwm.docci.provisioner.Provisioner;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.wocci.enactor.executor.DatalinkExecutor;
import de.ugoe.cs.rwm.wocci.enactor.executor.TaskExecutor;
import workflow.Status;
import workflow.Task;

public class TestUtility {

	public static void loggerSetup() {
		File log4jfile = new File(ModelUtility.getPathToResource("log4j.properties"));
		PropertyConfigurator.configure(log4jfile.getAbsolutePath());
		Logger.getLogger(Executor.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(Extractor.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Provisioner.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Comparator.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(Deprovisioner.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Connector.class.getName()).setLevel(Level.OFF);
		// Logger.getLogger(ElementAdapter.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(Deployer.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(DynamicEnactor.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(DatalinkExecutor.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(TaskExecutor.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.INFO);
	}

	public static boolean isWorkflowFinished(Connector conn) {
		boolean everyTaskFinished = true;

		Path runtimeOCCI = Paths.get(System.getProperty("user.home") + "/.rwm/runtime.occic");
		conn.loadRuntimeModel(runtimeOCCI);
		org.eclipse.emf.ecore.resource.Resource runtimeModel = ModelUtility.loadOCCIintoEMFResource(runtimeOCCI);
		Configuration config = (Configuration) runtimeModel.getContents().get(0);
		for (org.eclipse.cmf.occi.core.Resource res : config.getResources()) {
			if (res instanceof Task) {
				Task task = (Task) res;
				if ((task.getWorkflowTaskState().getValue() == Status.FINISHED.getValue()) == false
						&& task.getWorkflowTaskState().getValue() == Status.SKIPPED.getValue() == false) {
					everyTaskFinished = false;
					System.out.println("Task not finished: " + task);
				}
			}
		}

		if (everyTaskFinished) {
			return true;
		}

		return false;
	}
}
