/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/
package de.ugoe.cs.rwm.wocci.enactor;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.Mixin;
import org.eclipse.cmf.occi.core.Resource;
import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.TransformatorFactory;
import de.ugoe.cs.rwm.wocci.utility.TestExtensionRegistry;

public class UtilityTest {

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestExtensionRegistry.extensionRegistrySetup();
	}

	@Test
	public void resolveProxies() {

		System.out.println(OcciRegistry.getInstance().getRegisteredExtensions());

		Path newOCCI = Paths.get(de.ugoe.cs.rwm.docci.ModelUtility.getPathToResource("occi/MLS_Hadoop.occic"));
		org.eclipse.emf.ecore.resource.Resource ress = de.ugoe.cs.rwm.docci.ModelUtility
				.loadOCCIintoEMFResource(newOCCI);
		ResourceSet resSet = ress.getResourceSet();

		EList<EObject> newModel = de.ugoe.cs.rwm.docci.ModelUtility.loadOCCI(newOCCI);
		for (Resource res : de.ugoe.cs.rwm.docci.ModelUtility.getResources(newModel)) {
			System.out.println("Resource Title: " + res.getTitle());
			System.out.println("      Kind: " + res.getKind());
			for (Link link : res.getLinks()) {
				System.out.println("      Link: " + link.getTitle());
				System.out.println("      " + "      Kind:" + link.getKind());
				System.out.println("      " + "      Target:" + link.getTarget());
			}
			for (Mixin mix : res.getMixins()) {
				System.out.println("      " + mix);
				System.out.println("      " + mix.getScheme());
			}
		}
	}

	@Test
	public void migrationTest() throws EolRuntimeException {
		Path newOCCI = Paths.get(de.ugoe.cs.rwm.docci.ModelUtility.getPathToResource("occi/WaaSWithAppMig.occic"));
		EList<EObject> newModel = de.ugoe.cs.rwm.docci.ModelUtility.loadOCCI(newOCCI);
		Transformator trans = TransformatorFactory.getTransformator("OCCI2OCCI");
		trans.transform(newOCCI, newOCCI);
		for (Resource res : de.ugoe.cs.rwm.docci.ModelUtility.getResources(newModel)) {
			System.out.println("Resource Title: " + res.getTitle());
			System.out.println("      Kind: " + res.getKind());
			for (Link link : res.getLinks()) {
				System.out.println("      Link: " + link.getTitle());
				System.out.println("      " + "      Kind:" + link.getKind());
				System.out.println("      " + "      Target:" + link.getTarget());
			}
			for (Mixin mix : res.getMixins()) {
				System.out.println("      " + mix);
				System.out.println("      " + mix.getScheme());
			}
		}
	}
}
