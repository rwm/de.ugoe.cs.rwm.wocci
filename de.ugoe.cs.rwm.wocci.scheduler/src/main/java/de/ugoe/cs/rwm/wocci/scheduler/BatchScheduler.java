/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.scheduler;

import org.eclipse.emf.ecore.resource.Resource;

import de.ugoe.cs.rwm.docci.Deployer;
import de.ugoe.cs.rwm.docci.connector.Connector;

public class BatchScheduler extends AbsScheduler {

	public BatchScheduler(Connector conn, Deployer depl) {
		this.conn = conn;
		this.depl = depl;
	}

	@Override
	boolean performScheduling(Resource designTimeModel, Resource runtimeModel) throws ArchitectureSchedulingException {
		performDeploy(designTimeModel);
		return true;
	}

}
