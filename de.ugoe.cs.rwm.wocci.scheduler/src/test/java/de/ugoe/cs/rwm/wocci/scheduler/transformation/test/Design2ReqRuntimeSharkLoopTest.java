/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.scheduler.transformation.test;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.wocci.scheduler.transformation.DESIGN2REQRUNTIMETransformator;
import de.ugoe.cs.rwm.wocci.utility.TestExtensionRegistry;

public class Design2ReqRuntimeSharkLoopTest {
	Path targetOcci = Paths.get(TestUtility.getPathToResource("occi/Target.occic"));
	private static final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	private final PrintStream originalErr = System.err;

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestExtensionRegistry.extensionRegistrySetup();
		TestUtility.loggerSetup();
		System.setErr(new PrintStream(errContent));
	}

	@Before
	public void clearCache() {
		CachedResourceSet.getCache().clear();
	}

	@Test
	public void Shark_VCS() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/SmartSharkLoop.occic"));
		Path runOcci = Paths.get(TestUtility.getPathToResource("occi/Empty2.occic"));

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designOcci, runOcci, targetOcci);

		// clearCache();
	}

	@Test
	public void Shark_Loop_Start() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/SmartSharkForEach.occic"));
		Path runOcci = Paths.get(TestUtility.getPathToResource("occi/Empty2.occic"));

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designOcci, runOcci, targetOcci);

		assertTrue(errContent.toString().contains("org.eclipse.epsilon") == false);
		// clearCache();
	}

	@After
	public void restoreStreams() {
		System.setErr(originalErr);
	}
}
