/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.scheduler.transformation.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.OCCIPackage;
import org.eclipse.cmf.occi.core.Resource;
import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.cmf.occi.docker.Container;
import org.eclipse.cmf.occi.docker.Contains;
import org.eclipse.cmf.occi.docker.DockerPackage;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.wocci.scheduler.transformation.DESIGN2REQRUNTIMETransformator;
import de.ugoe.cs.rwm.wocci.utility.TestExtensionRegistry;

public class Design2ReqRuntimeSharkContainerTest {
	Path targetOcci = Paths.get(TestUtility.getPathToResource("occi/Target.occic"));

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestExtensionRegistry.extensionRegistrySetup();
		TestUtility.loggerSetup();
		DockerPackage.eINSTANCE.eClass();

		OcciRegistry.getInstance().registerExtension("http://occiware.org/occi/docker#",
				OCCIPackage.class.getClassLoader().getResource("model/docker.occie").toString());
	}

	@Before
	public void clearCache() {
		CachedResourceSet.getCache().clear();
	}

	@Test
	public void DockerContainerTransformationTest() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths.get(
				TestUtility.getPathToResource("occi/design2reqruntime/shark/container/shark_preinstalled_all.occic"));
		Path runOcci = Paths.get(TestUtility.getPathToResource("occi/Empty2.occic"));

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designOcci, runOcci, targetOcci);

		Configuration finishedModel = ModelUtility.loadOCCIConfiguration(targetOcci);

		int containerCount = 0;

		for (Resource res : finishedModel.getResources()) {
			for (Link link : res.getLinks()) {
				System.out.println(link.getTarget());
				if (link.getTarget() == null) {
					assertNotNull(link.getTarget());
				}
			}
			if (res instanceof Container) {
				containerCount++;
			}
		}

		assertEquals(2, containerCount);
	}

	@Test
	public void DockerContainerParTransformationTest() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths.get(TestUtility
				.getPathToResource("occi/design2reqruntime/shark/container/shark_preinstalled_all_par.occic"));
		Path runOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/shark/container/ParLoopsCreated.occic"));

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designOcci, runOcci, targetOcci);

		Configuration finishedModel = ModelUtility.loadOCCIConfiguration(targetOcci);

		int containerCount = 0;

		for (Resource res : finishedModel.getResources()) {
			for (Link link : res.getLinks()) {
				System.out.println(link.getTarget());
				if (link.getTarget() == null) {
					assertNotNull(link.getTarget());
				}
			}
			if (res instanceof Container) {
				containerCount++;
				assertEquals(true, hasContainsLink(res));
			}
		}

		System.out.println(targetOcci.toAbsolutePath());

		assertEquals(3, containerCount);
	}

	@Test
	public void DockerContainerParFinishedLoopTest() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths.get(TestUtility
				.getPathToResource("occi/design2reqruntime/shark/container/shark_preinstalled_all_par.occic"));
		Path runOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/shark/container/FinishedParLoops.occic"));

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designOcci, runOcci, targetOcci);

		Configuration finishedModel = ModelUtility.loadOCCIConfiguration(targetOcci);

		int containerCount = 0;

		for (Resource res : finishedModel.getResources()) {
			for (Link link : res.getLinks()) {
				System.out.println(link.getTarget());
				if (link.getTarget() == null) {
					assertNotNull(link.getTarget());
				}
			}
			if (res instanceof Container) {
				containerCount++;
				assertEquals(true, hasContainsLink(res));
			}
		}

		System.out.println(targetOcci.toAbsolutePath());

		assertEquals(2, containerCount);
	}

	private boolean hasContainsLink(Resource res) {
		for (Link rLink : res.getRlinks()) {
			if (rLink instanceof Contains) {
				return true;
			}
		}
		return false;
	}
}
