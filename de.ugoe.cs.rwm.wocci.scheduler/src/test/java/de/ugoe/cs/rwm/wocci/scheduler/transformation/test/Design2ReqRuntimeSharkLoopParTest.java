/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.scheduler.transformation.test;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.cmf.occi.core.MixinBase;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.*;

import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.occi2openstack.OCCI2OPENSTACKTransformator;
import de.ugoe.cs.rwm.wocci.scheduler.transformation.DESIGN2REQRUNTIMETransformator;
import de.ugoe.cs.rwm.wocci.utility.TestExtensionRegistry;
import workflow.Replica;

public class Design2ReqRuntimeSharkLoopParTest {
	Path targetOcci = Paths.get(TestUtility.getPathToResource("occi/Target.occic"));
	private static final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	private final PrintStream originalErr = System.err;
	String manNWid = "urn:uuid:29d78078-fb4c-47aa-a9af-b8aaf3339590";
	String managementNWRuntimeId = "75a4639e-9ce7-4058-b859-8a711b0e2e7b";
	String sshKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6H7Ydi45BTHid4qNppGAi5mzjbnZgt7bi6xLGmZ"
			+ "G9CiLmhMsxOuk3Z05Nn+pmoN98qS0eY8S240PPk5VOlYqBY0vdRAwrZSHHaLdMp6I7ARNrI2KraYduweqz7Z"
			+ "QxPXQfwIeYx2HKQxEF2r+4//Fo4WfgdBkLuulvl/Gw3TUzJNQHvgpaiNo9+PI5CZydHnZbjUkRikS12pT+Cb"
			+ "NKj+0QKeQztbCd41aKxDv5H0DjltVRcpPppv4dmiU/zoCAIngWLO1PPgfYWyze8Z9IoyBT7Qdg30U91TYZBu"
			+ "xzXR5lq7Fh64y/IZ/SjdOdSIvIuDjtmJDULRdLJzrvubrKY+YH Generated-by-Nova";
	String userData = "I2Nsb3VkLWNvbmZpZwoKIyBVcGdyYWRlIHRoZSBpbnN0YW5jZSBvbiBmaXJzdCBib290CiMgKGll"
			+ "IHJ1biBhcHQtZ2V0IHVwZ3JhZGUpCiMKIyBEZWZhdWx0OiBmYWxzZQojIEFsaWFzZXM6IGFwdF91cGdyYWRl"
			+ "CnBhY2thZ2VfdXBncmFkZTogdHJ1ZQoKcGFja2FnZXM6CiAtIHB5dGhvbgoKd3JpdGVfZmlsZXM6CiAgLSBw"
			+ "YXRoOiAvZXRjL25ldHdvcmsvaW50ZXJmYWNlcy5kLzUwLWNsb3VkLWluaXQuY2ZnCiAgICBjb250ZW50OiB8"
			+ "CiAgICAgIGF1dG8gbG8KICAgICAgaWZhY2UgbG8gaW5ldCBsb29wYmFjawogICAgICAKICAgICAgYXV0byBl"
			+ "bnMwCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMAogICAgICBpZmFjZSBlbnMwIGluZXQgZGhjcAogICAgICAK"
			+ "ICAgICAgYXV0byBlbnMxCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMQogICAgICBpZmFjZSBlbnMxIGluZXQg"
			+ "ZGhjcAogICAgICAKICAgICAgYXV0byBlbnMyCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMgogICAgICBpZmFj"
			+ "ZSBlbnMyIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMzCiAgICAgIGFsbG93LWhvdHBsdWcgZW5z"
			+ "MwogICAgICBpZmFjZSBlbnMzIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM0CiAgICAgIGFsbG93"
			+ "LWhvdHBsdWcgZW5zNAogICAgICBpZmFjZSBlbnM0IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM1"
			+ "CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNQogICAgICBpZmFjZSBlbnM1IGluZXQgZGhjcAogICAgICAKICAg"
			+ "ICAgYXV0byBlbnM2CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNgogICAgICBpZmFjZSBlbnM2IGluZXQgZGhj"
			+ "cAogICAgICAKICAgICAgYXV0byBlbnM3CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNwogICAgICBpZmFjZSBl"
			+ "bnM3IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM4CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zOAog"
			+ "ICAgICBpZmFjZSBlbnM4IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM5CiAgICAgIGFsbG93LWhv"
			+ "dHBsdWcgZW5zOQogICAgICBpZmFjZSBlbnM5IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMxMAog"
			+ "ICAgICBhbGxvdy1ob3RwbHVnIGVuczEwCiAgICAgIGlmYWNlIGVuczEwIGluZXQgZGhjcAoKIyMj";
	String flavor = "ce8c33af-0cd5-4aac-b6f3-fcde58c4b262";
	String image = "e02f6965-0c9e-45e0-9a54-e2730bd05749";
	String remoteUser = "ubuntu";

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestExtensionRegistry.extensionRegistrySetup();
		TestUtility.loggerSetup();
		// System.setErr(new PrintStream(errContent));
	}

	@Before
	public void clearCache() {
		CachedResourceSet.getCache().clear();
	}

	@Test
	public void Shark_Loop_Par() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths.get(
				TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/par/SmartSharkForEachLocalPar.occic"));
		Path runOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/par/parRuntime.occic"));

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designOcci, runOcci, targetOcci);

		System.out.println(targetOcci.toAbsolutePath());

		assertTrue(errContent.toString().contains("org.eclipse.epsilon") == false);

		Configuration config = ModelUtility.loadOCCIConfiguration(targetOcci);
		assertTrue(TestUtility.detectDuplicateLinks(config) == false);
		// clearCache();
	}

	// Not backwards compatible to replica group extension
	@Test(expected = EolRuntimeException.class)
	public void Shark_Loop_Replicas_Active_Empty_Replica_Group() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths.get(
				TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/par/SmartSharkForEachLocalPar.occic"));
		Path runOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/par/activeNestedLoops.occic"));

		Resource designTimeModel = ModelUtility.loadOCCIintoEMFResource(designOcci);
		Resource runtimeModel = ModelUtility.loadOCCIintoEMFResource(runOcci);

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designTimeModel, runtimeModel, targetOcci);

		System.out.println(runOcci.toAbsolutePath());
		System.out.println(targetOcci.toAbsolutePath());

		assertTrue(errContent.toString().contains("org.eclipse.epsilon") == false);

		Configuration config = ModelUtility.loadOCCIConfiguration(targetOcci);
		assertTrue(TestUtility.detectDuplicateLinks(config) == false);
		assertTrue(TestUtility.numberOfVms(config) == 2);
	}

	// Not backwards compatible to replica group extension
	@Ignore
	public void Shark_Loop_Replica_multiple_trans() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths.get(
				TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/par/SmartSharkForEachLocalPar.occic"));
		Path runOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/par/activeNestedLoops.occic"));

		Resource designTimeModel = ModelUtility.loadOCCIintoEMFResource(designOcci);
		Resource runtimeModel = ModelUtility.loadOCCIintoEMFResource(runOcci);

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designTimeModel, runtimeModel, targetOcci);

		Resource targetModel = ModelUtility.loadOCCIintoEMFResource(targetOcci);
		Configuration target = (Configuration) targetModel.getContents().get(0);

		CachedResourceSet.getCache().clear();

		trans.transform(designTimeModel, targetModel, targetOcci);
		Resource targetModel2 = ModelUtility.loadOCCIintoEMFResource(targetOcci);
		Configuration target2 = (Configuration) targetModel2.getContents().get(0);

		assertEquals(target.getResources().size(), target2.getResources().size());
		assertTrue(errContent.toString().contains("org.eclipse.epsilon") == false);

		Configuration config = ModelUtility.loadOCCIConfiguration(targetOcci);
		assertTrue(TestUtility.detectDuplicateLinks(config) == false);
		// clearCache();
	}

	@Test
	public void Shark_Loop_Replicas_Finished() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths.get(
				TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/par/SmartSharkForEachLocalPar.occic"));
		Path runOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/par/finishedParLoops.occic"));

		Resource designTimeModel = ModelUtility.loadOCCIintoEMFResource(designOcci);
		Resource runtimeModel = ModelUtility.loadOCCIintoEMFResource(runOcci);

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designTimeModel, runtimeModel, targetOcci);

		Resource tarModel = ModelUtility.loadOCCIintoEMFResource(targetOcci);
		System.out.println(targetOcci.toAbsolutePath());

		assertTrue(errContent.toString().contains("org.eclipse.epsilon") == false);

		Configuration config = ModelUtility.loadOCCIConfiguration(targetOcci);
		assertTrue(TestUtility.detectDuplicateLinks(config) == false);
		// clearCache();
	}

	// Not backwards compatible to replica group extension
	@Test(expected = EolRuntimeException.class)
	public void Shark_Loop_Replicas_Active_Shared() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths.get(TestUtility
				.getPathToResource("occi/design2reqruntime/sharkLoop/par/SmartSharkForEachLocalParShared.occic"));
		Path runOcci = Paths.get(
				TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/par/activeNestedLoopsShared.occic"));

		Resource designTimeModel = ModelUtility.loadOCCIintoEMFResource(designOcci);
		Resource runtimeModel = ModelUtility.loadOCCIintoEMFResource(runOcci);

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designTimeModel, runtimeModel, targetOcci);

		System.out.println(targetOcci.toAbsolutePath());

		assertTrue(errContent.toString().contains("org.eclipse.epsilon") == false);

		Configuration config = ModelUtility.loadOCCIConfiguration(targetOcci);
		assertTrue(TestUtility.detectDuplicateLinks(config) == false);
		// clearCache();
	}

	// Not backwards compatible to replica group extension
	@Ignore
	public void Shark_Loop_Replicas_Active_Shared_With_Network() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths.get(TestUtility
				.getPathToResource("occi/design2reqruntime/sharkLoop/par/SmartSharkForEachLocalParShared.occic"));

		OCCI2OPENSTACKTransformator o2otrans = new OCCI2OPENSTACKTransformator();
		o2otrans.setTransformationProperties(managementNWRuntimeId, sshKey, userData, manNWid, flavor, image,
				remoteUser);
		o2otrans.transform(designOcci, designOcci);

		Path runOcci = Paths.get(
				TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/par/activeNestedLoopsShared.occic"));

		Resource designTimeModel = ModelUtility.loadOCCIintoEMFResource(designOcci);
		Resource runtimeModel = ModelUtility.loadOCCIintoEMFResource(runOcci);

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designTimeModel, runtimeModel, targetOcci);

		System.out.println(runOcci.toAbsolutePath());
		System.out.println(targetOcci.toAbsolutePath());

		assertTrue(errContent.toString().contains("org.eclipse.epsilon") == false);

		Configuration config = ModelUtility.loadOCCIConfiguration(targetOcci);
		assertTrue(TestUtility.detectDuplicateLinks(config) == false);
		// clearCache();
	}

	@Test
	public void Shark_Loop_Replicas_Finished_Shared_With_Network() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths.get(TestUtility
				.getPathToResource("occi/design2reqruntime/sharkLoop/par/SmartSharkForEachLocalParShared.occic"));

		Path runOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/par/finishedParLoops.occic"));

		Resource designTimeModel = ModelUtility.loadOCCIintoEMFResource(designOcci);
		Resource runtimeModel = ModelUtility.loadOCCIintoEMFResource(runOcci);

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designTimeModel, runtimeModel, targetOcci);

		System.out.println(targetOcci.toAbsolutePath());

		assertTrue(errContent.toString().contains("org.eclipse.epsilon") == false);

		Configuration config = ModelUtility.loadOCCIConfiguration(targetOcci);
		assertTrue(TestUtility.detectDuplicateLinks(config) == false);
		// clearCache();
	}

	@Test
	public void missingExecutableReplicaTest() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths.get(TestUtility
				.getPathToResource("occi/design2reqruntime/sharkLoop/par/SmartSharkForEachLocalParShared.occic"));
		Path runOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/par/missingExecutable.occic"));

		Resource designTimeModel = ModelUtility.loadOCCIintoEMFResource(designOcci);
		Resource runtimeModel = ModelUtility.loadOCCIintoEMFResource(runOcci);

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designTimeModel, runtimeModel, targetOcci);
		System.out.println(runOcci.toAbsolutePath());
		System.out.println(targetOcci.toAbsolutePath());

		System.out.println(errContent.toString());
		assertTrue(errContent.toString().contains("org.eclipse.epsilon") == false);

		Configuration config = ModelUtility.loadOCCIConfiguration(targetOcci);
		boolean duplicatedJob = false;
		for (org.eclipse.cmf.occi.core.Resource res : config.getResources()) {
			if (res.getTitle().contains("Replica") && res.getTitle().contains("mecosharkjob")) {
				duplicatedJob = true;
			}
		}

		assertTrue("Executable is not replicated", duplicatedJob);
		assertTrue(TestUtility.detectDuplicateLinks(config) == false);
		// clearCache();
	}

	@Test
	public void replicaGroupTest() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths.get(TestUtility
				.getPathToResource("occi/design2reqruntime/sharkLoop/par/SmartSharkForEachLocalParShared.occic"));
		Path runOcci = Paths.get(TestUtility
				.getPathToResource("occi/design2reqruntime/sharkLoop/par/activeNestedLoopsSharedGrouped.occic"));

		Resource designTimeModel = ModelUtility.loadOCCIintoEMFResource(designOcci);

		Resource runtimeModel = ModelUtility.loadOCCIintoEMFResource(runOcci);

		assertTrue(checkUpToDateWorkflowModel(runtimeModel));

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designTimeModel, runtimeModel, targetOcci);
		System.out.println(runOcci.toAbsolutePath());
		System.out.println(targetOcci.toAbsolutePath());

		System.out.println(errContent.toString());
		assertTrue(errContent.toString().contains("org.eclipse.epsilon") == false);

		CachedResourceSet.getCache().clear();

		Configuration config = ModelUtility.loadOCCIConfiguration(targetOcci);

		boolean replicaGroupMissing = false;
		for (org.eclipse.cmf.occi.core.Resource res : config.getResources()) {
			for (MixinBase mixB : res.getParts()) {
				if (mixB instanceof Replica) {
					Replica rep = (Replica) mixB;
					if (rep.getReplicaGroup() == "" || rep.getReplicaGroup() == null) {
						replicaGroupMissing = true;
					}
				}
			}
		}
		assertFalse("Replica Group is Missing", replicaGroupMissing);
		assertTrue(TestUtility.detectDuplicateLinks(config) == false);
		// clearCache();
	}

	private boolean checkUpToDateWorkflowModel(Resource runtimeModel) {
		ResourceSet rs = runtimeModel.getResourceSet();
		for (Resource res : rs.getResources()) {
			if (res.getURI().toString().contains("workflow")) {
				System.out.println(res);
				for (EObject obj : res.getContents()) {
					System.out.println(obj);
					for (EObject obj2 : obj.eContents()) {
						System.out.println(obj2);
						if (obj2.toString().contains("Replica")) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	@Test
	public void test11() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths.get(TestUtility
				.getPathToResource("occi/design2reqruntime/sharkLoop/par/SmartSharkForEachLocalParShared.occic"));

		OCCI2OPENSTACKTransformator o2otrans = new OCCI2OPENSTACKTransformator();
		o2otrans.setTransformationProperties(managementNWRuntimeId, sshKey, userData, manNWid, flavor, image,
				remoteUser);
		// o2otrans.transform(designOcci, designOcci);

		Path runOcci = Paths.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/par/test11.occic"));

		Resource designTimeModel = ModelUtility.loadOCCIintoEMFResource(designOcci);
		Resource runtimeModel = ModelUtility.loadOCCIintoEMFResource(runOcci);

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designTimeModel, runtimeModel, targetOcci);
		System.out.println(runOcci.toAbsolutePath());
		System.out.println(targetOcci.toAbsolutePath());

		System.out.println(errContent.toString());
		assertTrue(errContent.toString().contains("org.eclipse.epsilon") == false);

		Configuration config = ModelUtility.loadOCCIConfiguration(targetOcci);
		boolean replicaGroupMissing = false;
		for (org.eclipse.cmf.occi.core.Resource res : config.getResources()) {
			for (MixinBase mixB : res.getParts()) {
				if (mixB instanceof Replica) {
					Replica rep = (Replica) mixB;
					if (rep.getReplicaGroup() == "" || rep.getReplicaGroup() == null) {
						replicaGroupMissing = true;
					}
				}
			}
		}
		assertFalse("Replica Group is Missing", replicaGroupMissing);
		// clearCache();
	}

	@Test
	public void finishedMainReplicatedTaskTest() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths.get(TestUtility
				.getPathToResource("occi/design2reqruntime/sharkLoop/par/SmartSharkForEachPar3Shared(cocci).occic"));

		Path runOcci = Paths.get(TestUtility.getPathToResource(
				"occi/design2reqruntime/sharkLoop/par/SmartSharkForEachPar3Shared(cocci)FinishedMainTask.occic"));

		Resource designTimeModel = ModelUtility.loadOCCIintoEMFResource(designOcci);
		Resource runtimeModel = ModelUtility.loadOCCIintoEMFResource(runOcci);

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designTimeModel, runtimeModel, targetOcci);
		System.out.println(runOcci.toAbsolutePath());
		System.out.println(targetOcci.toAbsolutePath());

		System.out.println(errContent.toString());
		assertTrue(errContent.toString().contains("org.eclipse.epsilon") == false);

		Configuration config = ModelUtility.loadOCCIConfiguration(targetOcci);

		assertTrue(TestUtility.detectDuplicateLinks(config) == false);

		// clearCache();
	}

	@Test
	public void finishedMainReplicatedTaskWithRemovedComponentTest() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths.get(TestUtility
				.getPathToResource("occi/design2reqruntime/sharkLoop/par/SmartSharkForEachPar3Shared(cocci).occic"));

		Path runOcci = Paths.get(TestUtility.getPathToResource(
				"occi/design2reqruntime/sharkLoop/par/SmartSharkForEachPar3Shared(cocci)FinishedMainTaskRemovedExecutable.occic"));

		Resource designTimeModel = ModelUtility.loadOCCIintoEMFResource(designOcci);
		Resource runtimeModel = ModelUtility.loadOCCIintoEMFResource(runOcci);

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designTimeModel, runtimeModel, targetOcci);
		System.out.println(runOcci.toAbsolutePath());
		System.out.println(targetOcci.toAbsolutePath());

		System.out.println(errContent.toString());
		assertTrue(errContent.toString().contains("org.eclipse.epsilon") == false);

		Configuration config = ModelUtility.loadOCCIConfiguration(targetOcci);

		assertTrue(TestUtility.detectDuplicateLinks(config) == false);

		// clearCache();
	}

	@Test
	public void par5Test() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/par/Par5Design.occic"));

		Path runOcci = Paths.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/par/Par5Run.occic"));

		Resource designTimeModel = ModelUtility.loadOCCIintoEMFResource(designOcci);
		Resource runtimeModel = ModelUtility.loadOCCIintoEMFResource(runOcci);

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designTimeModel, runtimeModel, targetOcci);
		System.out.println(runOcci.toAbsolutePath());
		System.out.println(targetOcci.toAbsolutePath());

		System.out.println(errContent.toString());
		assertTrue(errContent.toString().contains("org.eclipse.epsilon") == false);

		Configuration config = ModelUtility.loadOCCIConfiguration(targetOcci);

		assertTrue(TestUtility.detectDuplicateLinks(config) == false);

		// clearCache();
	}

	@Test
	public void finishedNestedLoops() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths.get(TestUtility
				.getPathToResource("occi/design2reqruntime/sharkLoop/par/SmartSharkForEachPar5Shared(cocci).occic"));

		Path runOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/par/finishedNestedLoops.occic"));

		Resource designTimeModel = ModelUtility.loadOCCIintoEMFResource(designOcci);
		Resource runtimeModel = ModelUtility.loadOCCIintoEMFResource(runOcci);

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designTimeModel, runtimeModel, targetOcci);
		System.out.println(runOcci.toAbsolutePath());
		System.out.println(targetOcci.toAbsolutePath());

		System.out.println(errContent.toString());
		assertTrue(errContent.toString().contains("org.eclipse.epsilon") == false);

		Configuration config = ModelUtility.loadOCCIConfiguration(targetOcci);

		assertTrue(TestUtility.detectDuplicateLinks(config) == false);

		// clearCache();
	}

	@Test
	public void missingCompReplica() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths.get(TestUtility
				.getPathToResource("occi/design2reqruntime/sharkLoop/par/SmartSharkForEachPar5Shared(cocci).occic"));

		Path runOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/par/missingComp.occic"));

		Resource designTimeModel = ModelUtility.loadOCCIintoEMFResource(designOcci);
		Resource runtimeModel = ModelUtility.loadOCCIintoEMFResource(runOcci);

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designTimeModel, runtimeModel, targetOcci);
		System.out.println(runOcci.toAbsolutePath());
		System.out.println(targetOcci.toAbsolutePath());

		System.out.println(errContent.toString());
		assertTrue(errContent.toString().contains("org.eclipse.epsilon") == false);

		Configuration config = ModelUtility.loadOCCIConfiguration(targetOcci);

		assertTrue(TestUtility.detectDuplicateLinks(config) == false);

		// clearCache();
	}

	@Test
	public void VM1Deleted() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/par/testDesign.occic"));

		Path runOcci = Paths.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/par/testRun.occic"));

		Resource designTimeModel = ModelUtility.loadOCCIintoEMFResource(designOcci);
		Resource runtimeModel = ModelUtility.loadOCCIintoEMFResource(runOcci);

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designTimeModel, runtimeModel, targetOcci);
		System.out.println(runOcci.toAbsolutePath());
		System.out.println(targetOcci.toAbsolutePath());

		System.out.println(errContent.toString());
		assertTrue(errContent.toString().contains("org.eclipse.epsilon") == false);

		Configuration config = ModelUtility.loadOCCIConfiguration(targetOcci);

		assertTrue(TestUtility.detectDuplicateLinks(config) == false);
		for (org.eclipse.cmf.occi.core.Resource res : config.getResources()) {
			if (res.getTitle().equals("VM1")) {
				assertTrue(false);
			}
		}
		// clearCache();
	}

	@After
	public void restoreStreams() {
		System.setErr(originalErr);
	}
}
