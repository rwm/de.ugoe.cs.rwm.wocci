/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.scheduler.transformation.test;

import static junit.framework.TestCase.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.cmf.occi.core.Resource;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.wocci.scheduler.transformation.DESIGN2REQRUNTIMETransformator;
import de.ugoe.cs.rwm.wocci.utility.TestExtensionRegistry;
import monitoring.Datagatherer;
import monitoring.Sensor;

public class Design2ReqRuntimeSharkLoopTest2 {
	Path targetOcci = Paths.get(TestUtility.getPathToResource("occi/Target.occic"));

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestExtensionRegistry.extensionRegistrySetup();
		TestUtility.loggerSetup();
	}

	@Before
	public void clearCache() {
		CachedResourceSet.getCache().clear();
	}

	@Test
	public void Shark_VCS_withoutLoose() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/SmartSharkForEachLocal.occic"));
		Path runOcci = Paths.get(TestUtility.getPathToResource("occi/Empty2.occic"));

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designOcci, runOcci, targetOcci);

		// clearCache();
	}

	@Test
	public void Shark_VCS_withLoose() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths
				.get(TestUtility.getPathToResource("occi/design2reqruntime/sharkLoop/SmartSharkForEachLocal2.occic"));
		Path runOcci = Paths.get(TestUtility.getPathToResource("occi/Empty2.occic"));

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designOcci, runOcci, targetOcci);

		// clearCache();
	}

	@Test
	public void Shark_Loop_Sensor_Finished() throws EolRuntimeException {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.DEBUG);
		Path designOcci = Paths.get(TestUtility
				.getPathToResource("occi/design2reqruntime/sharkLoop/SmartSharkForEachSensorFinished.occic"));
		Path runOcci = Paths.get(TestUtility.getPathToResource("occi/Empty2.occic"));

		DESIGN2REQRUNTIMETransformator trans = new DESIGN2REQRUNTIMETransformator();
		trans.transform(designOcci, runOcci, targetOcci);

		System.out.println(targetOcci.toAbsolutePath());

		Configuration config = ModelUtility.loadOCCIConfiguration(targetOcci);

		for (Resource res : config.getResources()) {
			if (res instanceof Sensor) {
				assertTrue(false);
			} else if (res instanceof Datagatherer) {
				assertTrue(false);
			}
		}
	}
}
