# OCCI Workflow Extension
The OCCI Workflow extension adds Resource and Link definitions to model task sequences on top of infrastructure designed with OCCI. The extension itself is modeled using [OCCI-Studio](https://github.com/occiware/OCCI-Studio) including generated artifacts.

A small sketch of the extension and how it integrates with the standard conform extension is visualized below. The corresponding EMF artifacts can be found in the [model](./model) folder with the generated java classes being found in [src-gen](./src-gen/).

![alt text](./doc/bitmap.png "Extension")