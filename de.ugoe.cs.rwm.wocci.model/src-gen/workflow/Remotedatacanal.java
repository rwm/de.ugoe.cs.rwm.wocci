/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package workflow;

import java.util.Map;

import org.eclipse.cmf.occi.core.MixinBase;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Remotedatacanal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link workflow.Remotedatacanal#getTaskDatalinkSourceResource <em>Task Datalink Source Resource</em>}</li>
 *   <li>{@link workflow.Remotedatacanal#getTaskDatalinkTargetResource <em>Task Datalink Target Resource</em>}</li>
 * </ul>
 *
 * @see workflow.WorkflowPackage#getRemotedatacanal()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='appliesConstraint'"
 * @generated
 */
public interface Remotedatacanal extends MixinBase {
	/**
	 * Returns the value of the '<em><b>Task Datalink Source Resource</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Task Datalink Source Resource</em>' attribute.
	 * @see #setTaskDatalinkSourceResource(String)
	 * @see workflow.WorkflowPackage#getRemotedatacanal_TaskDatalinkSourceResource()
	 * @model dataType="workflow.URI"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Remotedatacanal!taskDatalinkSourceResource'"
	 * @generated
	 */
	String getTaskDatalinkSourceResource();

	/**
	 * Sets the value of the '{@link workflow.Remotedatacanal#getTaskDatalinkSourceResource <em>Task Datalink Source Resource</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Datalink Source Resource</em>' attribute.
	 * @see #getTaskDatalinkSourceResource()
	 * @generated
	 */
	void setTaskDatalinkSourceResource(String value);

	/**
	 * Returns the value of the '<em><b>Task Datalink Target Resource</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Task Datalink Target Resource</em>' attribute.
	 * @see #setTaskDatalinkTargetResource(String)
	 * @see workflow.WorkflowPackage#getRemotedatacanal_TaskDatalinkTargetResource()
	 * @model dataType="workflow.URI"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Remotedatacanal!taskDatalinkTargetResource'"
	 * @generated
	 */
	String getTaskDatalinkTargetResource();

	/**
	 * Sets the value of the '{@link workflow.Remotedatacanal#getTaskDatalinkTargetResource <em>Task Datalink Target Resource</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Datalink Target Resource</em>' attribute.
	 * @see #getTaskDatalinkTargetResource()
	 * @generated
	 */
	void setTaskDatalinkTargetResource(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='/**\n *\n * inv appliesConstraint:\n *   let\n *     severity : Integer[1] = \'Remotedatacanal::appliesConstraint\'.getSeverity()\n *   in\n *     if severity &lt;= 0\n *     then true\n *     else\n *       let result : Boolean[1] = self.entity.oclIsKindOf(Datalink)\n *       in\n *         \'Remotedatacanal::appliesConstraint\'.logDiagnostic(self, null, diagnostics, context, null, severity, result, 0)\n *     endif\n \052/\nfinal /*@NonInvalid\052/ &lt;%org.eclipse.ocl.pivot.evaluation.Executor%&gt; executor = &lt;%org.eclipse.ocl.pivot.internal.utilities.PivotUtilInternal%&gt;.getExecutor(this);\nfinal /*@NonInvalid\052/ &lt;%org.eclipse.ocl.pivot.ids.IdResolver%&gt; idResolver = executor.getIdResolver();\nfinal /*@NonInvalid\052/ &lt;%org.eclipse.ocl.pivot.values.IntegerValue%&gt; severity_0 = &lt;%org.eclipse.ocl.pivot.library.string.CGStringGetSeverityOperation%&gt;.INSTANCE.evaluate(executor, &lt;%workflow.WorkflowTables%&gt;.STR_Remotedatacanal_c_c_appliesConstraint);\nfinal /*@NonInvalid\052/ boolean le = &lt;%org.eclipse.ocl.pivot.library.oclany.OclComparableLessThanEqualOperation%&gt;.INSTANCE.evaluate(executor, severity_0, &lt;%workflow.WorkflowTables%&gt;.INT_0).booleanValue();\n/*@NonInvalid\052/ boolean symbol_0;\nif (le) {\n\tsymbol_0 = &lt;%org.eclipse.ocl.pivot.utilities.ValueUtil%&gt;.TRUE_VALUE;\n}\nelse {\n\tfinal /*@NonInvalid\052/ &lt;%org.eclipse.ocl.pivot.Class%&gt; TYP_workflow_c_c_Datalink_0 = idResolver.getClass(&lt;%workflow.WorkflowTables%&gt;.CLSSid_Datalink, null);\n\tfinal /*@NonInvalid\052/ &lt;%org.eclipse.cmf.occi.core.Entity%&gt; entity = this.getEntity();\n\tfinal /*@NonInvalid\052/ boolean result = &lt;%org.eclipse.ocl.pivot.library.oclany.OclAnyOclIsKindOfOperation%&gt;.INSTANCE.evaluate(executor, entity, TYP_workflow_c_c_Datalink_0).booleanValue();\n\tfinal /*@NonInvalid\052/ boolean logDiagnostic = &lt;%org.eclipse.ocl.pivot.library.string.CGStringLogDiagnosticOperation%&gt;.INSTANCE.evaluate(executor, &lt;%org.eclipse.ocl.pivot.ids.TypeId%&gt;.BOOLEAN, &lt;%workflow.WorkflowTables%&gt;.STR_Remotedatacanal_c_c_appliesConstraint, this, (Object)null, diagnostics, context, (Object)null, severity_0, result, &lt;%workflow.WorkflowTables%&gt;.INT_0).booleanValue();\n\tsymbol_0 = logDiagnostic;\n}\nreturn Boolean.TRUE == symbol_0;'"
	 * @generated
	 */
	boolean appliesConstraint(DiagnosticChain diagnostics, Map<Object, Object> context);

} // Remotedatacanal
