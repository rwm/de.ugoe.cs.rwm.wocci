/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package workflow;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Decision</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Decision
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link workflow.Decision#getWorkflowDecisionInput <em>Workflow Decision Input</em>}</li>
 *   <li>{@link workflow.Decision#getWorkflowDecisionExpression <em>Workflow Decision Expression</em>}</li>
 *   <li>{@link workflow.Decision#getWorkflowDecisionResult <em>Workflow Decision Result</em>}</li>
 * </ul>
 *
 * @see workflow.WorkflowPackage#getDecision()
 * @model
 * @generated
 */
public interface Decision extends Task {
	/**
	 * Returns the value of the '<em><b>Workflow Decision Input</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Workflow Decision Input</em>' attribute.
	 * @see #setWorkflowDecisionInput(String)
	 * @see workflow.WorkflowPackage#getDecision_WorkflowDecisionInput()
	 * @model dataType="workflow.String"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Decision!workflowDecisionInput'"
	 * @generated
	 */
	String getWorkflowDecisionInput();

	/**
	 * Sets the value of the '{@link workflow.Decision#getWorkflowDecisionInput <em>Workflow Decision Input</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Workflow Decision Input</em>' attribute.
	 * @see #getWorkflowDecisionInput()
	 * @generated
	 */
	void setWorkflowDecisionInput(String value);

	/**
	 * Returns the value of the '<em><b>Workflow Decision Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Workflow Decision Expression</em>' attribute.
	 * @see #setWorkflowDecisionExpression(String)
	 * @see workflow.WorkflowPackage#getDecision_WorkflowDecisionExpression()
	 * @model dataType="workflow.String"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Decision!workflowDecisionExpression'"
	 * @generated
	 */
	String getWorkflowDecisionExpression();

	/**
	 * Sets the value of the '{@link workflow.Decision#getWorkflowDecisionExpression <em>Workflow Decision Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Workflow Decision Expression</em>' attribute.
	 * @see #getWorkflowDecisionExpression()
	 * @generated
	 */
	void setWorkflowDecisionExpression(String value);

	/**
	 * Returns the value of the '<em><b>Workflow Decision Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Workflow Decision Result</em>' attribute.
	 * @see #setWorkflowDecisionResult(String)
	 * @see workflow.WorkflowPackage#getDecision_WorkflowDecisionResult()
	 * @model dataType="workflow.String"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Decision!workflowDecisionResult'"
	 * @generated
	 */
	String getWorkflowDecisionResult();

	/**
	 * Sets the value of the '{@link workflow.Decision#getWorkflowDecisionResult <em>Workflow Decision Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Workflow Decision Result</em>' attribute.
	 * @see #getWorkflowDecisionResult()
	 * @generated
	 */
	void setWorkflowDecisionResult(String value);

} // Decision
