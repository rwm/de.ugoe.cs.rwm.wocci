/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package workflow.util;

import java.util.Map;

import org.eclipse.cmf.occi.core.util.OCCIValidator;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import workflow.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see workflow.WorkflowPackage
 * @generated
 */
public class WorkflowValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final WorkflowValidator INSTANCE = new WorkflowValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "workflow";

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Source Constraint' of 'Executionlink'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int EXECUTIONLINK__SOURCE_CONSTRAINT = 1;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Target Constraint' of 'Executionlink'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int EXECUTIONLINK__TARGET_CONSTRAINT = 2;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Source Constraint' of 'Taskdependency'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TASKDEPENDENCY__SOURCE_CONSTRAINT = 3;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Target Constraint' of 'Taskdependency'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TASKDEPENDENCY__TARGET_CONSTRAINT = 4;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Source Constraint' of 'Platformdependency'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int PLATFORMDEPENDENCY__SOURCE_CONSTRAINT = 5;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Target Constraint' of 'Platformdependency'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int PLATFORMDEPENDENCY__TARGET_CONSTRAINT = 6;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Source Constraint' of 'Nesteddependency'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int NESTEDDEPENDENCY__SOURCE_CONSTRAINT = 7;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Target Constraint' of 'Nesteddependency'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int NESTEDDEPENDENCY__TARGET_CONSTRAINT = 8;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Applies Constraint' of 'Storagecanal'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int STORAGECANAL__APPLIES_CONSTRAINT = 9;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Applies Constraint' of 'Networkcanal'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int NETWORKCANAL__APPLIES_CONSTRAINT = 10;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Applies Constraint' of 'Remotedatacanal'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int REMOTEDATACANAL__APPLIES_CONSTRAINT = 11;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Applies Constraint' of 'Profile'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int PROFILE__APPLIES_CONSTRAINT = 12;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Applies Constraint' of 'Localcanal'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int LOCALCANAL__APPLIES_CONSTRAINT = 13;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Applies Constraint' of 'Taskobservation'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TASKOBSERVATION__APPLIES_CONSTRAINT = 14;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Applies Constraint' of 'Controlflowguard'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int CONTROLFLOWGUARD__APPLIES_CONSTRAINT = 15;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Applies Constraint' of 'Loopiteration'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int LOOPITERATION__APPLIES_CONSTRAINT = 16;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Applies Constraint' of 'Foreach'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int FOREACH__APPLIES_CONSTRAINT = 17;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Applies Constraint' of 'For'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int FOR__APPLIES_CONSTRAINT = 18;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Applies Constraint' of 'While'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int WHILE__APPLIES_CONSTRAINT = 19;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Applies Constraint' of 'Parallelloop'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int PARALLELLOOP__APPLIES_CONSTRAINT = 20;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Applies Constraint' of 'Replica'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int REPLICA__APPLIES_CONSTRAINT = 21;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Applies Constraint' of 'Shared'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SHARED__APPLIES_CONSTRAINT = 22;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 22;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * The cached base package validator.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OCCIValidator occiValidator;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowValidator() {
		super();
		occiValidator = OCCIValidator.INSTANCE;
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return WorkflowPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case WorkflowPackage.TASK:
				return validateTask((Task)value, diagnostics, context);
			case WorkflowPackage.EXECUTIONLINK:
				return validateExecutionlink((Executionlink)value, diagnostics, context);
			case WorkflowPackage.DATALINK:
				return validateDatalink((Datalink)value, diagnostics, context);
			case WorkflowPackage.CONTROLFLOWLINK:
				return validateControlflowlink((Controlflowlink)value, diagnostics, context);
			case WorkflowPackage.TASKDEPENDENCY:
				return validateTaskdependency((Taskdependency)value, diagnostics, context);
			case WorkflowPackage.PLATFORMDEPENDENCY:
				return validatePlatformdependency((Platformdependency)value, diagnostics, context);
			case WorkflowPackage.DECISION:
				return validateDecision((Decision)value, diagnostics, context);
			case WorkflowPackage.LOOP:
				return validateLoop((Loop)value, diagnostics, context);
			case WorkflowPackage.NESTEDDEPENDENCY:
				return validateNesteddependency((Nesteddependency)value, diagnostics, context);
			case WorkflowPackage.STORAGECANAL:
				return validateStoragecanal((Storagecanal)value, diagnostics, context);
			case WorkflowPackage.NETWORKCANAL:
				return validateNetworkcanal((Networkcanal)value, diagnostics, context);
			case WorkflowPackage.REMOTEDATACANAL:
				return validateRemotedatacanal((Remotedatacanal)value, diagnostics, context);
			case WorkflowPackage.PROFILE:
				return validateProfile((Profile)value, diagnostics, context);
			case WorkflowPackage.LOCALCANAL:
				return validateLocalcanal((Localcanal)value, diagnostics, context);
			case WorkflowPackage.TASKOBSERVATION:
				return validateTaskobservation((Taskobservation)value, diagnostics, context);
			case WorkflowPackage.CONTROLFLOWGUARD:
				return validateControlflowguard((Controlflowguard)value, diagnostics, context);
			case WorkflowPackage.LOOPITERATION:
				return validateLoopiteration((Loopiteration)value, diagnostics, context);
			case WorkflowPackage.FOREACH:
				return validateForeach((Foreach)value, diagnostics, context);
			case WorkflowPackage.FOR:
				return validateFor((For)value, diagnostics, context);
			case WorkflowPackage.WHILE:
				return validateWhile((While)value, diagnostics, context);
			case WorkflowPackage.PARALLELLOOP:
				return validateParallelloop((Parallelloop)value, diagnostics, context);
			case WorkflowPackage.REPLICA:
				return validateReplica((Replica)value, diagnostics, context);
			case WorkflowPackage.SHARED:
				return validateShared((Shared)value, diagnostics, context);
			case WorkflowPackage.STATUS:
				return validateStatus((Status)value, diagnostics, context);
			case WorkflowPackage.DURATION:
				return validateDuration((Double)value, diagnostics, context);
			case WorkflowPackage.TIMESTAMP:
				return validateTimestamp((Long)value, diagnostics, context);
			case WorkflowPackage.URI:
				return validateURI((String)value, diagnostics, context);
			case WorkflowPackage.PATH:
				return validatePath((String)value, diagnostics, context);
			case WorkflowPackage.STRING:
				return validateString((String)value, diagnostics, context);
			case WorkflowPackage.BOOLEAN:
				return validateBoolean((Boolean)value, diagnostics, context);
			case WorkflowPackage.INTEGER:
				return validateInteger((Integer)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTask(Task task, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(task, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(task, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(task, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(task, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(task, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(task, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(task, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(task, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(task, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_IdUnique(task, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_AttributesNameUnique(task, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_KindCompatibleWithOneAppliesOfEachMixin(task, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_DifferentMixins(task, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateResource_ResourceKindIsInParent(task, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExecutionlink(Executionlink executionlink, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(executionlink, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(executionlink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(executionlink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(executionlink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(executionlink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(executionlink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(executionlink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(executionlink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(executionlink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_IdUnique(executionlink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_AttributesNameUnique(executionlink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_KindCompatibleWithOneAppliesOfEachMixin(executionlink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_DifferentMixins(executionlink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateLink_LinkKindIsInParent(executionlink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateLink_sourceReferenceInvariant(executionlink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateLink_targetReferenceInvariant(executionlink, diagnostics, context);
		if (result || diagnostics != null) result &= validateExecutionlink_targetConstraint(executionlink, diagnostics, context);
		if (result || diagnostics != null) result &= validateExecutionlink_sourceConstraint(executionlink, diagnostics, context);
		return result;
	}

	/**
	 * Validates the targetConstraint constraint of '<em>Executionlink</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExecutionlink_targetConstraint(Executionlink executionlink, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return executionlink.targetConstraint(diagnostics, context);
	}

	/**
	 * Validates the sourceConstraint constraint of '<em>Executionlink</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExecutionlink_sourceConstraint(Executionlink executionlink, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return executionlink.sourceConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDatalink(Datalink datalink, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(datalink, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(datalink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(datalink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(datalink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(datalink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(datalink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(datalink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(datalink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(datalink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_IdUnique(datalink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_AttributesNameUnique(datalink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_KindCompatibleWithOneAppliesOfEachMixin(datalink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_DifferentMixins(datalink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateLink_LinkKindIsInParent(datalink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateLink_sourceReferenceInvariant(datalink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateLink_targetReferenceInvariant(datalink, diagnostics, context);
		if (result || diagnostics != null) result &= validateTaskdependency_targetConstraint(datalink, diagnostics, context);
		if (result || diagnostics != null) result &= validateTaskdependency_sourceConstraint(datalink, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateControlflowlink(Controlflowlink controlflowlink, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(controlflowlink, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(controlflowlink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(controlflowlink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(controlflowlink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(controlflowlink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(controlflowlink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(controlflowlink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(controlflowlink, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(controlflowlink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_IdUnique(controlflowlink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_AttributesNameUnique(controlflowlink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_KindCompatibleWithOneAppliesOfEachMixin(controlflowlink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_DifferentMixins(controlflowlink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateLink_LinkKindIsInParent(controlflowlink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateLink_sourceReferenceInvariant(controlflowlink, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateLink_targetReferenceInvariant(controlflowlink, diagnostics, context);
		if (result || diagnostics != null) result &= validateTaskdependency_targetConstraint(controlflowlink, diagnostics, context);
		if (result || diagnostics != null) result &= validateTaskdependency_sourceConstraint(controlflowlink, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTaskdependency(Taskdependency taskdependency, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(taskdependency, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(taskdependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(taskdependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(taskdependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(taskdependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(taskdependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(taskdependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(taskdependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(taskdependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_IdUnique(taskdependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_AttributesNameUnique(taskdependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_KindCompatibleWithOneAppliesOfEachMixin(taskdependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_DifferentMixins(taskdependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateLink_LinkKindIsInParent(taskdependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateLink_sourceReferenceInvariant(taskdependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateLink_targetReferenceInvariant(taskdependency, diagnostics, context);
		if (result || diagnostics != null) result &= validateTaskdependency_targetConstraint(taskdependency, diagnostics, context);
		if (result || diagnostics != null) result &= validateTaskdependency_sourceConstraint(taskdependency, diagnostics, context);
		return result;
	}

	/**
	 * Validates the targetConstraint constraint of '<em>Taskdependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTaskdependency_targetConstraint(Taskdependency taskdependency, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return taskdependency.targetConstraint(diagnostics, context);
	}

	/**
	 * Validates the sourceConstraint constraint of '<em>Taskdependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTaskdependency_sourceConstraint(Taskdependency taskdependency, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return taskdependency.sourceConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePlatformdependency(Platformdependency platformdependency, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(platformdependency, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(platformdependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(platformdependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(platformdependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(platformdependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(platformdependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(platformdependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(platformdependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(platformdependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_IdUnique(platformdependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_AttributesNameUnique(platformdependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_KindCompatibleWithOneAppliesOfEachMixin(platformdependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_DifferentMixins(platformdependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateLink_LinkKindIsInParent(platformdependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateLink_sourceReferenceInvariant(platformdependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateLink_targetReferenceInvariant(platformdependency, diagnostics, context);
		if (result || diagnostics != null) result &= validatePlatformdependency_targetConstraint(platformdependency, diagnostics, context);
		if (result || diagnostics != null) result &= validatePlatformdependency_sourceConstraint(platformdependency, diagnostics, context);
		return result;
	}

	/**
	 * Validates the targetConstraint constraint of '<em>Platformdependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePlatformdependency_targetConstraint(Platformdependency platformdependency, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return platformdependency.targetConstraint(diagnostics, context);
	}

	/**
	 * Validates the sourceConstraint constraint of '<em>Platformdependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePlatformdependency_sourceConstraint(Platformdependency platformdependency, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return platformdependency.sourceConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDecision(Decision decision, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(decision, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(decision, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(decision, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(decision, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(decision, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(decision, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(decision, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(decision, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(decision, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_IdUnique(decision, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_AttributesNameUnique(decision, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_KindCompatibleWithOneAppliesOfEachMixin(decision, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_DifferentMixins(decision, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateResource_ResourceKindIsInParent(decision, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLoop(Loop loop, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(loop, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(loop, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(loop, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(loop, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(loop, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(loop, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(loop, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(loop, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(loop, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_IdUnique(loop, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_AttributesNameUnique(loop, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_KindCompatibleWithOneAppliesOfEachMixin(loop, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_DifferentMixins(loop, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateResource_ResourceKindIsInParent(loop, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNesteddependency(Nesteddependency nesteddependency, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(nesteddependency, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(nesteddependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(nesteddependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(nesteddependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(nesteddependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(nesteddependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(nesteddependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(nesteddependency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(nesteddependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_IdUnique(nesteddependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_AttributesNameUnique(nesteddependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_KindCompatibleWithOneAppliesOfEachMixin(nesteddependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateEntity_DifferentMixins(nesteddependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateLink_LinkKindIsInParent(nesteddependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateLink_sourceReferenceInvariant(nesteddependency, diagnostics, context);
		if (result || diagnostics != null) result &= occiValidator.validateLink_targetReferenceInvariant(nesteddependency, diagnostics, context);
		if (result || diagnostics != null) result &= validateNesteddependency_targetConstraint(nesteddependency, diagnostics, context);
		if (result || diagnostics != null) result &= validateNesteddependency_sourceConstraint(nesteddependency, diagnostics, context);
		return result;
	}

	/**
	 * Validates the targetConstraint constraint of '<em>Nesteddependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNesteddependency_targetConstraint(Nesteddependency nesteddependency, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return nesteddependency.targetConstraint(diagnostics, context);
	}

	/**
	 * Validates the sourceConstraint constraint of '<em>Nesteddependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNesteddependency_sourceConstraint(Nesteddependency nesteddependency, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return nesteddependency.sourceConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStoragecanal(Storagecanal storagecanal, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(storagecanal, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(storagecanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(storagecanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(storagecanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(storagecanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(storagecanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(storagecanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(storagecanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(storagecanal, diagnostics, context);
		if (result || diagnostics != null) result &= validateStoragecanal_appliesConstraint(storagecanal, diagnostics, context);
		return result;
	}

	/**
	 * Validates the appliesConstraint constraint of '<em>Storagecanal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStoragecanal_appliesConstraint(Storagecanal storagecanal, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return storagecanal.appliesConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNetworkcanal(Networkcanal networkcanal, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(networkcanal, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(networkcanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(networkcanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(networkcanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(networkcanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(networkcanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(networkcanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(networkcanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(networkcanal, diagnostics, context);
		if (result || diagnostics != null) result &= validateNetworkcanal_appliesConstraint(networkcanal, diagnostics, context);
		return result;
	}

	/**
	 * Validates the appliesConstraint constraint of '<em>Networkcanal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNetworkcanal_appliesConstraint(Networkcanal networkcanal, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return networkcanal.appliesConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRemotedatacanal(Remotedatacanal remotedatacanal, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(remotedatacanal, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(remotedatacanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(remotedatacanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(remotedatacanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(remotedatacanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(remotedatacanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(remotedatacanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(remotedatacanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(remotedatacanal, diagnostics, context);
		if (result || diagnostics != null) result &= validateRemotedatacanal_appliesConstraint(remotedatacanal, diagnostics, context);
		return result;
	}

	/**
	 * Validates the appliesConstraint constraint of '<em>Remotedatacanal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRemotedatacanal_appliesConstraint(Remotedatacanal remotedatacanal, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return remotedatacanal.appliesConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProfile(Profile profile, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(profile, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(profile, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(profile, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(profile, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(profile, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(profile, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(profile, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(profile, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(profile, diagnostics, context);
		if (result || diagnostics != null) result &= validateProfile_appliesConstraint(profile, diagnostics, context);
		return result;
	}

	/**
	 * Validates the appliesConstraint constraint of '<em>Profile</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProfile_appliesConstraint(Profile profile, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return profile.appliesConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLocalcanal(Localcanal localcanal, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(localcanal, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(localcanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(localcanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(localcanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(localcanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(localcanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(localcanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(localcanal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(localcanal, diagnostics, context);
		if (result || diagnostics != null) result &= validateLocalcanal_appliesConstraint(localcanal, diagnostics, context);
		return result;
	}

	/**
	 * Validates the appliesConstraint constraint of '<em>Localcanal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLocalcanal_appliesConstraint(Localcanal localcanal, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return localcanal.appliesConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTaskobservation(Taskobservation taskobservation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(taskobservation, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(taskobservation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(taskobservation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(taskobservation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(taskobservation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(taskobservation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(taskobservation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(taskobservation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(taskobservation, diagnostics, context);
		if (result || diagnostics != null) result &= validateTaskobservation_appliesConstraint(taskobservation, diagnostics, context);
		return result;
	}

	/**
	 * Validates the appliesConstraint constraint of '<em>Taskobservation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTaskobservation_appliesConstraint(Taskobservation taskobservation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return taskobservation.appliesConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateControlflowguard(Controlflowguard controlflowguard, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(controlflowguard, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(controlflowguard, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(controlflowguard, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(controlflowguard, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(controlflowguard, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(controlflowguard, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(controlflowguard, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(controlflowguard, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(controlflowguard, diagnostics, context);
		if (result || diagnostics != null) result &= validateControlflowguard_appliesConstraint(controlflowguard, diagnostics, context);
		return result;
	}

	/**
	 * Validates the appliesConstraint constraint of '<em>Controlflowguard</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateControlflowguard_appliesConstraint(Controlflowguard controlflowguard, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return controlflowguard.appliesConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLoopiteration(Loopiteration loopiteration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(loopiteration, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(loopiteration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(loopiteration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(loopiteration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(loopiteration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(loopiteration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(loopiteration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(loopiteration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(loopiteration, diagnostics, context);
		if (result || diagnostics != null) result &= validateLoopiteration_appliesConstraint(loopiteration, diagnostics, context);
		return result;
	}

	/**
	 * Validates the appliesConstraint constraint of '<em>Loopiteration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLoopiteration_appliesConstraint(Loopiteration loopiteration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return loopiteration.appliesConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateForeach(Foreach foreach, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(foreach, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(foreach, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(foreach, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(foreach, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(foreach, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(foreach, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(foreach, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(foreach, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(foreach, diagnostics, context);
		if (result || diagnostics != null) result &= validateForeach_appliesConstraint(foreach, diagnostics, context);
		return result;
	}

	/**
	 * Validates the appliesConstraint constraint of '<em>Foreach</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateForeach_appliesConstraint(Foreach foreach, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return foreach.appliesConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFor(For for_, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(for_, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(for_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(for_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(for_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(for_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(for_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(for_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(for_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(for_, diagnostics, context);
		if (result || diagnostics != null) result &= validateFor_appliesConstraint(for_, diagnostics, context);
		return result;
	}

	/**
	 * Validates the appliesConstraint constraint of '<em>For</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFor_appliesConstraint(For for_, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return for_.appliesConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateWhile(While while_, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(while_, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(while_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(while_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(while_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(while_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(while_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(while_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(while_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(while_, diagnostics, context);
		if (result || diagnostics != null) result &= validateWhile_appliesConstraint(while_, diagnostics, context);
		return result;
	}

	/**
	 * Validates the appliesConstraint constraint of '<em>While</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateWhile_appliesConstraint(While while_, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return while_.appliesConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateParallelloop(Parallelloop parallelloop, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(parallelloop, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(parallelloop, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(parallelloop, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(parallelloop, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(parallelloop, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(parallelloop, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(parallelloop, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(parallelloop, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(parallelloop, diagnostics, context);
		if (result || diagnostics != null) result &= validateParallelloop_appliesConstraint(parallelloop, diagnostics, context);
		return result;
	}

	/**
	 * Validates the appliesConstraint constraint of '<em>Parallelloop</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateParallelloop_appliesConstraint(Parallelloop parallelloop, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return parallelloop.appliesConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateReplica(Replica replica, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(replica, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(replica, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(replica, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(replica, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(replica, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(replica, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(replica, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(replica, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(replica, diagnostics, context);
		if (result || diagnostics != null) result &= validateReplica_appliesConstraint(replica, diagnostics, context);
		return result;
	}

	/**
	 * Validates the appliesConstraint constraint of '<em>Replica</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateReplica_appliesConstraint(Replica replica, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return replica.appliesConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShared(Shared shared, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(shared, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(shared, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(shared, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(shared, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(shared, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(shared, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(shared, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(shared, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(shared, diagnostics, context);
		if (result || diagnostics != null) result &= validateShared_appliesConstraint(shared, diagnostics, context);
		return result;
	}

	/**
	 * Validates the appliesConstraint constraint of '<em>Shared</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShared_appliesConstraint(Shared shared, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return shared.appliesConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStatus(Status status, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDuration(Double duration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTimestamp(Long timestamp, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateURI(String uri, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePath(String path, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateString(String string, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBoolean(Boolean boolean_, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateInteger(Integer integer, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //WorkflowValidator
