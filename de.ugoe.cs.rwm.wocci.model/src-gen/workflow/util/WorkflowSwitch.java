/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package workflow.util;

import org.eclipse.cmf.occi.core.Entity;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.MixinBase;
import org.eclipse.cmf.occi.core.Resource;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import workflow.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see workflow.WorkflowPackage
 * @generated
 */
public class WorkflowSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static WorkflowPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowSwitch() {
		if (modelPackage == null) {
			modelPackage = WorkflowPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case WorkflowPackage.TASK: {
				Task task = (Task)theEObject;
				T result = caseTask(task);
				if (result == null) result = caseResource(task);
				if (result == null) result = caseEntity(task);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.EXECUTIONLINK: {
				Executionlink executionlink = (Executionlink)theEObject;
				T result = caseExecutionlink(executionlink);
				if (result == null) result = caseLink(executionlink);
				if (result == null) result = caseEntity(executionlink);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.DATALINK: {
				Datalink datalink = (Datalink)theEObject;
				T result = caseDatalink(datalink);
				if (result == null) result = caseTaskdependency(datalink);
				if (result == null) result = caseLink(datalink);
				if (result == null) result = caseEntity(datalink);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.CONTROLFLOWLINK: {
				Controlflowlink controlflowlink = (Controlflowlink)theEObject;
				T result = caseControlflowlink(controlflowlink);
				if (result == null) result = caseTaskdependency(controlflowlink);
				if (result == null) result = caseLink(controlflowlink);
				if (result == null) result = caseEntity(controlflowlink);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.TASKDEPENDENCY: {
				Taskdependency taskdependency = (Taskdependency)theEObject;
				T result = caseTaskdependency(taskdependency);
				if (result == null) result = caseLink(taskdependency);
				if (result == null) result = caseEntity(taskdependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.PLATFORMDEPENDENCY: {
				Platformdependency platformdependency = (Platformdependency)theEObject;
				T result = casePlatformdependency(platformdependency);
				if (result == null) result = caseLink(platformdependency);
				if (result == null) result = caseEntity(platformdependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.DECISION: {
				Decision decision = (Decision)theEObject;
				T result = caseDecision(decision);
				if (result == null) result = caseTask(decision);
				if (result == null) result = caseResource(decision);
				if (result == null) result = caseEntity(decision);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.LOOP: {
				Loop loop = (Loop)theEObject;
				T result = caseLoop(loop);
				if (result == null) result = caseDecision(loop);
				if (result == null) result = caseTask(loop);
				if (result == null) result = caseResource(loop);
				if (result == null) result = caseEntity(loop);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.NESTEDDEPENDENCY: {
				Nesteddependency nesteddependency = (Nesteddependency)theEObject;
				T result = caseNesteddependency(nesteddependency);
				if (result == null) result = caseLink(nesteddependency);
				if (result == null) result = caseEntity(nesteddependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.STORAGECANAL: {
				Storagecanal storagecanal = (Storagecanal)theEObject;
				T result = caseStoragecanal(storagecanal);
				if (result == null) result = caseMixinBase(storagecanal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.NETWORKCANAL: {
				Networkcanal networkcanal = (Networkcanal)theEObject;
				T result = caseNetworkcanal(networkcanal);
				if (result == null) result = caseMixinBase(networkcanal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.REMOTEDATACANAL: {
				Remotedatacanal remotedatacanal = (Remotedatacanal)theEObject;
				T result = caseRemotedatacanal(remotedatacanal);
				if (result == null) result = caseMixinBase(remotedatacanal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.PROFILE: {
				Profile profile = (Profile)theEObject;
				T result = caseProfile(profile);
				if (result == null) result = caseMixinBase(profile);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.LOCALCANAL: {
				Localcanal localcanal = (Localcanal)theEObject;
				T result = caseLocalcanal(localcanal);
				if (result == null) result = caseMixinBase(localcanal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.TASKOBSERVATION: {
				Taskobservation taskobservation = (Taskobservation)theEObject;
				T result = caseTaskobservation(taskobservation);
				if (result == null) result = caseMixinBase(taskobservation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.CONTROLFLOWGUARD: {
				Controlflowguard controlflowguard = (Controlflowguard)theEObject;
				T result = caseControlflowguard(controlflowguard);
				if (result == null) result = caseMixinBase(controlflowguard);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.LOOPITERATION: {
				Loopiteration loopiteration = (Loopiteration)theEObject;
				T result = caseLoopiteration(loopiteration);
				if (result == null) result = caseMixinBase(loopiteration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.FOREACH: {
				Foreach foreach = (Foreach)theEObject;
				T result = caseForeach(foreach);
				if (result == null) result = caseMixinBase(foreach);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.FOR: {
				For for_ = (For)theEObject;
				T result = caseFor(for_);
				if (result == null) result = caseMixinBase(for_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.WHILE: {
				While while_ = (While)theEObject;
				T result = caseWhile(while_);
				if (result == null) result = caseMixinBase(while_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.PARALLELLOOP: {
				Parallelloop parallelloop = (Parallelloop)theEObject;
				T result = caseParallelloop(parallelloop);
				if (result == null) result = caseMixinBase(parallelloop);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.REPLICA: {
				Replica replica = (Replica)theEObject;
				T result = caseReplica(replica);
				if (result == null) result = caseMixinBase(replica);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorkflowPackage.SHARED: {
				Shared shared = (Shared)theEObject;
				T result = caseShared(shared);
				if (result == null) result = caseMixinBase(shared);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Task</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Task</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTask(Task object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Executionlink</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Executionlink</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExecutionlink(Executionlink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Datalink</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Datalink</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDatalink(Datalink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Controlflowlink</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Controlflowlink</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseControlflowlink(Controlflowlink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Taskdependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Taskdependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaskdependency(Taskdependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Platformdependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Platformdependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePlatformdependency(Platformdependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Decision</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Decision</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDecision(Decision object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Loop</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Loop</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLoop(Loop object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Nesteddependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Nesteddependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNesteddependency(Nesteddependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Storagecanal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Storagecanal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStoragecanal(Storagecanal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Networkcanal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Networkcanal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNetworkcanal(Networkcanal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Remotedatacanal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Remotedatacanal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRemotedatacanal(Remotedatacanal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Profile</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Profile</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProfile(Profile object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Localcanal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Localcanal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLocalcanal(Localcanal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Taskobservation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Taskobservation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaskobservation(Taskobservation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Controlflowguard</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Controlflowguard</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseControlflowguard(Controlflowguard object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Loopiteration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Loopiteration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLoopiteration(Loopiteration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Foreach</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Foreach</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseForeach(Foreach object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>For</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>For</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFor(For object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>While</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>While</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWhile(While object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parallelloop</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parallelloop</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParallelloop(Parallelloop object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Replica</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Replica</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReplica(Replica object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Shared</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Shared</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShared(Shared object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Entity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEntity(Entity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resource</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resource</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResource(Resource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLink(Link object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mixin Base</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mixin Base</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMixinBase(MixinBase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //WorkflowSwitch
