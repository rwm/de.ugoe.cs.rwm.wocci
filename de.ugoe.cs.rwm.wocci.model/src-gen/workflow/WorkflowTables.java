/*******************************************************************************
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 *************************************************************************
 * This code is 100% auto-generated
 * from:
 *   /de.ugoe.cs.rwm.wocci.model/model/workflow.ecore
 * using:
 *   /de.ugoe.cs.rwm.wocci.model/model/workflow.genmodel
 *   org.eclipse.ocl.examples.codegen.oclinecore.OCLinEcoreTables
 *
 * Do not edit it.
 *******************************************************************************/
package workflow;

import org.eclipse.cmf.occi.core.OCCITables;
import org.eclipse.ocl.pivot.ParameterTypes;
import org.eclipse.ocl.pivot.TemplateParameters;
import org.eclipse.ocl.pivot.ids.TypeId;
import org.eclipse.ocl.pivot.internal.library.ecore.EcoreExecutorEnumeration;
import org.eclipse.ocl.pivot.internal.library.ecore.EcoreExecutorEnumerationLiteral;
import org.eclipse.ocl.pivot.internal.library.ecore.EcoreExecutorPackage;
import org.eclipse.ocl.pivot.internal.library.ecore.EcoreExecutorProperty;
import org.eclipse.ocl.pivot.internal.library.ecore.EcoreExecutorType;
import org.eclipse.ocl.pivot.internal.library.executor.ExecutorFragment;
import org.eclipse.ocl.pivot.internal.library.executor.ExecutorOperation;
import org.eclipse.ocl.pivot.internal.library.executor.ExecutorProperty;
import org.eclipse.ocl.pivot.internal.library.executor.ExecutorStandardLibrary;
import org.eclipse.ocl.pivot.oclstdlib.OCLstdlibTables;
import org.eclipse.ocl.pivot.utilities.TypeUtil;
import workflow.WorkflowTables;

/**
 * WorkflowTables provides the dispatch tables for the workflow for use by the OCL dispatcher.
 *
 * In order to ensure correct static initialization, a top level class element must be accessed
 * before any nested class element. Therefore an access to PACKAGE.getClass() is recommended.
 */
public class WorkflowTables
{
	static {
		Init.initStart();
	}

	/**
	 *	The package descriptor for the package.
	 */
	public static final /*@NonNull*/ EcoreExecutorPackage PACKAGE = new EcoreExecutorPackage(WorkflowPackage.eINSTANCE);

	/**
	 *	The library of all packages and types.
	 */
	public static final /*@NonNull*/ ExecutorStandardLibrary LIBRARY = OCLstdlibTables.LIBRARY;

	/**
	 *	Constants used by auto-generated code.
	 */
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.RootPackageId PACKid_$metamodel$ = org.eclipse.ocl.pivot.ids.IdManager.getRootPackageId("$metamodel$");
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.NsURIPackageId PACKid_http_c_s_s_schemas_modmacao_org_s_occi_s_platform_s_ecore = org.eclipse.ocl.pivot.ids.IdManager.getNsURIPackageId("http://schemas.modmacao.org/occi/platform/ecore", null, org.modmacao.occi.platform.PlatformPackage.eINSTANCE);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.NsURIPackageId PACKid_http_c_s_s_schemas_ogf_org_s_occi_s_core_s_ecore = org.eclipse.ocl.pivot.ids.IdManager.getNsURIPackageId("http://schemas.ogf.org/occi/core/ecore", "occi", org.eclipse.cmf.occi.core.OCCIPackage.eINSTANCE);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.NsURIPackageId PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore = org.eclipse.ocl.pivot.ids.IdManager.getNsURIPackageId("http://schemas.ugoe.cs.rwm/workflow/ecore", null, workflow.WorkflowPackage.eINSTANCE);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Application = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_modmacao_org_s_occi_s_platform_s_ecore.getClassId("Application", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Class = workflow.WorkflowTables.PACKid_$metamodel$.getClassId("Class", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Component = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_modmacao_org_s_occi_s_platform_s_ecore.getClassId("Component", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Controlflowguard = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("Controlflowguard", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Controlflowlink = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("Controlflowlink", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Datalink = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("Datalink", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Entity = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ogf_org_s_occi_s_core_s_ecore.getClassId("Entity", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Executionlink = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("Executionlink", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_For = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("For", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Foreach = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("Foreach", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Localcanal = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("Localcanal", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Loop = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("Loop", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Loopiteration = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("Loopiteration", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Nesteddependency = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("Nesteddependency", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Networkcanal = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("Networkcanal", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Parallelloop = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("Parallelloop", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Platformdependency = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("Platformdependency", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Profile = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("Profile", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Remotedatacanal = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("Remotedatacanal", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Replica = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("Replica", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Resource = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ogf_org_s_occi_s_core_s_ecore.getClassId("Resource", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Shared = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("Shared", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Storagecanal = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("Storagecanal", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Task = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("Task", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Taskdependency = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("Taskdependency", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_Taskobservation = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("Taskobservation", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.ClassId CLSSid_While = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getClassId("While", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.DataTypeId DATAid_Duration = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getDataTypeId("Duration", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.DataTypeId DATAid_Path = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getDataTypeId("Path", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.DataTypeId DATAid_URI = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getDataTypeId("URI", 0);
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.ids.EnumerationId ENUMid_Status = workflow.WorkflowTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_workflow_s_ecore.getEnumerationId("Status");
	public static final /*@NonInvalid*/ org.eclipse.ocl.pivot.values.IntegerValue INT_0 = org.eclipse.ocl.pivot.utilities.ValueUtil.integerValueOf("0");
	public static final /*@NonInvalid*/ java.lang.String STR_Controlflowguard_c_c_appliesConstraint = "Controlflowguard::appliesConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_Executionlink_c_c_sourceConstraint = "Executionlink::sourceConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_Executionlink_c_c_targetConstraint = "Executionlink::targetConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_For_c_c_appliesConstraint = "For::appliesConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_Foreach_c_c_appliesConstraint = "Foreach::appliesConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_Localcanal_c_c_appliesConstraint = "Localcanal::appliesConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_Loopiteration_c_c_appliesConstraint = "Loopiteration::appliesConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_Nesteddependency_c_c_sourceConstraint = "Nesteddependency::sourceConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_Nesteddependency_c_c_targetConstraint = "Nesteddependency::targetConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_Networkcanal_c_c_appliesConstraint = "Networkcanal::appliesConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_Parallelloop_c_c_appliesConstraint = "Parallelloop::appliesConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_Platformdependency_c_c_sourceConstraint = "Platformdependency::sourceConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_Platformdependency_c_c_targetConstraint = "Platformdependency::targetConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_Profile_c_c_appliesConstraint = "Profile::appliesConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_Remotedatacanal_c_c_appliesConstraint = "Remotedatacanal::appliesConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_Replica_c_c_appliesConstraint = "Replica::appliesConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_Shared_c_c_appliesConstraint = "Shared::appliesConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_Storagecanal_c_c_appliesConstraint = "Storagecanal::appliesConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_Taskdependency_c_c_sourceConstraint = "Taskdependency::sourceConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_Taskdependency_c_c_targetConstraint = "Taskdependency::targetConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_Taskobservation_c_c_appliesConstraint = "Taskobservation::appliesConstraint";
	public static final /*@NonInvalid*/ java.lang.String STR_While_c_c_appliesConstraint = "While::appliesConstraint";

	/**
	 *	The type parameters for templated types and operations.
	 */
	public static class TypeParameters {
		static {
			Init.initStart();
			WorkflowTables.init();
		}

		static {
			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of WorkflowTables::TypeParameters and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The type descriptors for each type.
	 */
	public static class Types {
		static {
			Init.initStart();
			TypeParameters.init();
		}

		public static final /*@NonNull*/ EcoreExecutorType _Boolean = new EcoreExecutorType(TypeId.BOOLEAN, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Controlflowguard = new EcoreExecutorType(WorkflowPackage.Literals.CONTROLFLOWGUARD, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Controlflowlink = new EcoreExecutorType(WorkflowPackage.Literals.CONTROLFLOWLINK, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Datalink = new EcoreExecutorType(WorkflowPackage.Literals.DATALINK, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Decision = new EcoreExecutorType(WorkflowPackage.Literals.DECISION, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Duration = new EcoreExecutorType("Duration", PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Executionlink = new EcoreExecutorType(WorkflowPackage.Literals.EXECUTIONLINK, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _For = new EcoreExecutorType(WorkflowPackage.Literals.FOR, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Foreach = new EcoreExecutorType(WorkflowPackage.Literals.FOREACH, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Integer = new EcoreExecutorType(TypeId.INTEGER, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Localcanal = new EcoreExecutorType(WorkflowPackage.Literals.LOCALCANAL, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Loop = new EcoreExecutorType(WorkflowPackage.Literals.LOOP, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Loopiteration = new EcoreExecutorType(WorkflowPackage.Literals.LOOPITERATION, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Nesteddependency = new EcoreExecutorType(WorkflowPackage.Literals.NESTEDDEPENDENCY, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Networkcanal = new EcoreExecutorType(WorkflowPackage.Literals.NETWORKCANAL, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Parallelloop = new EcoreExecutorType(WorkflowPackage.Literals.PARALLELLOOP, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Path = new EcoreExecutorType("Path", PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Platformdependency = new EcoreExecutorType(WorkflowPackage.Literals.PLATFORMDEPENDENCY, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Profile = new EcoreExecutorType(WorkflowPackage.Literals.PROFILE, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Remotedatacanal = new EcoreExecutorType(WorkflowPackage.Literals.REMOTEDATACANAL, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Replica = new EcoreExecutorType(WorkflowPackage.Literals.REPLICA, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Shared = new EcoreExecutorType(WorkflowPackage.Literals.SHARED, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorEnumeration _Status = new EcoreExecutorEnumeration(WorkflowPackage.Literals.STATUS, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Storagecanal = new EcoreExecutorType(WorkflowPackage.Literals.STORAGECANAL, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _String = new EcoreExecutorType(TypeId.STRING, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Task = new EcoreExecutorType(WorkflowPackage.Literals.TASK, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Taskdependency = new EcoreExecutorType(WorkflowPackage.Literals.TASKDEPENDENCY, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Taskobservation = new EcoreExecutorType(WorkflowPackage.Literals.TASKOBSERVATION, PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _Timestamp = new EcoreExecutorType("Timestamp", PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _URI = new EcoreExecutorType("URI", PACKAGE, 0);
		public static final /*@NonNull*/ EcoreExecutorType _While = new EcoreExecutorType(WorkflowPackage.Literals.WHILE, PACKAGE, 0);

		private static final /*@NonNull*/ EcoreExecutorType /*@NonNull*/ [] types = {
			_Boolean,
			_Controlflowguard,
			_Controlflowlink,
			_Datalink,
			_Decision,
			_Duration,
			_Executionlink,
			_For,
			_Foreach,
			_Integer,
			_Localcanal,
			_Loop,
			_Loopiteration,
			_Nesteddependency,
			_Networkcanal,
			_Parallelloop,
			_Path,
			_Platformdependency,
			_Profile,
			_Remotedatacanal,
			_Replica,
			_Shared,
			_Status,
			_Storagecanal,
			_String,
			_Task,
			_Taskdependency,
			_Taskobservation,
			_Timestamp,
			_URI,
			_While
		};

		/*
		 *	Install the type descriptors in the package descriptor.
		 */
		static {
			PACKAGE.init(LIBRARY, types);
			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of WorkflowTables::Types and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The fragment descriptors for the local elements of each type and its supertypes.
	 */
	public static class Fragments {
		static {
			Init.initStart();
			Types.init();
		}

		private static final /*@NonNull*/ ExecutorFragment _Boolean__Boolean = new ExecutorFragment(Types._Boolean, WorkflowTables.Types._Boolean);
		private static final /*@NonNull*/ ExecutorFragment _Boolean__OclAny = new ExecutorFragment(Types._Boolean, OCLstdlibTables.Types._OclAny);

		private static final /*@NonNull*/ ExecutorFragment _Controlflowguard__Controlflowguard = new ExecutorFragment(Types._Controlflowguard, WorkflowTables.Types._Controlflowguard);
		private static final /*@NonNull*/ ExecutorFragment _Controlflowguard__MixinBase = new ExecutorFragment(Types._Controlflowguard, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Controlflowguard__OclAny = new ExecutorFragment(Types._Controlflowguard, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Controlflowguard__OclElement = new ExecutorFragment(Types._Controlflowguard, OCLstdlibTables.Types._OclElement);

		private static final /*@NonNull*/ ExecutorFragment _Controlflowlink__Controlflowlink = new ExecutorFragment(Types._Controlflowlink, WorkflowTables.Types._Controlflowlink);
		private static final /*@NonNull*/ ExecutorFragment _Controlflowlink__Entity = new ExecutorFragment(Types._Controlflowlink, OCCITables.Types._Entity);
		private static final /*@NonNull*/ ExecutorFragment _Controlflowlink__Link = new ExecutorFragment(Types._Controlflowlink, OCCITables.Types._Link);
		private static final /*@NonNull*/ ExecutorFragment _Controlflowlink__OclAny = new ExecutorFragment(Types._Controlflowlink, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Controlflowlink__OclElement = new ExecutorFragment(Types._Controlflowlink, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Controlflowlink__Taskdependency = new ExecutorFragment(Types._Controlflowlink, WorkflowTables.Types._Taskdependency);

		private static final /*@NonNull*/ ExecutorFragment _Datalink__Datalink = new ExecutorFragment(Types._Datalink, WorkflowTables.Types._Datalink);
		private static final /*@NonNull*/ ExecutorFragment _Datalink__Entity = new ExecutorFragment(Types._Datalink, OCCITables.Types._Entity);
		private static final /*@NonNull*/ ExecutorFragment _Datalink__Link = new ExecutorFragment(Types._Datalink, OCCITables.Types._Link);
		private static final /*@NonNull*/ ExecutorFragment _Datalink__OclAny = new ExecutorFragment(Types._Datalink, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Datalink__OclElement = new ExecutorFragment(Types._Datalink, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Datalink__Taskdependency = new ExecutorFragment(Types._Datalink, WorkflowTables.Types._Taskdependency);

		private static final /*@NonNull*/ ExecutorFragment _Decision__Decision = new ExecutorFragment(Types._Decision, WorkflowTables.Types._Decision);
		private static final /*@NonNull*/ ExecutorFragment _Decision__Entity = new ExecutorFragment(Types._Decision, OCCITables.Types._Entity);
		private static final /*@NonNull*/ ExecutorFragment _Decision__OclAny = new ExecutorFragment(Types._Decision, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Decision__OclElement = new ExecutorFragment(Types._Decision, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Decision__Resource = new ExecutorFragment(Types._Decision, OCCITables.Types._Resource);
		private static final /*@NonNull*/ ExecutorFragment _Decision__Task = new ExecutorFragment(Types._Decision, WorkflowTables.Types._Task);

		private static final /*@NonNull*/ ExecutorFragment _Duration__Duration = new ExecutorFragment(Types._Duration, WorkflowTables.Types._Duration);
		private static final /*@NonNull*/ ExecutorFragment _Duration__OclAny = new ExecutorFragment(Types._Duration, OCLstdlibTables.Types._OclAny);

		private static final /*@NonNull*/ ExecutorFragment _Executionlink__Entity = new ExecutorFragment(Types._Executionlink, OCCITables.Types._Entity);
		private static final /*@NonNull*/ ExecutorFragment _Executionlink__Executionlink = new ExecutorFragment(Types._Executionlink, WorkflowTables.Types._Executionlink);
		private static final /*@NonNull*/ ExecutorFragment _Executionlink__Link = new ExecutorFragment(Types._Executionlink, OCCITables.Types._Link);
		private static final /*@NonNull*/ ExecutorFragment _Executionlink__OclAny = new ExecutorFragment(Types._Executionlink, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Executionlink__OclElement = new ExecutorFragment(Types._Executionlink, OCLstdlibTables.Types._OclElement);

		private static final /*@NonNull*/ ExecutorFragment _For__For = new ExecutorFragment(Types._For, WorkflowTables.Types._For);
		private static final /*@NonNull*/ ExecutorFragment _For__MixinBase = new ExecutorFragment(Types._For, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _For__OclAny = new ExecutorFragment(Types._For, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _For__OclElement = new ExecutorFragment(Types._For, OCLstdlibTables.Types._OclElement);

		private static final /*@NonNull*/ ExecutorFragment _Foreach__Foreach = new ExecutorFragment(Types._Foreach, WorkflowTables.Types._Foreach);
		private static final /*@NonNull*/ ExecutorFragment _Foreach__MixinBase = new ExecutorFragment(Types._Foreach, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Foreach__OclAny = new ExecutorFragment(Types._Foreach, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Foreach__OclElement = new ExecutorFragment(Types._Foreach, OCLstdlibTables.Types._OclElement);

		private static final /*@NonNull*/ ExecutorFragment _Integer__Integer = new ExecutorFragment(Types._Integer, WorkflowTables.Types._Integer);
		private static final /*@NonNull*/ ExecutorFragment _Integer__OclAny = new ExecutorFragment(Types._Integer, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Integer__OclComparable = new ExecutorFragment(Types._Integer, OCLstdlibTables.Types._OclComparable);
		private static final /*@NonNull*/ ExecutorFragment _Integer__OclSummable = new ExecutorFragment(Types._Integer, OCLstdlibTables.Types._OclSummable);
		private static final /*@NonNull*/ ExecutorFragment _Integer__Real = new ExecutorFragment(Types._Integer, OCLstdlibTables.Types._Real);

		private static final /*@NonNull*/ ExecutorFragment _Localcanal__Localcanal = new ExecutorFragment(Types._Localcanal, WorkflowTables.Types._Localcanal);
		private static final /*@NonNull*/ ExecutorFragment _Localcanal__MixinBase = new ExecutorFragment(Types._Localcanal, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Localcanal__OclAny = new ExecutorFragment(Types._Localcanal, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Localcanal__OclElement = new ExecutorFragment(Types._Localcanal, OCLstdlibTables.Types._OclElement);

		private static final /*@NonNull*/ ExecutorFragment _Loop__Decision = new ExecutorFragment(Types._Loop, WorkflowTables.Types._Decision);
		private static final /*@NonNull*/ ExecutorFragment _Loop__Entity = new ExecutorFragment(Types._Loop, OCCITables.Types._Entity);
		private static final /*@NonNull*/ ExecutorFragment _Loop__Loop = new ExecutorFragment(Types._Loop, WorkflowTables.Types._Loop);
		private static final /*@NonNull*/ ExecutorFragment _Loop__OclAny = new ExecutorFragment(Types._Loop, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Loop__OclElement = new ExecutorFragment(Types._Loop, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Loop__Resource = new ExecutorFragment(Types._Loop, OCCITables.Types._Resource);
		private static final /*@NonNull*/ ExecutorFragment _Loop__Task = new ExecutorFragment(Types._Loop, WorkflowTables.Types._Task);

		private static final /*@NonNull*/ ExecutorFragment _Loopiteration__Loopiteration = new ExecutorFragment(Types._Loopiteration, WorkflowTables.Types._Loopiteration);
		private static final /*@NonNull*/ ExecutorFragment _Loopiteration__MixinBase = new ExecutorFragment(Types._Loopiteration, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Loopiteration__OclAny = new ExecutorFragment(Types._Loopiteration, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Loopiteration__OclElement = new ExecutorFragment(Types._Loopiteration, OCLstdlibTables.Types._OclElement);

		private static final /*@NonNull*/ ExecutorFragment _Nesteddependency__Entity = new ExecutorFragment(Types._Nesteddependency, OCCITables.Types._Entity);
		private static final /*@NonNull*/ ExecutorFragment _Nesteddependency__Link = new ExecutorFragment(Types._Nesteddependency, OCCITables.Types._Link);
		private static final /*@NonNull*/ ExecutorFragment _Nesteddependency__Nesteddependency = new ExecutorFragment(Types._Nesteddependency, WorkflowTables.Types._Nesteddependency);
		private static final /*@NonNull*/ ExecutorFragment _Nesteddependency__OclAny = new ExecutorFragment(Types._Nesteddependency, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Nesteddependency__OclElement = new ExecutorFragment(Types._Nesteddependency, OCLstdlibTables.Types._OclElement);

		private static final /*@NonNull*/ ExecutorFragment _Networkcanal__MixinBase = new ExecutorFragment(Types._Networkcanal, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Networkcanal__Networkcanal = new ExecutorFragment(Types._Networkcanal, WorkflowTables.Types._Networkcanal);
		private static final /*@NonNull*/ ExecutorFragment _Networkcanal__OclAny = new ExecutorFragment(Types._Networkcanal, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Networkcanal__OclElement = new ExecutorFragment(Types._Networkcanal, OCLstdlibTables.Types._OclElement);

		private static final /*@NonNull*/ ExecutorFragment _Parallelloop__MixinBase = new ExecutorFragment(Types._Parallelloop, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Parallelloop__OclAny = new ExecutorFragment(Types._Parallelloop, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Parallelloop__OclElement = new ExecutorFragment(Types._Parallelloop, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Parallelloop__Parallelloop = new ExecutorFragment(Types._Parallelloop, WorkflowTables.Types._Parallelloop);

		private static final /*@NonNull*/ ExecutorFragment _Path__OclAny = new ExecutorFragment(Types._Path, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Path__Path = new ExecutorFragment(Types._Path, WorkflowTables.Types._Path);

		private static final /*@NonNull*/ ExecutorFragment _Platformdependency__Entity = new ExecutorFragment(Types._Platformdependency, OCCITables.Types._Entity);
		private static final /*@NonNull*/ ExecutorFragment _Platformdependency__Link = new ExecutorFragment(Types._Platformdependency, OCCITables.Types._Link);
		private static final /*@NonNull*/ ExecutorFragment _Platformdependency__OclAny = new ExecutorFragment(Types._Platformdependency, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Platformdependency__OclElement = new ExecutorFragment(Types._Platformdependency, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Platformdependency__Platformdependency = new ExecutorFragment(Types._Platformdependency, WorkflowTables.Types._Platformdependency);

		private static final /*@NonNull*/ ExecutorFragment _Profile__MixinBase = new ExecutorFragment(Types._Profile, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Profile__OclAny = new ExecutorFragment(Types._Profile, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Profile__OclElement = new ExecutorFragment(Types._Profile, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Profile__Profile = new ExecutorFragment(Types._Profile, WorkflowTables.Types._Profile);

		private static final /*@NonNull*/ ExecutorFragment _Remotedatacanal__MixinBase = new ExecutorFragment(Types._Remotedatacanal, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Remotedatacanal__OclAny = new ExecutorFragment(Types._Remotedatacanal, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Remotedatacanal__OclElement = new ExecutorFragment(Types._Remotedatacanal, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Remotedatacanal__Remotedatacanal = new ExecutorFragment(Types._Remotedatacanal, WorkflowTables.Types._Remotedatacanal);

		private static final /*@NonNull*/ ExecutorFragment _Replica__MixinBase = new ExecutorFragment(Types._Replica, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Replica__OclAny = new ExecutorFragment(Types._Replica, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Replica__OclElement = new ExecutorFragment(Types._Replica, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Replica__Replica = new ExecutorFragment(Types._Replica, WorkflowTables.Types._Replica);

		private static final /*@NonNull*/ ExecutorFragment _Shared__MixinBase = new ExecutorFragment(Types._Shared, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Shared__OclAny = new ExecutorFragment(Types._Shared, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Shared__OclElement = new ExecutorFragment(Types._Shared, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Shared__Shared = new ExecutorFragment(Types._Shared, WorkflowTables.Types._Shared);

		private static final /*@NonNull*/ ExecutorFragment _Status__OclAny = new ExecutorFragment(Types._Status, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Status__OclElement = new ExecutorFragment(Types._Status, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Status__OclEnumeration = new ExecutorFragment(Types._Status, OCLstdlibTables.Types._OclEnumeration);
		private static final /*@NonNull*/ ExecutorFragment _Status__OclType = new ExecutorFragment(Types._Status, OCLstdlibTables.Types._OclType);
		private static final /*@NonNull*/ ExecutorFragment _Status__Status = new ExecutorFragment(Types._Status, WorkflowTables.Types._Status);

		private static final /*@NonNull*/ ExecutorFragment _Storagecanal__MixinBase = new ExecutorFragment(Types._Storagecanal, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Storagecanal__OclAny = new ExecutorFragment(Types._Storagecanal, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Storagecanal__OclElement = new ExecutorFragment(Types._Storagecanal, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Storagecanal__Storagecanal = new ExecutorFragment(Types._Storagecanal, WorkflowTables.Types._Storagecanal);

		private static final /*@NonNull*/ ExecutorFragment _String__OclAny = new ExecutorFragment(Types._String, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _String__OclComparable = new ExecutorFragment(Types._String, OCLstdlibTables.Types._OclComparable);
		private static final /*@NonNull*/ ExecutorFragment _String__OclSummable = new ExecutorFragment(Types._String, OCLstdlibTables.Types._OclSummable);
		private static final /*@NonNull*/ ExecutorFragment _String__String = new ExecutorFragment(Types._String, WorkflowTables.Types._String);

		private static final /*@NonNull*/ ExecutorFragment _Task__Entity = new ExecutorFragment(Types._Task, OCCITables.Types._Entity);
		private static final /*@NonNull*/ ExecutorFragment _Task__OclAny = new ExecutorFragment(Types._Task, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Task__OclElement = new ExecutorFragment(Types._Task, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Task__Resource = new ExecutorFragment(Types._Task, OCCITables.Types._Resource);
		private static final /*@NonNull*/ ExecutorFragment _Task__Task = new ExecutorFragment(Types._Task, WorkflowTables.Types._Task);

		private static final /*@NonNull*/ ExecutorFragment _Taskdependency__Entity = new ExecutorFragment(Types._Taskdependency, OCCITables.Types._Entity);
		private static final /*@NonNull*/ ExecutorFragment _Taskdependency__Link = new ExecutorFragment(Types._Taskdependency, OCCITables.Types._Link);
		private static final /*@NonNull*/ ExecutorFragment _Taskdependency__OclAny = new ExecutorFragment(Types._Taskdependency, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Taskdependency__OclElement = new ExecutorFragment(Types._Taskdependency, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Taskdependency__Taskdependency = new ExecutorFragment(Types._Taskdependency, WorkflowTables.Types._Taskdependency);

		private static final /*@NonNull*/ ExecutorFragment _Taskobservation__MixinBase = new ExecutorFragment(Types._Taskobservation, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _Taskobservation__OclAny = new ExecutorFragment(Types._Taskobservation, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Taskobservation__OclElement = new ExecutorFragment(Types._Taskobservation, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _Taskobservation__Taskobservation = new ExecutorFragment(Types._Taskobservation, WorkflowTables.Types._Taskobservation);

		private static final /*@NonNull*/ ExecutorFragment _Timestamp__OclAny = new ExecutorFragment(Types._Timestamp, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _Timestamp__Timestamp = new ExecutorFragment(Types._Timestamp, WorkflowTables.Types._Timestamp);

		private static final /*@NonNull*/ ExecutorFragment _URI__OclAny = new ExecutorFragment(Types._URI, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _URI__URI = new ExecutorFragment(Types._URI, WorkflowTables.Types._URI);

		private static final /*@NonNull*/ ExecutorFragment _While__MixinBase = new ExecutorFragment(Types._While, OCCITables.Types._MixinBase);
		private static final /*@NonNull*/ ExecutorFragment _While__OclAny = new ExecutorFragment(Types._While, OCLstdlibTables.Types._OclAny);
		private static final /*@NonNull*/ ExecutorFragment _While__OclElement = new ExecutorFragment(Types._While, OCLstdlibTables.Types._OclElement);
		private static final /*@NonNull*/ ExecutorFragment _While__While = new ExecutorFragment(Types._While, WorkflowTables.Types._While);

		static {
			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of WorkflowTables::Fragments and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The parameter lists shared by operations.
	 *
	 * @noextend This class is not intended to be subclassed by clients.
	 * @noinstantiate This class is not intended to be instantiated by clients.
	 * @noreference This class is not intended to be referenced by clients.
	 */
	public static class Parameters {
		static {
			Init.initStart();
			Fragments.init();
		}

		public static final /*@NonNull*/ ParameterTypes _Boolean = TypeUtil.createParameterTypes(OCLstdlibTables.Types._Boolean);
		public static final /*@NonNull*/ ParameterTypes _Integer = TypeUtil.createParameterTypes(OCLstdlibTables.Types._Integer);
		public static final /*@NonNull*/ ParameterTypes _Integer___Integer = TypeUtil.createParameterTypes(OCLstdlibTables.Types._Integer, OCLstdlibTables.Types._Integer);
		public static final /*@NonNull*/ ParameterTypes _OclAny___OclAny___OclAny___Integer___Boolean___Integer = TypeUtil.createParameterTypes(OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._Integer, OCLstdlibTables.Types._Boolean, OCLstdlibTables.Types._Integer);
		public static final /*@NonNull*/ ParameterTypes _OclAny___OclAny___OclAny___OclAny___String___Integer___OclAny___Integer = TypeUtil.createParameterTypes(OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._String, OCLstdlibTables.Types._Integer, OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._Integer);
		public static final /*@NonNull*/ ParameterTypes _OclSelf = TypeUtil.createParameterTypes(OCLstdlibTables.Types._OclSelf);
		public static final /*@NonNull*/ ParameterTypes _String = TypeUtil.createParameterTypes(OCLstdlibTables.Types._String);
		public static final /*@NonNull*/ ParameterTypes _String___Boolean = TypeUtil.createParameterTypes(OCLstdlibTables.Types._String, OCLstdlibTables.Types._Boolean);
		public static final /*@NonNull*/ ParameterTypes _String___String = TypeUtil.createParameterTypes(OCLstdlibTables.Types._String, OCLstdlibTables.Types._String);

		static {
			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of WorkflowTables::Parameters and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The operation descriptors for each operation of each type.
	 *
	 * @noextend This class is not intended to be subclassed by clients.
	 * @noinstantiate This class is not intended to be instantiated by clients.
	 * @noreference This class is not intended to be referenced by clients.
	 */
	public static class Operations {
		static {
			Init.initStart();
			Parameters.init();
		}

		public static final /*@NonNull*/ ExecutorOperation _Boolean___lt__gt_ = new ExecutorOperation("<>", Parameters._OclSelf, Types._Boolean,
			0, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.oclany.OclAnyNotEqualOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean___eq_ = new ExecutorOperation("=", Parameters._OclSelf, Types._Boolean,
			1, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.oclany.OclAnyEqualOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__and = new ExecutorOperation("and", Parameters._Boolean, Types._Boolean,
			2, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanAndOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__implies = new ExecutorOperation("implies", Parameters._Boolean, Types._Boolean,
			3, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanImpliesOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__not = new ExecutorOperation("not", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Boolean,
			4, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanNotOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__or = new ExecutorOperation("or", Parameters._Boolean, Types._Boolean,
			5, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanOrOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__xor = new ExecutorOperation("xor", Parameters._Boolean, Types._Boolean,
			6, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanXorOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__allInstances = new ExecutorOperation("allInstances", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Boolean,
			7, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanAllInstancesOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__and2 = new ExecutorOperation("and2", Parameters._Boolean, Types._Boolean,
			8, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanAndOperation2.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__implies2 = new ExecutorOperation("implies2", Parameters._Boolean, Types._Boolean,
			9, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanImpliesOperation2.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__not2 = new ExecutorOperation("not2", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Boolean,
			10, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanNotOperation2.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__or2 = new ExecutorOperation("or2", Parameters._Boolean, Types._Boolean,
			11, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanOrOperation2.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__toString = new ExecutorOperation("toString", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Boolean,
			12, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.oclany.OclAnyToStringOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Boolean__xor2 = new ExecutorOperation("xor2", Parameters._Boolean, Types._Boolean,
			13, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.logical.BooleanXorOperation2.INSTANCE);

		public static final /*@NonNull*/ ExecutorOperation _Datalink__schedule = new ExecutorOperation("schedule", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Datalink,
			0, TemplateParameters.EMPTY_LIST, null);
		public static final /*@NonNull*/ ExecutorOperation _Datalink__skip = new ExecutorOperation("skip", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Datalink,
			1, TemplateParameters.EMPTY_LIST, null);
		public static final /*@NonNull*/ ExecutorOperation _Datalink__start = new ExecutorOperation("start", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Datalink,
			2, TemplateParameters.EMPTY_LIST, null);
		public static final /*@NonNull*/ ExecutorOperation _Datalink__stop = new ExecutorOperation("stop", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Datalink,
			3, TemplateParameters.EMPTY_LIST, null);

		public static final /*@NonNull*/ ExecutorOperation _Integer___mul_ = new ExecutorOperation("*", Parameters._OclSelf, Types._Integer,
			0, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.NumericTimesOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer___add_ = new ExecutorOperation("+", Parameters._OclSelf, Types._Integer,
			1, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.NumericPlusOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer___neg_ = new ExecutorOperation("-", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Integer,
			2, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.NumericNegateOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer___sub_ = new ExecutorOperation("-", Parameters._OclSelf, Types._Integer,
			3, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.NumericMinusOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer___div_ = new ExecutorOperation("/", Parameters._OclSelf, Types._Integer,
			4, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.NumericDivideOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer__abs = new ExecutorOperation("abs", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Integer,
			5, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.NumericAbsOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer__div = new ExecutorOperation("div", Parameters._Integer, Types._Integer,
			6, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.NumericDivOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer__max = new ExecutorOperation("max", Parameters._OclSelf, Types._Integer,
			7, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.NumericMaxOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer__min = new ExecutorOperation("min", Parameters._OclSelf, Types._Integer,
			8, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.NumericMinOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer__mod = new ExecutorOperation("mod", Parameters._Integer, Types._Integer,
			9, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.NumericModOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer__toString = new ExecutorOperation("toString", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Integer,
			10, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.oclany.OclAnyToStringOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _Integer__toUnlimitedNatural = new ExecutorOperation("toUnlimitedNatural", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Integer,
			11, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.numeric.IntegerToUnlimitedNaturalOperation.INSTANCE);

		public static final /*@NonNull*/ ExecutorOperation _String___add_ = new ExecutorOperation("+", Parameters._String, Types._String,
			0, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringConcatOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String___lt_ = new ExecutorOperation("<", Parameters._OclSelf, Types._String,
			1, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringLessThanOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String___lt__eq_ = new ExecutorOperation("<=", Parameters._OclSelf, Types._String,
			2, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringLessThanEqualOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String___lt__gt_ = new ExecutorOperation("<>", Parameters._OclSelf, Types._String,
			3, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.oclany.OclAnyNotEqualOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String___eq_ = new ExecutorOperation("=", Parameters._OclSelf, Types._String,
			4, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.oclany.OclAnyEqualOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String___gt_ = new ExecutorOperation(">", Parameters._OclSelf, Types._String,
			5, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringGreaterThanOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String___gt__eq_ = new ExecutorOperation(">=", Parameters._OclSelf, Types._String,
			6, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringGreaterThanEqualOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__at = new ExecutorOperation("at", Parameters._Integer, Types._String,
			7, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringAtOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__characters = new ExecutorOperation("characters", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			8, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringCharactersOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__compareTo = new ExecutorOperation("compareTo", Parameters._OclSelf, Types._String,
			9, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringCompareToOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__concat = new ExecutorOperation("concat", Parameters._String, Types._String,
			10, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringConcatOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__endsWith = new ExecutorOperation("endsWith", Parameters._String, Types._String,
			11, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringEndsWithOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__equalsIgnoreCase = new ExecutorOperation("equalsIgnoreCase", Parameters._String, Types._String,
			12, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringEqualsIgnoreCaseOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__getSeverity = new ExecutorOperation("getSeverity", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			13, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.CGStringGetSeverityOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__indexOf = new ExecutorOperation("indexOf", Parameters._String, Types._String,
			14, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringIndexOfOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__lastIndexOf = new ExecutorOperation("lastIndexOf", Parameters._String, Types._String,
			15, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringLastIndexOfOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__0_logDiagnostic = new ExecutorOperation("logDiagnostic", Parameters._OclAny___OclAny___OclAny___Integer___Boolean___Integer, Types._String,
			16, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.CGStringLogDiagnosticOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__1_logDiagnostic = new ExecutorOperation("logDiagnostic", Parameters._OclAny___OclAny___OclAny___OclAny___String___Integer___OclAny___Integer, Types._String,
			17, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.CGStringLogDiagnosticOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__matches = new ExecutorOperation("matches", Parameters._String, Types._String,
			18, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringMatchesOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__replaceAll = new ExecutorOperation("replaceAll", Parameters._String___String, Types._String,
			19, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringReplaceAllOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__replaceFirst = new ExecutorOperation("replaceFirst", Parameters._String___String, Types._String,
			20, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringReplaceFirstOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__size = new ExecutorOperation("size", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			21, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringSizeOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__startsWith = new ExecutorOperation("startsWith", Parameters._String, Types._String,
			22, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringStartsWithOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__substituteAll = new ExecutorOperation("substituteAll", Parameters._String___String, Types._String,
			23, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringSubstituteAllOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__substituteFirst = new ExecutorOperation("substituteFirst", Parameters._String___String, Types._String,
			24, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringSubstituteFirstOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__substring = new ExecutorOperation("substring", Parameters._Integer___Integer, Types._String,
			25, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringSubstringOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__toBoolean = new ExecutorOperation("toBoolean", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			26, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToBooleanOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__toInteger = new ExecutorOperation("toInteger", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			27, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToIntegerOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__toLower = new ExecutorOperation("toLower", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			28, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToLowerCaseOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__toLowerCase = new ExecutorOperation("toLowerCase", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			29, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToLowerCaseOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__toReal = new ExecutorOperation("toReal", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			30, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToRealOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__toString = new ExecutorOperation("toString", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			31, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.oclany.OclAnyToStringOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__toUpper = new ExecutorOperation("toUpper", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			32, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToUpperCaseOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__toUpperCase = new ExecutorOperation("toUpperCase", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			33, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToUpperCaseOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__0_tokenize = new ExecutorOperation("tokenize", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			34, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringTokenizeOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__1_tokenize = new ExecutorOperation("tokenize", Parameters._String, Types._String,
			35, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringTokenizeOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__2_tokenize = new ExecutorOperation("tokenize", Parameters._String___Boolean, Types._String,
			36, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringTokenizeOperation.INSTANCE);
		public static final /*@NonNull*/ ExecutorOperation _String__trim = new ExecutorOperation("trim", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			37, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringTrimOperation.INSTANCE);

		public static final /*@NonNull*/ ExecutorOperation _Task__schedule = new ExecutorOperation("schedule", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Task,
			0, TemplateParameters.EMPTY_LIST, null);
		public static final /*@NonNull*/ ExecutorOperation _Task__skip = new ExecutorOperation("skip", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Task,
			1, TemplateParameters.EMPTY_LIST, null);
		public static final /*@NonNull*/ ExecutorOperation _Task__start = new ExecutorOperation("start", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Task,
			2, TemplateParameters.EMPTY_LIST, null);
		public static final /*@NonNull*/ ExecutorOperation _Task__stop = new ExecutorOperation("stop", TypeUtil.EMPTY_PARAMETER_TYPES, Types._Task,
			3, TemplateParameters.EMPTY_LIST, null);

		static {
			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of WorkflowTables::Operations and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The property descriptors for each property of each type.
	 *
	 * @noextend This class is not intended to be subclassed by clients.
	 * @noinstantiate This class is not intended to be instantiated by clients.
	 * @noreference This class is not intended to be referenced by clients.
	 */
	public static class Properties {
		static {
			Init.initStart();
			Operations.init();
		}


		public static final /*@NonNull*/ ExecutorProperty _Controlflowguard__controlflowGuard = new EcoreExecutorProperty(WorkflowPackage.Literals.CONTROLFLOWGUARD__CONTROLFLOW_GUARD, Types._Controlflowguard, 0);

		public static final /*@NonNull*/ ExecutorProperty _Datalink__taskDatalinkSourceFile = new EcoreExecutorProperty(WorkflowPackage.Literals.DATALINK__TASK_DATALINK_SOURCE_FILE, Types._Datalink, 0);
		public static final /*@NonNull*/ ExecutorProperty _Datalink__taskDatalinkState = new EcoreExecutorProperty(WorkflowPackage.Literals.DATALINK__TASK_DATALINK_STATE, Types._Datalink, 1);
		public static final /*@NonNull*/ ExecutorProperty _Datalink__taskDatalinkStateMessage = new EcoreExecutorProperty(WorkflowPackage.Literals.DATALINK__TASK_DATALINK_STATE_MESSAGE, Types._Datalink, 2);
		public static final /*@NonNull*/ ExecutorProperty _Datalink__taskDatalinkTargetFile = new EcoreExecutorProperty(WorkflowPackage.Literals.DATALINK__TASK_DATALINK_TARGET_FILE, Types._Datalink, 3);

		public static final /*@NonNull*/ ExecutorProperty _Decision__workflowDecisionExpression = new EcoreExecutorProperty(WorkflowPackage.Literals.DECISION__WORKFLOW_DECISION_EXPRESSION, Types._Decision, 0);
		public static final /*@NonNull*/ ExecutorProperty _Decision__workflowDecisionInput = new EcoreExecutorProperty(WorkflowPackage.Literals.DECISION__WORKFLOW_DECISION_INPUT, Types._Decision, 1);
		public static final /*@NonNull*/ ExecutorProperty _Decision__workflowDecisionResult = new EcoreExecutorProperty(WorkflowPackage.Literals.DECISION__WORKFLOW_DECISION_RESULT, Types._Decision, 2);

		public static final /*@NonNull*/ ExecutorProperty _For__loopIterationCounterIsIncrement = new EcoreExecutorProperty(WorkflowPackage.Literals.FOR__LOOP_ITERATION_COUNTER_IS_INCREMENT, Types._For, 0);

		public static final /*@NonNull*/ ExecutorProperty _Foreach__loopItemDelimiter = new EcoreExecutorProperty(WorkflowPackage.Literals.FOREACH__LOOP_ITEM_DELIMITER, Types._Foreach, 0);
		public static final /*@NonNull*/ ExecutorProperty _Foreach__loopItemName = new EcoreExecutorProperty(WorkflowPackage.Literals.FOREACH__LOOP_ITEM_NAME, Types._Foreach, 1);

		public static final /*@NonNull*/ ExecutorProperty _Loop__loopIterationCount = new EcoreExecutorProperty(WorkflowPackage.Literals.LOOP__LOOP_ITERATION_COUNT, Types._Loop, 0);

		public static final /*@NonNull*/ ExecutorProperty _Loopiteration__loopIterationVarName = new EcoreExecutorProperty(WorkflowPackage.Literals.LOOPITERATION__LOOP_ITERATION_VAR_NAME, Types._Loopiteration, 0);
		public static final /*@NonNull*/ ExecutorProperty _Loopiteration__loopIterationVarValue = new EcoreExecutorProperty(WorkflowPackage.Literals.LOOPITERATION__LOOP_ITERATION_VAR_VALUE, Types._Loopiteration, 1);

		public static final /*@NonNull*/ ExecutorProperty _Networkcanal__taskDatalinkNetwork = new EcoreExecutorProperty(WorkflowPackage.Literals.NETWORKCANAL__TASK_DATALINK_NETWORK, Types._Networkcanal, 0);

		public static final /*@NonNull*/ ExecutorProperty _Parallelloop__parallelReplicateNumber = new EcoreExecutorProperty(WorkflowPackage.Literals.PARALLELLOOP__PARALLEL_REPLICATE_NUMBER, Types._Parallelloop, 0);

		public static final /*@NonNull*/ ExecutorProperty _Profile__occiProfileDeploymenttime = new EcoreExecutorProperty(WorkflowPackage.Literals.PROFILE__OCCI_PROFILE_DEPLOYMENTTIME, Types._Profile, 0);
		public static final /*@NonNull*/ ExecutorProperty _Profile__occiProfileRuntime = new EcoreExecutorProperty(WorkflowPackage.Literals.PROFILE__OCCI_PROFILE_RUNTIME, Types._Profile, 1);

		public static final /*@NonNull*/ ExecutorProperty _Remotedatacanal__taskDatalinkSourceResource = new EcoreExecutorProperty(WorkflowPackage.Literals.REMOTEDATACANAL__TASK_DATALINK_SOURCE_RESOURCE, Types._Remotedatacanal, 0);
		public static final /*@NonNull*/ ExecutorProperty _Remotedatacanal__taskDatalinkTargetResource = new EcoreExecutorProperty(WorkflowPackage.Literals.REMOTEDATACANAL__TASK_DATALINK_TARGET_RESOURCE, Types._Remotedatacanal, 1);

		public static final /*@NonNull*/ ExecutorProperty _Replica__replicaGroup = new EcoreExecutorProperty(WorkflowPackage.Literals.REPLICA__REPLICA_GROUP, Types._Replica, 0);
		public static final /*@NonNull*/ ExecutorProperty _Replica__replicaSourceId = new EcoreExecutorProperty(WorkflowPackage.Literals.REPLICA__REPLICA_SOURCE_ID, Types._Replica, 1);

		public static final /*@NonNull*/ ExecutorProperty _Storagecanal__taskDatalinkStorage = new EcoreExecutorProperty(WorkflowPackage.Literals.STORAGECANAL__TASK_DATALINK_STORAGE, Types._Storagecanal, 0);

		public static final /*@NonNull*/ ExecutorProperty _Task__workflowTaskState = new EcoreExecutorProperty(WorkflowPackage.Literals.TASK__WORKFLOW_TASK_STATE, Types._Task, 0);
		public static final /*@NonNull*/ ExecutorProperty _Task__workflowTaskStateMessage = new EcoreExecutorProperty(WorkflowPackage.Literals.TASK__WORKFLOW_TASK_STATE_MESSAGE, Types._Task, 1);

		public static final /*@NonNull*/ ExecutorProperty _Taskdependency__taskDependencyIsloose = new EcoreExecutorProperty(WorkflowPackage.Literals.TASKDEPENDENCY__TASK_DEPENDENCY_ISLOOSE, Types._Taskdependency, 0);

		public static final /*@NonNull*/ ExecutorProperty _Taskobservation__taskObservation = new EcoreExecutorProperty(WorkflowPackage.Literals.TASKOBSERVATION__TASK_OBSERVATION, Types._Taskobservation, 0);
		public static final /*@NonNull*/ ExecutorProperty _Taskobservation__taskObservationFile = new EcoreExecutorProperty(WorkflowPackage.Literals.TASKOBSERVATION__TASK_OBSERVATION_FILE, Types._Taskobservation, 1);
		static {
			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of WorkflowTables::Properties and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The fragments for all base types in depth order: OclAny first, OclSelf last.
	 */
	public static class TypeFragments {
		static {
			Init.initStart();
			Properties.init();
		}

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Boolean =
			{
				Fragments._Boolean__OclAny /* 0 */,
				Fragments._Boolean__Boolean /* 1 */
			};
		private static final int /*@NonNull*/ [] __Boolean = { 1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Controlflowguard =
			{
				Fragments._Controlflowguard__OclAny /* 0 */,
				Fragments._Controlflowguard__OclElement /* 1 */,
				Fragments._Controlflowguard__MixinBase /* 2 */,
				Fragments._Controlflowguard__Controlflowguard /* 3 */
			};
		private static final int /*@NonNull*/ [] __Controlflowguard = { 1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Controlflowlink =
			{
				Fragments._Controlflowlink__OclAny /* 0 */,
				Fragments._Controlflowlink__OclElement /* 1 */,
				Fragments._Controlflowlink__Entity /* 2 */,
				Fragments._Controlflowlink__Link /* 3 */,
				Fragments._Controlflowlink__Taskdependency /* 4 */,
				Fragments._Controlflowlink__Controlflowlink /* 5 */
			};
		private static final int /*@NonNull*/ [] __Controlflowlink = { 1,1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Datalink =
			{
				Fragments._Datalink__OclAny /* 0 */,
				Fragments._Datalink__OclElement /* 1 */,
				Fragments._Datalink__Entity /* 2 */,
				Fragments._Datalink__Link /* 3 */,
				Fragments._Datalink__Taskdependency /* 4 */,
				Fragments._Datalink__Datalink /* 5 */
			};
		private static final int /*@NonNull*/ [] __Datalink = { 1,1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Decision =
			{
				Fragments._Decision__OclAny /* 0 */,
				Fragments._Decision__OclElement /* 1 */,
				Fragments._Decision__Entity /* 2 */,
				Fragments._Decision__Resource /* 3 */,
				Fragments._Decision__Task /* 4 */,
				Fragments._Decision__Decision /* 5 */
			};
		private static final int /*@NonNull*/ [] __Decision = { 1,1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Duration =
			{
				Fragments._Duration__OclAny /* 0 */,
				Fragments._Duration__Duration /* 1 */
			};
		private static final int /*@NonNull*/ [] __Duration = { 1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Executionlink =
			{
				Fragments._Executionlink__OclAny /* 0 */,
				Fragments._Executionlink__OclElement /* 1 */,
				Fragments._Executionlink__Entity /* 2 */,
				Fragments._Executionlink__Link /* 3 */,
				Fragments._Executionlink__Executionlink /* 4 */
			};
		private static final int /*@NonNull*/ [] __Executionlink = { 1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _For =
			{
				Fragments._For__OclAny /* 0 */,
				Fragments._For__OclElement /* 1 */,
				Fragments._For__MixinBase /* 2 */,
				Fragments._For__For /* 3 */
			};
		private static final int /*@NonNull*/ [] __For = { 1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Foreach =
			{
				Fragments._Foreach__OclAny /* 0 */,
				Fragments._Foreach__OclElement /* 1 */,
				Fragments._Foreach__MixinBase /* 2 */,
				Fragments._Foreach__Foreach /* 3 */
			};
		private static final int /*@NonNull*/ [] __Foreach = { 1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Integer =
			{
				Fragments._Integer__OclAny /* 0 */,
				Fragments._Integer__OclComparable /* 1 */,
				Fragments._Integer__OclSummable /* 1 */,
				Fragments._Integer__Real /* 2 */,
				Fragments._Integer__Integer /* 3 */
			};
		private static final int /*@NonNull*/ [] __Integer = { 1,2,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Localcanal =
			{
				Fragments._Localcanal__OclAny /* 0 */,
				Fragments._Localcanal__OclElement /* 1 */,
				Fragments._Localcanal__MixinBase /* 2 */,
				Fragments._Localcanal__Localcanal /* 3 */
			};
		private static final int /*@NonNull*/ [] __Localcanal = { 1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Loop =
			{
				Fragments._Loop__OclAny /* 0 */,
				Fragments._Loop__OclElement /* 1 */,
				Fragments._Loop__Entity /* 2 */,
				Fragments._Loop__Resource /* 3 */,
				Fragments._Loop__Task /* 4 */,
				Fragments._Loop__Decision /* 5 */,
				Fragments._Loop__Loop /* 6 */
			};
		private static final int /*@NonNull*/ [] __Loop = { 1,1,1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Loopiteration =
			{
				Fragments._Loopiteration__OclAny /* 0 */,
				Fragments._Loopiteration__OclElement /* 1 */,
				Fragments._Loopiteration__MixinBase /* 2 */,
				Fragments._Loopiteration__Loopiteration /* 3 */
			};
		private static final int /*@NonNull*/ [] __Loopiteration = { 1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Nesteddependency =
			{
				Fragments._Nesteddependency__OclAny /* 0 */,
				Fragments._Nesteddependency__OclElement /* 1 */,
				Fragments._Nesteddependency__Entity /* 2 */,
				Fragments._Nesteddependency__Link /* 3 */,
				Fragments._Nesteddependency__Nesteddependency /* 4 */
			};
		private static final int /*@NonNull*/ [] __Nesteddependency = { 1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Networkcanal =
			{
				Fragments._Networkcanal__OclAny /* 0 */,
				Fragments._Networkcanal__OclElement /* 1 */,
				Fragments._Networkcanal__MixinBase /* 2 */,
				Fragments._Networkcanal__Networkcanal /* 3 */
			};
		private static final int /*@NonNull*/ [] __Networkcanal = { 1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Parallelloop =
			{
				Fragments._Parallelloop__OclAny /* 0 */,
				Fragments._Parallelloop__OclElement /* 1 */,
				Fragments._Parallelloop__MixinBase /* 2 */,
				Fragments._Parallelloop__Parallelloop /* 3 */
			};
		private static final int /*@NonNull*/ [] __Parallelloop = { 1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Path =
			{
				Fragments._Path__OclAny /* 0 */,
				Fragments._Path__Path /* 1 */
			};
		private static final int /*@NonNull*/ [] __Path = { 1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Platformdependency =
			{
				Fragments._Platformdependency__OclAny /* 0 */,
				Fragments._Platformdependency__OclElement /* 1 */,
				Fragments._Platformdependency__Entity /* 2 */,
				Fragments._Platformdependency__Link /* 3 */,
				Fragments._Platformdependency__Platformdependency /* 4 */
			};
		private static final int /*@NonNull*/ [] __Platformdependency = { 1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Profile =
			{
				Fragments._Profile__OclAny /* 0 */,
				Fragments._Profile__OclElement /* 1 */,
				Fragments._Profile__MixinBase /* 2 */,
				Fragments._Profile__Profile /* 3 */
			};
		private static final int /*@NonNull*/ [] __Profile = { 1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Remotedatacanal =
			{
				Fragments._Remotedatacanal__OclAny /* 0 */,
				Fragments._Remotedatacanal__OclElement /* 1 */,
				Fragments._Remotedatacanal__MixinBase /* 2 */,
				Fragments._Remotedatacanal__Remotedatacanal /* 3 */
			};
		private static final int /*@NonNull*/ [] __Remotedatacanal = { 1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Replica =
			{
				Fragments._Replica__OclAny /* 0 */,
				Fragments._Replica__OclElement /* 1 */,
				Fragments._Replica__MixinBase /* 2 */,
				Fragments._Replica__Replica /* 3 */
			};
		private static final int /*@NonNull*/ [] __Replica = { 1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Shared =
			{
				Fragments._Shared__OclAny /* 0 */,
				Fragments._Shared__OclElement /* 1 */,
				Fragments._Shared__MixinBase /* 2 */,
				Fragments._Shared__Shared /* 3 */
			};
		private static final int /*@NonNull*/ [] __Shared = { 1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Status =
			{
				Fragments._Status__OclAny /* 0 */,
				Fragments._Status__OclElement /* 1 */,
				Fragments._Status__OclType /* 2 */,
				Fragments._Status__OclEnumeration /* 3 */,
				Fragments._Status__Status /* 4 */
			};
		private static final int /*@NonNull*/ [] __Status = { 1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Storagecanal =
			{
				Fragments._Storagecanal__OclAny /* 0 */,
				Fragments._Storagecanal__OclElement /* 1 */,
				Fragments._Storagecanal__MixinBase /* 2 */,
				Fragments._Storagecanal__Storagecanal /* 3 */
			};
		private static final int /*@NonNull*/ [] __Storagecanal = { 1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _String =
			{
				Fragments._String__OclAny /* 0 */,
				Fragments._String__OclComparable /* 1 */,
				Fragments._String__OclSummable /* 1 */,
				Fragments._String__String /* 2 */
			};
		private static final int /*@NonNull*/ [] __String = { 1,2,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Task =
			{
				Fragments._Task__OclAny /* 0 */,
				Fragments._Task__OclElement /* 1 */,
				Fragments._Task__Entity /* 2 */,
				Fragments._Task__Resource /* 3 */,
				Fragments._Task__Task /* 4 */
			};
		private static final int /*@NonNull*/ [] __Task = { 1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Taskdependency =
			{
				Fragments._Taskdependency__OclAny /* 0 */,
				Fragments._Taskdependency__OclElement /* 1 */,
				Fragments._Taskdependency__Entity /* 2 */,
				Fragments._Taskdependency__Link /* 3 */,
				Fragments._Taskdependency__Taskdependency /* 4 */
			};
		private static final int /*@NonNull*/ [] __Taskdependency = { 1,1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Taskobservation =
			{
				Fragments._Taskobservation__OclAny /* 0 */,
				Fragments._Taskobservation__OclElement /* 1 */,
				Fragments._Taskobservation__MixinBase /* 2 */,
				Fragments._Taskobservation__Taskobservation /* 3 */
			};
		private static final int /*@NonNull*/ [] __Taskobservation = { 1,1,1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _Timestamp =
			{
				Fragments._Timestamp__OclAny /* 0 */,
				Fragments._Timestamp__Timestamp /* 1 */
			};
		private static final int /*@NonNull*/ [] __Timestamp = { 1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _URI =
			{
				Fragments._URI__OclAny /* 0 */,
				Fragments._URI__URI /* 1 */
			};
		private static final int /*@NonNull*/ [] __URI = { 1,1 };

		private static final /*@NonNull*/ ExecutorFragment /*@NonNull*/ [] _While =
			{
				Fragments._While__OclAny /* 0 */,
				Fragments._While__OclElement /* 1 */,
				Fragments._While__MixinBase /* 2 */,
				Fragments._While__While /* 3 */
			};
		private static final int /*@NonNull*/ [] __While = { 1,1,1,1 };

		/**
		 *	Install the fragment descriptors in the class descriptors.
		 */
		static {
			Types._Boolean.initFragments(_Boolean, __Boolean);
			Types._Controlflowguard.initFragments(_Controlflowguard, __Controlflowguard);
			Types._Controlflowlink.initFragments(_Controlflowlink, __Controlflowlink);
			Types._Datalink.initFragments(_Datalink, __Datalink);
			Types._Decision.initFragments(_Decision, __Decision);
			Types._Duration.initFragments(_Duration, __Duration);
			Types._Executionlink.initFragments(_Executionlink, __Executionlink);
			Types._For.initFragments(_For, __For);
			Types._Foreach.initFragments(_Foreach, __Foreach);
			Types._Integer.initFragments(_Integer, __Integer);
			Types._Localcanal.initFragments(_Localcanal, __Localcanal);
			Types._Loop.initFragments(_Loop, __Loop);
			Types._Loopiteration.initFragments(_Loopiteration, __Loopiteration);
			Types._Nesteddependency.initFragments(_Nesteddependency, __Nesteddependency);
			Types._Networkcanal.initFragments(_Networkcanal, __Networkcanal);
			Types._Parallelloop.initFragments(_Parallelloop, __Parallelloop);
			Types._Path.initFragments(_Path, __Path);
			Types._Platformdependency.initFragments(_Platformdependency, __Platformdependency);
			Types._Profile.initFragments(_Profile, __Profile);
			Types._Remotedatacanal.initFragments(_Remotedatacanal, __Remotedatacanal);
			Types._Replica.initFragments(_Replica, __Replica);
			Types._Shared.initFragments(_Shared, __Shared);
			Types._Status.initFragments(_Status, __Status);
			Types._Storagecanal.initFragments(_Storagecanal, __Storagecanal);
			Types._String.initFragments(_String, __String);
			Types._Task.initFragments(_Task, __Task);
			Types._Taskdependency.initFragments(_Taskdependency, __Taskdependency);
			Types._Taskobservation.initFragments(_Taskobservation, __Taskobservation);
			Types._Timestamp.initFragments(_Timestamp, __Timestamp);
			Types._URI.initFragments(_URI, __URI);
			Types._While.initFragments(_While, __While);

			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of WorkflowTables::TypeFragments and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The lists of local operations or local operation overrides for each fragment of each type.
	 */
	public static class FragmentOperations {
		static {
			Init.initStart();
			TypeFragments.init();
		}

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Boolean__Boolean = {
			OCLstdlibTables.Operations._Boolean___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Boolean___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._Boolean__and /* _'and'(Boolean[?]) */,
			OCLstdlibTables.Operations._Boolean__implies /* _'implies'(Boolean[?]) */,
			OCLstdlibTables.Operations._Boolean__not /* _'not'() */,
			OCLstdlibTables.Operations._Boolean__or /* _'or'(Boolean[?]) */,
			OCLstdlibTables.Operations._Boolean__xor /* _'xor'(Boolean[?]) */,
			OCLstdlibTables.Operations._Boolean__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._Boolean__and2 /* and2(Boolean[?]) */,
			OCLstdlibTables.Operations._Boolean__implies2 /* implies2(Boolean[?]) */,
			OCLstdlibTables.Operations._Boolean__not2 /* not2() */,
			OCLstdlibTables.Operations._Boolean__or2 /* or2(Boolean[?]) */,
			OCLstdlibTables.Operations._Boolean__toString /* toString() */,
			OCLstdlibTables.Operations._Boolean__xor2 /* xor2(Boolean[?]) */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Boolean__OclAny = {
			OCLstdlibTables.Operations._Boolean___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Boolean___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._Boolean__toString /* toString() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Controlflowguard__Controlflowguard = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Controlflowguard__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Controlflowguard__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Controlflowguard__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Controlflowlink__Controlflowlink = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Controlflowlink__Entity = {
			OCCITables.Operations._Entity__occiCreate /* occiCreate() */,
			OCCITables.Operations._Entity__occiDelete /* occiDelete() */,
			OCCITables.Operations._Entity__occiRetrieve /* occiRetrieve() */,
			OCCITables.Operations._Entity__occiUpdate /* occiUpdate() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Controlflowlink__Link = {
			OCCITables.Operations._Link__LinkSourceInvariant /* LinkSourceInvariant(Kind[?],Kind[?]) */,
			OCCITables.Operations._Link__LinkTargetInvariant /* LinkTargetInvariant(Kind[?],Kind[?]) */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Controlflowlink__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Controlflowlink__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Controlflowlink__Taskdependency = {};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Datalink__Datalink = {
			WorkflowTables.Operations._Datalink__schedule /* schedule() */,
			WorkflowTables.Operations._Datalink__skip /* skip() */,
			WorkflowTables.Operations._Datalink__start /* start() */,
			WorkflowTables.Operations._Datalink__stop /* stop() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Datalink__Entity = {
			OCCITables.Operations._Entity__occiCreate /* occiCreate() */,
			OCCITables.Operations._Entity__occiDelete /* occiDelete() */,
			OCCITables.Operations._Entity__occiRetrieve /* occiRetrieve() */,
			OCCITables.Operations._Entity__occiUpdate /* occiUpdate() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Datalink__Link = {
			OCCITables.Operations._Link__LinkSourceInvariant /* LinkSourceInvariant(Kind[?],Kind[?]) */,
			OCCITables.Operations._Link__LinkTargetInvariant /* LinkTargetInvariant(Kind[?],Kind[?]) */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Datalink__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Datalink__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Datalink__Taskdependency = {};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Decision__Decision = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Decision__Entity = {
			OCCITables.Operations._Entity__occiCreate /* occiCreate() */,
			OCCITables.Operations._Entity__occiDelete /* occiDelete() */,
			OCCITables.Operations._Entity__occiRetrieve /* occiRetrieve() */,
			OCCITables.Operations._Entity__occiUpdate /* occiUpdate() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Decision__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Decision__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Decision__Resource = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Decision__Task = {
			WorkflowTables.Operations._Task__schedule /* schedule() */,
			WorkflowTables.Operations._Task__skip /* skip() */,
			WorkflowTables.Operations._Task__start /* start() */,
			WorkflowTables.Operations._Task__stop /* stop() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Duration__Duration = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Duration__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Executionlink__Executionlink = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Executionlink__Entity = {
			OCCITables.Operations._Entity__occiCreate /* occiCreate() */,
			OCCITables.Operations._Entity__occiDelete /* occiDelete() */,
			OCCITables.Operations._Entity__occiRetrieve /* occiRetrieve() */,
			OCCITables.Operations._Entity__occiUpdate /* occiUpdate() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Executionlink__Link = {
			OCCITables.Operations._Link__LinkSourceInvariant /* LinkSourceInvariant(Kind[?],Kind[?]) */,
			OCCITables.Operations._Link__LinkTargetInvariant /* LinkTargetInvariant(Kind[?],Kind[?]) */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Executionlink__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Executionlink__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _For__For = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _For__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _For__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _For__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Foreach__Foreach = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Foreach__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Foreach__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Foreach__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Integer__Integer = {
			OCLstdlibTables.Operations._Integer___mul_ /* _'*'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer___add_ /* _'+'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer___neg_ /* _'-'() */,
			OCLstdlibTables.Operations._Integer___sub_ /* _'-'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer___div_ /* _'/'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer__abs /* abs() */,
			OCLstdlibTables.Operations._Integer__div /* div(Integer[?]) */,
			OCLstdlibTables.Operations._Integer__max /* max(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer__min /* min(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer__mod /* mod(Integer[?]) */,
			OCLstdlibTables.Operations._Integer__toString /* toString() */,
			OCLstdlibTables.Operations._Integer__toUnlimitedNatural /* toUnlimitedNatural() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Integer__OclAny = {
			OCLstdlibTables.Operations._Real___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Real___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._Integer__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Integer__OclComparable = {
			OCLstdlibTables.Operations._OclComparable___lt_ /* _'<'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclComparable___lt__eq_ /* _'<='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclComparable___gt_ /* _'>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclComparable___gt__eq_ /* _'>='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclComparable__compareTo /* compareTo(OclSelf[?]) */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Integer__OclSummable = {
			OCLstdlibTables.Operations._OclSummable__sum /* sum(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclSummable__zero /* zero() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Integer__Real = {
			OCLstdlibTables.Operations._Integer___mul_ /* _'*'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer___add_ /* _'+'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer___neg_ /* _'-'() */,
			OCLstdlibTables.Operations._Integer___sub_ /* _'-'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer___div_ /* _'/'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Real___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._Real___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer__abs /* abs() */,
			OCLstdlibTables.Operations._Real__floor /* floor() */,
			OCLstdlibTables.Operations._Integer__max /* max(OclSelf[?]) */,
			OCLstdlibTables.Operations._Integer__min /* min(OclSelf[?]) */,
			OCLstdlibTables.Operations._Real__round /* round() */,
			OCLstdlibTables.Operations._Integer__toString /* toString() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Localcanal__Localcanal = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Localcanal__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Localcanal__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Localcanal__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Loop__Loop = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Loop__Decision = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Loop__Entity = {
			OCCITables.Operations._Entity__occiCreate /* occiCreate() */,
			OCCITables.Operations._Entity__occiDelete /* occiDelete() */,
			OCCITables.Operations._Entity__occiRetrieve /* occiRetrieve() */,
			OCCITables.Operations._Entity__occiUpdate /* occiUpdate() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Loop__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Loop__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Loop__Resource = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Loop__Task = {
			WorkflowTables.Operations._Task__schedule /* schedule() */,
			WorkflowTables.Operations._Task__skip /* skip() */,
			WorkflowTables.Operations._Task__start /* start() */,
			WorkflowTables.Operations._Task__stop /* stop() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Loopiteration__Loopiteration = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Loopiteration__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Loopiteration__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Loopiteration__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Nesteddependency__Nesteddependency = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Nesteddependency__Entity = {
			OCCITables.Operations._Entity__occiCreate /* occiCreate() */,
			OCCITables.Operations._Entity__occiDelete /* occiDelete() */,
			OCCITables.Operations._Entity__occiRetrieve /* occiRetrieve() */,
			OCCITables.Operations._Entity__occiUpdate /* occiUpdate() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Nesteddependency__Link = {
			OCCITables.Operations._Link__LinkSourceInvariant /* LinkSourceInvariant(Kind[?],Kind[?]) */,
			OCCITables.Operations._Link__LinkTargetInvariant /* LinkTargetInvariant(Kind[?],Kind[?]) */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Nesteddependency__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Nesteddependency__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Networkcanal__Networkcanal = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Networkcanal__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Networkcanal__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Networkcanal__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Parallelloop__Parallelloop = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Parallelloop__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Parallelloop__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Parallelloop__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Path__Path = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Path__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Platformdependency__Platformdependency = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Platformdependency__Entity = {
			OCCITables.Operations._Entity__occiCreate /* occiCreate() */,
			OCCITables.Operations._Entity__occiDelete /* occiDelete() */,
			OCCITables.Operations._Entity__occiRetrieve /* occiRetrieve() */,
			OCCITables.Operations._Entity__occiUpdate /* occiUpdate() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Platformdependency__Link = {
			OCCITables.Operations._Link__LinkSourceInvariant /* LinkSourceInvariant(Kind[?],Kind[?]) */,
			OCCITables.Operations._Link__LinkTargetInvariant /* LinkTargetInvariant(Kind[?],Kind[?]) */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Platformdependency__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Platformdependency__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Profile__Profile = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Profile__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Profile__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Profile__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Remotedatacanal__Remotedatacanal = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Remotedatacanal__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Remotedatacanal__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Remotedatacanal__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Replica__Replica = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Replica__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Replica__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Replica__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Shared__Shared = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Shared__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Shared__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Shared__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Status__Status = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Status__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Status__OclElement = {
			OCLstdlibTables.Operations._OclEnumeration__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Status__OclEnumeration = {
			OCLstdlibTables.Operations._OclEnumeration__allInstances /* allInstances() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Status__OclType = {
			OCLstdlibTables.Operations._OclType__conformsTo /* conformsTo(OclType[?]) */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Storagecanal__Storagecanal = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Storagecanal__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Storagecanal__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Storagecanal__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _String__String = {
			OCLstdlibTables.Operations._String___add_ /* _'+'(String[?]) */,
			OCLstdlibTables.Operations._String___lt_ /* _'<'(OclSelf[?]) */,
			OCLstdlibTables.Operations._String___lt__eq_ /* _'<='(OclSelf[?]) */,
			OCLstdlibTables.Operations._String___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._String___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._String___gt_ /* _'>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._String___gt__eq_ /* _'>='(OclSelf[?]) */,
			OCLstdlibTables.Operations._String__at /* at(Integer[?]) */,
			OCLstdlibTables.Operations._String__characters /* characters() */,
			OCLstdlibTables.Operations._String__compareTo /* compareTo(OclSelf[?]) */,
			OCLstdlibTables.Operations._String__concat /* concat(String[?]) */,
			OCLstdlibTables.Operations._String__endsWith /* endsWith(String[?]) */,
			OCLstdlibTables.Operations._String__equalsIgnoreCase /* equalsIgnoreCase(String[?]) */,
			//OCLstdlibTables.Operations._String__getSeverity /* getSeverity() */,
			OCLstdlibTables.Operations._String__indexOf /* indexOf(String[?]) */,
			OCLstdlibTables.Operations._String__lastIndexOf /* lastIndexOf(String[?]) */,
			//OCLstdlibTables.Operations._String__0_logDiagnostic /* logDiagnostic(OclAny[1],OclAny[?],OclAny[?],Integer[1],Boolean[?],Integer[1]) */,
			//OCLstdlibTables.Operations._String__1_logDiagnostic /* logDiagnostic(OclAny[1],OclAny[?],OclAny[?],OclAny[?],String[?],Integer[1],OclAny[?],Integer[1]) */,
			OCLstdlibTables.Operations._String__matches /* matches(String[?]) */,
			OCLstdlibTables.Operations._String__replaceAll /* replaceAll(String[?],String[?]) */,
			OCLstdlibTables.Operations._String__replaceFirst /* replaceFirst(String[?],String[?]) */,
			OCLstdlibTables.Operations._String__size /* size() */,
			OCLstdlibTables.Operations._String__startsWith /* startsWith(String[?]) */,
			OCLstdlibTables.Operations._String__substituteAll /* substituteAll(String[?],String[?]) */,
			OCLstdlibTables.Operations._String__substituteFirst /* substituteFirst(String[?],String[?]) */,
			OCLstdlibTables.Operations._String__substring /* substring(Integer[?],Integer[?]) */,
			OCLstdlibTables.Operations._String__toBoolean /* toBoolean() */,
			OCLstdlibTables.Operations._String__toInteger /* toInteger() */,
			OCLstdlibTables.Operations._String__toLower /* toLower() */,
			OCLstdlibTables.Operations._String__toLowerCase /* toLowerCase() */,
			OCLstdlibTables.Operations._String__toReal /* toReal() */,
			OCLstdlibTables.Operations._String__toString /* toString() */,
			OCLstdlibTables.Operations._String__toUpper /* toUpper() */,
			OCLstdlibTables.Operations._String__toUpperCase /* toUpperCase() */,
			OCLstdlibTables.Operations._String__0_tokenize /* tokenize() */,
			OCLstdlibTables.Operations._String__1_tokenize /* tokenize(String[?]) */,
			OCLstdlibTables.Operations._String__2_tokenize /* tokenize(String[?],Boolean[?]) */,
			OCLstdlibTables.Operations._String__trim /* trim() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _String__OclAny = {
			OCLstdlibTables.Operations._String___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._String___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._String__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _String__OclComparable = {
			OCLstdlibTables.Operations._String___lt_ /* _'<'(OclSelf[?]) */,
			OCLstdlibTables.Operations._String___lt__eq_ /* _'<='(OclSelf[?]) */,
			OCLstdlibTables.Operations._String___gt_ /* _'>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._String___gt__eq_ /* _'>='(OclSelf[?]) */,
			OCLstdlibTables.Operations._String__compareTo /* compareTo(OclSelf[?]) */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _String__OclSummable = {
			OCLstdlibTables.Operations._OclSummable__sum /* sum(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclSummable__zero /* zero() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Task__Task = {
			WorkflowTables.Operations._Task__schedule /* schedule() */,
			WorkflowTables.Operations._Task__skip /* skip() */,
			WorkflowTables.Operations._Task__start /* start() */,
			WorkflowTables.Operations._Task__stop /* stop() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Task__Entity = {
			OCCITables.Operations._Entity__occiCreate /* occiCreate() */,
			OCCITables.Operations._Entity__occiDelete /* occiDelete() */,
			OCCITables.Operations._Entity__occiRetrieve /* occiRetrieve() */,
			OCCITables.Operations._Entity__occiUpdate /* occiUpdate() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Task__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Task__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Task__Resource = {};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Taskdependency__Taskdependency = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Taskdependency__Entity = {
			OCCITables.Operations._Entity__occiCreate /* occiCreate() */,
			OCCITables.Operations._Entity__occiDelete /* occiDelete() */,
			OCCITables.Operations._Entity__occiRetrieve /* occiRetrieve() */,
			OCCITables.Operations._Entity__occiUpdate /* occiUpdate() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Taskdependency__Link = {
			OCCITables.Operations._Link__LinkSourceInvariant /* LinkSourceInvariant(Kind[?],Kind[?]) */,
			OCCITables.Operations._Link__LinkTargetInvariant /* LinkTargetInvariant(Kind[?],Kind[?]) */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Taskdependency__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Taskdependency__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Taskobservation__Taskobservation = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Taskobservation__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Taskobservation__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Taskobservation__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Timestamp__Timestamp = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _Timestamp__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _URI__URI = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _URI__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};

		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _While__While = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _While__MixinBase = {};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _While__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[?]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final /*@NonNull*/ ExecutorOperation /*@NonNull*/ [] _While__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[?]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[?]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		/*
		 *	Install the operation descriptors in the fragment descriptors.
		 */
		static {
			Fragments._Boolean__Boolean.initOperations(_Boolean__Boolean);
			Fragments._Boolean__OclAny.initOperations(_Boolean__OclAny);

			Fragments._Controlflowguard__Controlflowguard.initOperations(_Controlflowguard__Controlflowguard);
			Fragments._Controlflowguard__MixinBase.initOperations(_Controlflowguard__MixinBase);
			Fragments._Controlflowguard__OclAny.initOperations(_Controlflowguard__OclAny);
			Fragments._Controlflowguard__OclElement.initOperations(_Controlflowguard__OclElement);

			Fragments._Controlflowlink__Controlflowlink.initOperations(_Controlflowlink__Controlflowlink);
			Fragments._Controlflowlink__Entity.initOperations(_Controlflowlink__Entity);
			Fragments._Controlflowlink__Link.initOperations(_Controlflowlink__Link);
			Fragments._Controlflowlink__OclAny.initOperations(_Controlflowlink__OclAny);
			Fragments._Controlflowlink__OclElement.initOperations(_Controlflowlink__OclElement);
			Fragments._Controlflowlink__Taskdependency.initOperations(_Controlflowlink__Taskdependency);

			Fragments._Datalink__Datalink.initOperations(_Datalink__Datalink);
			Fragments._Datalink__Entity.initOperations(_Datalink__Entity);
			Fragments._Datalink__Link.initOperations(_Datalink__Link);
			Fragments._Datalink__OclAny.initOperations(_Datalink__OclAny);
			Fragments._Datalink__OclElement.initOperations(_Datalink__OclElement);
			Fragments._Datalink__Taskdependency.initOperations(_Datalink__Taskdependency);

			Fragments._Decision__Decision.initOperations(_Decision__Decision);
			Fragments._Decision__Entity.initOperations(_Decision__Entity);
			Fragments._Decision__OclAny.initOperations(_Decision__OclAny);
			Fragments._Decision__OclElement.initOperations(_Decision__OclElement);
			Fragments._Decision__Resource.initOperations(_Decision__Resource);
			Fragments._Decision__Task.initOperations(_Decision__Task);

			Fragments._Duration__Duration.initOperations(_Duration__Duration);
			Fragments._Duration__OclAny.initOperations(_Duration__OclAny);

			Fragments._Executionlink__Entity.initOperations(_Executionlink__Entity);
			Fragments._Executionlink__Executionlink.initOperations(_Executionlink__Executionlink);
			Fragments._Executionlink__Link.initOperations(_Executionlink__Link);
			Fragments._Executionlink__OclAny.initOperations(_Executionlink__OclAny);
			Fragments._Executionlink__OclElement.initOperations(_Executionlink__OclElement);

			Fragments._For__For.initOperations(_For__For);
			Fragments._For__MixinBase.initOperations(_For__MixinBase);
			Fragments._For__OclAny.initOperations(_For__OclAny);
			Fragments._For__OclElement.initOperations(_For__OclElement);

			Fragments._Foreach__Foreach.initOperations(_Foreach__Foreach);
			Fragments._Foreach__MixinBase.initOperations(_Foreach__MixinBase);
			Fragments._Foreach__OclAny.initOperations(_Foreach__OclAny);
			Fragments._Foreach__OclElement.initOperations(_Foreach__OclElement);

			Fragments._Integer__Integer.initOperations(_Integer__Integer);
			Fragments._Integer__OclAny.initOperations(_Integer__OclAny);
			Fragments._Integer__OclComparable.initOperations(_Integer__OclComparable);
			Fragments._Integer__OclSummable.initOperations(_Integer__OclSummable);
			Fragments._Integer__Real.initOperations(_Integer__Real);

			Fragments._Localcanal__Localcanal.initOperations(_Localcanal__Localcanal);
			Fragments._Localcanal__MixinBase.initOperations(_Localcanal__MixinBase);
			Fragments._Localcanal__OclAny.initOperations(_Localcanal__OclAny);
			Fragments._Localcanal__OclElement.initOperations(_Localcanal__OclElement);

			Fragments._Loop__Decision.initOperations(_Loop__Decision);
			Fragments._Loop__Entity.initOperations(_Loop__Entity);
			Fragments._Loop__Loop.initOperations(_Loop__Loop);
			Fragments._Loop__OclAny.initOperations(_Loop__OclAny);
			Fragments._Loop__OclElement.initOperations(_Loop__OclElement);
			Fragments._Loop__Resource.initOperations(_Loop__Resource);
			Fragments._Loop__Task.initOperations(_Loop__Task);

			Fragments._Loopiteration__Loopiteration.initOperations(_Loopiteration__Loopiteration);
			Fragments._Loopiteration__MixinBase.initOperations(_Loopiteration__MixinBase);
			Fragments._Loopiteration__OclAny.initOperations(_Loopiteration__OclAny);
			Fragments._Loopiteration__OclElement.initOperations(_Loopiteration__OclElement);

			Fragments._Nesteddependency__Entity.initOperations(_Nesteddependency__Entity);
			Fragments._Nesteddependency__Link.initOperations(_Nesteddependency__Link);
			Fragments._Nesteddependency__Nesteddependency.initOperations(_Nesteddependency__Nesteddependency);
			Fragments._Nesteddependency__OclAny.initOperations(_Nesteddependency__OclAny);
			Fragments._Nesteddependency__OclElement.initOperations(_Nesteddependency__OclElement);

			Fragments._Networkcanal__MixinBase.initOperations(_Networkcanal__MixinBase);
			Fragments._Networkcanal__Networkcanal.initOperations(_Networkcanal__Networkcanal);
			Fragments._Networkcanal__OclAny.initOperations(_Networkcanal__OclAny);
			Fragments._Networkcanal__OclElement.initOperations(_Networkcanal__OclElement);

			Fragments._Parallelloop__MixinBase.initOperations(_Parallelloop__MixinBase);
			Fragments._Parallelloop__OclAny.initOperations(_Parallelloop__OclAny);
			Fragments._Parallelloop__OclElement.initOperations(_Parallelloop__OclElement);
			Fragments._Parallelloop__Parallelloop.initOperations(_Parallelloop__Parallelloop);

			Fragments._Path__OclAny.initOperations(_Path__OclAny);
			Fragments._Path__Path.initOperations(_Path__Path);

			Fragments._Platformdependency__Entity.initOperations(_Platformdependency__Entity);
			Fragments._Platformdependency__Link.initOperations(_Platformdependency__Link);
			Fragments._Platformdependency__OclAny.initOperations(_Platformdependency__OclAny);
			Fragments._Platformdependency__OclElement.initOperations(_Platformdependency__OclElement);
			Fragments._Platformdependency__Platformdependency.initOperations(_Platformdependency__Platformdependency);

			Fragments._Profile__MixinBase.initOperations(_Profile__MixinBase);
			Fragments._Profile__OclAny.initOperations(_Profile__OclAny);
			Fragments._Profile__OclElement.initOperations(_Profile__OclElement);
			Fragments._Profile__Profile.initOperations(_Profile__Profile);

			Fragments._Remotedatacanal__MixinBase.initOperations(_Remotedatacanal__MixinBase);
			Fragments._Remotedatacanal__OclAny.initOperations(_Remotedatacanal__OclAny);
			Fragments._Remotedatacanal__OclElement.initOperations(_Remotedatacanal__OclElement);
			Fragments._Remotedatacanal__Remotedatacanal.initOperations(_Remotedatacanal__Remotedatacanal);

			Fragments._Replica__MixinBase.initOperations(_Replica__MixinBase);
			Fragments._Replica__OclAny.initOperations(_Replica__OclAny);
			Fragments._Replica__OclElement.initOperations(_Replica__OclElement);
			Fragments._Replica__Replica.initOperations(_Replica__Replica);

			Fragments._Shared__MixinBase.initOperations(_Shared__MixinBase);
			Fragments._Shared__OclAny.initOperations(_Shared__OclAny);
			Fragments._Shared__OclElement.initOperations(_Shared__OclElement);
			Fragments._Shared__Shared.initOperations(_Shared__Shared);

			Fragments._Status__OclAny.initOperations(_Status__OclAny);
			Fragments._Status__OclElement.initOperations(_Status__OclElement);
			Fragments._Status__OclEnumeration.initOperations(_Status__OclEnumeration);
			Fragments._Status__OclType.initOperations(_Status__OclType);
			Fragments._Status__Status.initOperations(_Status__Status);

			Fragments._Storagecanal__MixinBase.initOperations(_Storagecanal__MixinBase);
			Fragments._Storagecanal__OclAny.initOperations(_Storagecanal__OclAny);
			Fragments._Storagecanal__OclElement.initOperations(_Storagecanal__OclElement);
			Fragments._Storagecanal__Storagecanal.initOperations(_Storagecanal__Storagecanal);

			Fragments._String__OclAny.initOperations(_String__OclAny);
			Fragments._String__OclComparable.initOperations(_String__OclComparable);
			Fragments._String__OclSummable.initOperations(_String__OclSummable);
			Fragments._String__String.initOperations(_String__String);

			Fragments._Task__Entity.initOperations(_Task__Entity);
			Fragments._Task__OclAny.initOperations(_Task__OclAny);
			Fragments._Task__OclElement.initOperations(_Task__OclElement);
			Fragments._Task__Resource.initOperations(_Task__Resource);
			Fragments._Task__Task.initOperations(_Task__Task);

			Fragments._Taskdependency__Entity.initOperations(_Taskdependency__Entity);
			Fragments._Taskdependency__Link.initOperations(_Taskdependency__Link);
			Fragments._Taskdependency__OclAny.initOperations(_Taskdependency__OclAny);
			Fragments._Taskdependency__OclElement.initOperations(_Taskdependency__OclElement);
			Fragments._Taskdependency__Taskdependency.initOperations(_Taskdependency__Taskdependency);

			Fragments._Taskobservation__MixinBase.initOperations(_Taskobservation__MixinBase);
			Fragments._Taskobservation__OclAny.initOperations(_Taskobservation__OclAny);
			Fragments._Taskobservation__OclElement.initOperations(_Taskobservation__OclElement);
			Fragments._Taskobservation__Taskobservation.initOperations(_Taskobservation__Taskobservation);

			Fragments._Timestamp__OclAny.initOperations(_Timestamp__OclAny);
			Fragments._Timestamp__Timestamp.initOperations(_Timestamp__Timestamp);

			Fragments._URI__OclAny.initOperations(_URI__OclAny);
			Fragments._URI__URI.initOperations(_URI__URI);

			Fragments._While__MixinBase.initOperations(_While__MixinBase);
			Fragments._While__OclAny.initOperations(_While__OclAny);
			Fragments._While__OclElement.initOperations(_While__OclElement);
			Fragments._While__While.initOperations(_While__While);

			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of WorkflowTables::FragmentOperations and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The lists of local properties for the local fragment of each type.
	 */
	public static class FragmentProperties {
		static {
			Init.initStart();
			FragmentOperations.init();
		}

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Boolean = {};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Controlflowguard = {
			WorkflowTables.Properties._Controlflowguard__controlflowGuard
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Controlflowlink = {
			WorkflowTables.Properties._Taskdependency__taskDependencyIsloose
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Datalink = {
			WorkflowTables.Properties._Datalink__taskDatalinkSourceFile,
			WorkflowTables.Properties._Datalink__taskDatalinkState,
			WorkflowTables.Properties._Datalink__taskDatalinkStateMessage,
			WorkflowTables.Properties._Datalink__taskDatalinkTargetFile,
			WorkflowTables.Properties._Taskdependency__taskDependencyIsloose
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Decision = {
			WorkflowTables.Properties._Decision__workflowDecisionExpression,
			WorkflowTables.Properties._Decision__workflowDecisionInput,
			WorkflowTables.Properties._Decision__workflowDecisionResult,
			WorkflowTables.Properties._Task__workflowTaskState,
			WorkflowTables.Properties._Task__workflowTaskStateMessage
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Duration = {};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Executionlink = {};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _For = {
			WorkflowTables.Properties._For__loopIterationCounterIsIncrement
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Foreach = {
			WorkflowTables.Properties._Foreach__loopItemDelimiter,
			WorkflowTables.Properties._Foreach__loopItemName
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Integer = {};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Localcanal = {};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Loop = {
			WorkflowTables.Properties._Loop__loopIterationCount,
			WorkflowTables.Properties._Decision__workflowDecisionExpression,
			WorkflowTables.Properties._Decision__workflowDecisionInput,
			WorkflowTables.Properties._Decision__workflowDecisionResult,
			WorkflowTables.Properties._Task__workflowTaskState,
			WorkflowTables.Properties._Task__workflowTaskStateMessage
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Loopiteration = {
			WorkflowTables.Properties._Loopiteration__loopIterationVarName,
			WorkflowTables.Properties._Loopiteration__loopIterationVarValue
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Nesteddependency = {};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Networkcanal = {
			WorkflowTables.Properties._Networkcanal__taskDatalinkNetwork
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Parallelloop = {
			WorkflowTables.Properties._Parallelloop__parallelReplicateNumber
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Path = {};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Platformdependency = {};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Profile = {
			WorkflowTables.Properties._Profile__occiProfileDeploymenttime,
			WorkflowTables.Properties._Profile__occiProfileRuntime
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Remotedatacanal = {
			WorkflowTables.Properties._Remotedatacanal__taskDatalinkSourceResource,
			WorkflowTables.Properties._Remotedatacanal__taskDatalinkTargetResource
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Replica = {
			WorkflowTables.Properties._Replica__replicaGroup,
			WorkflowTables.Properties._Replica__replicaSourceId
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Shared = {};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Status = {};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Storagecanal = {
			WorkflowTables.Properties._Storagecanal__taskDatalinkStorage
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _String = {};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Task = {
			WorkflowTables.Properties._Task__workflowTaskState,
			WorkflowTables.Properties._Task__workflowTaskStateMessage
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Taskdependency = {
			WorkflowTables.Properties._Taskdependency__taskDependencyIsloose
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Taskobservation = {
			WorkflowTables.Properties._Taskobservation__taskObservation,
			WorkflowTables.Properties._Taskobservation__taskObservationFile
		};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _Timestamp = {};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _URI = {};

		private static final /*@NonNull*/ ExecutorProperty /*@NonNull*/ [] _While = {};

		/**
		 *	Install the property descriptors in the fragment descriptors.
		 */
		static {
			Fragments._Boolean__Boolean.initProperties(_Boolean);
			Fragments._Controlflowguard__Controlflowguard.initProperties(_Controlflowguard);
			Fragments._Controlflowlink__Controlflowlink.initProperties(_Controlflowlink);
			Fragments._Datalink__Datalink.initProperties(_Datalink);
			Fragments._Decision__Decision.initProperties(_Decision);
			Fragments._Duration__Duration.initProperties(_Duration);
			Fragments._Executionlink__Executionlink.initProperties(_Executionlink);
			Fragments._For__For.initProperties(_For);
			Fragments._Foreach__Foreach.initProperties(_Foreach);
			Fragments._Integer__Integer.initProperties(_Integer);
			Fragments._Localcanal__Localcanal.initProperties(_Localcanal);
			Fragments._Loop__Loop.initProperties(_Loop);
			Fragments._Loopiteration__Loopiteration.initProperties(_Loopiteration);
			Fragments._Nesteddependency__Nesteddependency.initProperties(_Nesteddependency);
			Fragments._Networkcanal__Networkcanal.initProperties(_Networkcanal);
			Fragments._Parallelloop__Parallelloop.initProperties(_Parallelloop);
			Fragments._Path__Path.initProperties(_Path);
			Fragments._Platformdependency__Platformdependency.initProperties(_Platformdependency);
			Fragments._Profile__Profile.initProperties(_Profile);
			Fragments._Remotedatacanal__Remotedatacanal.initProperties(_Remotedatacanal);
			Fragments._Replica__Replica.initProperties(_Replica);
			Fragments._Shared__Shared.initProperties(_Shared);
			Fragments._Status__Status.initProperties(_Status);
			Fragments._Storagecanal__Storagecanal.initProperties(_Storagecanal);
			Fragments._String__String.initProperties(_String);
			Fragments._Task__Task.initProperties(_Task);
			Fragments._Taskdependency__Taskdependency.initProperties(_Taskdependency);
			Fragments._Taskobservation__Taskobservation.initProperties(_Taskobservation);
			Fragments._Timestamp__Timestamp.initProperties(_Timestamp);
			Fragments._URI__URI.initProperties(_URI);
			Fragments._While__While.initProperties(_While);

			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of WorkflowTables::FragmentProperties and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The lists of enumeration literals for each enumeration.
	 */
	public static class EnumerationLiterals {
		static {
			Init.initStart();
			FragmentProperties.init();
		}

		public static final /*@NonNull*/ EcoreExecutorEnumerationLiteral _Status__scheduled = new EcoreExecutorEnumerationLiteral(WorkflowPackage.Literals.STATUS.getEEnumLiteral("scheduled"), Types._Status, 0);
		public static final /*@NonNull*/ EcoreExecutorEnumerationLiteral _Status__active = new EcoreExecutorEnumerationLiteral(WorkflowPackage.Literals.STATUS.getEEnumLiteral("active"), Types._Status, 1);
		public static final /*@NonNull*/ EcoreExecutorEnumerationLiteral _Status__inactive = new EcoreExecutorEnumerationLiteral(WorkflowPackage.Literals.STATUS.getEEnumLiteral("inactive"), Types._Status, 2);
		public static final /*@NonNull*/ EcoreExecutorEnumerationLiteral _Status__error = new EcoreExecutorEnumerationLiteral(WorkflowPackage.Literals.STATUS.getEEnumLiteral("error"), Types._Status, 3);
		public static final /*@NonNull*/ EcoreExecutorEnumerationLiteral _Status__finished = new EcoreExecutorEnumerationLiteral(WorkflowPackage.Literals.STATUS.getEEnumLiteral("finished"), Types._Status, 4);
		public static final /*@NonNull*/ EcoreExecutorEnumerationLiteral _Status__skipped = new EcoreExecutorEnumerationLiteral(WorkflowPackage.Literals.STATUS.getEEnumLiteral("skipped"), Types._Status, 5);
		private static final /*@NonNull*/ EcoreExecutorEnumerationLiteral /*@NonNull*/ [] _Status = {
			_Status__scheduled,
			_Status__active,
			_Status__inactive,
			_Status__error,
			_Status__finished,
			_Status__skipped
		};

		/**
		 *	Install the enumeration literals in the enumerations.
		 */
		static {
			Types._Status.initLiterals(_Status);

			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of WorkflowTables::EnumerationLiterals and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 * The multiple packages above avoid problems with the Java 65536 byte limit but introduce a difficulty in ensuring that
	 * static construction occurs in the disciplined order of the packages when construction may start in any of the packages.
	 * The problem is resolved by ensuring that the static construction of each package first initializes its immediate predecessor.
	 * On completion of predecessor initialization, the residual packages are initialized by starting an initialization in the last package.
	 * This class maintains a count so that the various predecessors can distinguish whether they are the starting point and so
	 * ensure that residual construction occurs just once after all predecessors.
	 */
	private static class Init {
		/**
		 * Counter of nested static constructions. On return to zero residual construction starts. -ve once residual construction started.
		 */
		private static int initCount = 0;

		/**
		 * Invoked at the start of a static construction to defer residual cobstruction until primary constructions complete.
		 */
		private static void initStart() {
			if (initCount >= 0) {
				initCount++;
			}
		}

		/**
		 * Invoked at the end of a static construction to activate residual cobstruction once primary constructions complete.
		 */
		private static void initEnd() {
			if (initCount > 0) {
				if (--initCount == 0) {
					initCount = -1;
					EnumerationLiterals.init();
				}
			}
		}
	}

	static {
		Init.initEnd();
	}

	/*
	 * Force initialization of outer fields. Inner fields are lazily initialized.
	 */
	public static void init() {}
}
