/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package workflow.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import workflow.Decision;
import workflow.WorkflowPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Decision</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link workflow.impl.DecisionImpl#getWorkflowDecisionInput <em>Workflow Decision Input</em>}</li>
 *   <li>{@link workflow.impl.DecisionImpl#getWorkflowDecisionExpression <em>Workflow Decision Expression</em>}</li>
 *   <li>{@link workflow.impl.DecisionImpl#getWorkflowDecisionResult <em>Workflow Decision Result</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DecisionImpl extends TaskImpl implements Decision {
	/**
	 * The default value of the '{@link #getWorkflowDecisionInput() <em>Workflow Decision Input</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorkflowDecisionInput()
	 * @generated
	 * @ordered
	 */
	protected static final String WORKFLOW_DECISION_INPUT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getWorkflowDecisionInput() <em>Workflow Decision Input</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorkflowDecisionInput()
	 * @generated
	 * @ordered
	 */
	protected String workflowDecisionInput = WORKFLOW_DECISION_INPUT_EDEFAULT;

	/**
	 * The default value of the '{@link #getWorkflowDecisionExpression() <em>Workflow Decision Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorkflowDecisionExpression()
	 * @generated
	 * @ordered
	 */
	protected static final String WORKFLOW_DECISION_EXPRESSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getWorkflowDecisionExpression() <em>Workflow Decision Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorkflowDecisionExpression()
	 * @generated
	 * @ordered
	 */
	protected String workflowDecisionExpression = WORKFLOW_DECISION_EXPRESSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getWorkflowDecisionResult() <em>Workflow Decision Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorkflowDecisionResult()
	 * @generated
	 * @ordered
	 */
	protected static final String WORKFLOW_DECISION_RESULT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getWorkflowDecisionResult() <em>Workflow Decision Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorkflowDecisionResult()
	 * @generated
	 * @ordered
	 */
	protected String workflowDecisionResult = WORKFLOW_DECISION_RESULT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DecisionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowPackage.Literals.DECISION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getWorkflowDecisionInput() {
		return workflowDecisionInput;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setWorkflowDecisionInput(String newWorkflowDecisionInput) {
		String oldWorkflowDecisionInput = workflowDecisionInput;
		workflowDecisionInput = newWorkflowDecisionInput;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowPackage.DECISION__WORKFLOW_DECISION_INPUT, oldWorkflowDecisionInput, workflowDecisionInput));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getWorkflowDecisionExpression() {
		return workflowDecisionExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setWorkflowDecisionExpression(String newWorkflowDecisionExpression) {
		String oldWorkflowDecisionExpression = workflowDecisionExpression;
		workflowDecisionExpression = newWorkflowDecisionExpression;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowPackage.DECISION__WORKFLOW_DECISION_EXPRESSION, oldWorkflowDecisionExpression, workflowDecisionExpression));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getWorkflowDecisionResult() {
		return workflowDecisionResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setWorkflowDecisionResult(String newWorkflowDecisionResult) {
		String oldWorkflowDecisionResult = workflowDecisionResult;
		workflowDecisionResult = newWorkflowDecisionResult;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowPackage.DECISION__WORKFLOW_DECISION_RESULT, oldWorkflowDecisionResult, workflowDecisionResult));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkflowPackage.DECISION__WORKFLOW_DECISION_INPUT:
				return getWorkflowDecisionInput();
			case WorkflowPackage.DECISION__WORKFLOW_DECISION_EXPRESSION:
				return getWorkflowDecisionExpression();
			case WorkflowPackage.DECISION__WORKFLOW_DECISION_RESULT:
				return getWorkflowDecisionResult();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkflowPackage.DECISION__WORKFLOW_DECISION_INPUT:
				setWorkflowDecisionInput((String)newValue);
				return;
			case WorkflowPackage.DECISION__WORKFLOW_DECISION_EXPRESSION:
				setWorkflowDecisionExpression((String)newValue);
				return;
			case WorkflowPackage.DECISION__WORKFLOW_DECISION_RESULT:
				setWorkflowDecisionResult((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkflowPackage.DECISION__WORKFLOW_DECISION_INPUT:
				setWorkflowDecisionInput(WORKFLOW_DECISION_INPUT_EDEFAULT);
				return;
			case WorkflowPackage.DECISION__WORKFLOW_DECISION_EXPRESSION:
				setWorkflowDecisionExpression(WORKFLOW_DECISION_EXPRESSION_EDEFAULT);
				return;
			case WorkflowPackage.DECISION__WORKFLOW_DECISION_RESULT:
				setWorkflowDecisionResult(WORKFLOW_DECISION_RESULT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkflowPackage.DECISION__WORKFLOW_DECISION_INPUT:
				return WORKFLOW_DECISION_INPUT_EDEFAULT == null ? workflowDecisionInput != null : !WORKFLOW_DECISION_INPUT_EDEFAULT.equals(workflowDecisionInput);
			case WorkflowPackage.DECISION__WORKFLOW_DECISION_EXPRESSION:
				return WORKFLOW_DECISION_EXPRESSION_EDEFAULT == null ? workflowDecisionExpression != null : !WORKFLOW_DECISION_EXPRESSION_EDEFAULT.equals(workflowDecisionExpression);
			case WorkflowPackage.DECISION__WORKFLOW_DECISION_RESULT:
				return WORKFLOW_DECISION_RESULT_EDEFAULT == null ? workflowDecisionResult != null : !WORKFLOW_DECISION_RESULT_EDEFAULT.equals(workflowDecisionResult);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (workflowDecisionInput: ");
		result.append(workflowDecisionInput);
		result.append(", workflowDecisionExpression: ");
		result.append(workflowDecisionExpression);
		result.append(", workflowDecisionResult: ");
		result.append(workflowDecisionResult);
		result.append(')');
		return result.toString();
	}

} //DecisionImpl
