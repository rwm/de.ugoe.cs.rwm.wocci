/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package workflow.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.cmf.occi.core.impl.ResourceImpl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import workflow.Status;
import workflow.Task;
import workflow.WorkflowPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link workflow.impl.TaskImpl#getWorkflowTaskState <em>Workflow Task State</em>}</li>
 *   <li>{@link workflow.impl.TaskImpl#getWorkflowTaskStateMessage <em>Workflow Task State Message</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TaskImpl extends ResourceImpl implements Task {
	/**
	 * The default value of the '{@link #getWorkflowTaskState() <em>Workflow Task State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorkflowTaskState()
	 * @generated
	 * @ordered
	 */
	protected static final Status WORKFLOW_TASK_STATE_EDEFAULT = Status.SCHEDULED;

	/**
	 * The cached value of the '{@link #getWorkflowTaskState() <em>Workflow Task State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorkflowTaskState()
	 * @generated
	 * @ordered
	 */
	protected Status workflowTaskState = WORKFLOW_TASK_STATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getWorkflowTaskStateMessage() <em>Workflow Task State Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorkflowTaskStateMessage()
	 * @generated
	 * @ordered
	 */
	protected static final String WORKFLOW_TASK_STATE_MESSAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getWorkflowTaskStateMessage() <em>Workflow Task State Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorkflowTaskStateMessage()
	 * @generated
	 * @ordered
	 */
	protected String workflowTaskStateMessage = WORKFLOW_TASK_STATE_MESSAGE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowPackage.Literals.TASK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Status getWorkflowTaskState() {
		return workflowTaskState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setWorkflowTaskState(Status newWorkflowTaskState) {
		Status oldWorkflowTaskState = workflowTaskState;
		workflowTaskState = newWorkflowTaskState == null ? WORKFLOW_TASK_STATE_EDEFAULT : newWorkflowTaskState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowPackage.TASK__WORKFLOW_TASK_STATE, oldWorkflowTaskState, workflowTaskState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getWorkflowTaskStateMessage() {
		return workflowTaskStateMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setWorkflowTaskStateMessage(String newWorkflowTaskStateMessage) {
		String oldWorkflowTaskStateMessage = workflowTaskStateMessage;
		workflowTaskStateMessage = newWorkflowTaskStateMessage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowPackage.TASK__WORKFLOW_TASK_STATE_MESSAGE, oldWorkflowTaskStateMessage, workflowTaskStateMessage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void start() {
		throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Task!start()
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void schedule() {
		throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Task!schedule()
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void stop() {
		throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Task!stop()
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void skip() {
		throw new UnsupportedOperationException();  // FIXME Unimplemented http://schemas.ugoe.cs.rwm/workflow/ecore!Task!skip()
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkflowPackage.TASK__WORKFLOW_TASK_STATE:
				return getWorkflowTaskState();
			case WorkflowPackage.TASK__WORKFLOW_TASK_STATE_MESSAGE:
				return getWorkflowTaskStateMessage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkflowPackage.TASK__WORKFLOW_TASK_STATE:
				setWorkflowTaskState((Status)newValue);
				return;
			case WorkflowPackage.TASK__WORKFLOW_TASK_STATE_MESSAGE:
				setWorkflowTaskStateMessage((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkflowPackage.TASK__WORKFLOW_TASK_STATE:
				setWorkflowTaskState(WORKFLOW_TASK_STATE_EDEFAULT);
				return;
			case WorkflowPackage.TASK__WORKFLOW_TASK_STATE_MESSAGE:
				setWorkflowTaskStateMessage(WORKFLOW_TASK_STATE_MESSAGE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkflowPackage.TASK__WORKFLOW_TASK_STATE:
				return workflowTaskState != WORKFLOW_TASK_STATE_EDEFAULT;
			case WorkflowPackage.TASK__WORKFLOW_TASK_STATE_MESSAGE:
				return WORKFLOW_TASK_STATE_MESSAGE_EDEFAULT == null ? workflowTaskStateMessage != null : !WORKFLOW_TASK_STATE_MESSAGE_EDEFAULT.equals(workflowTaskStateMessage);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case WorkflowPackage.TASK___START:
				start();
				return null;
			case WorkflowPackage.TASK___SCHEDULE:
				schedule();
				return null;
			case WorkflowPackage.TASK___STOP:
				stop();
				return null;
			case WorkflowPackage.TASK___SKIP:
				skip();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (workflowTaskState: ");
		result.append(workflowTaskState);
		result.append(", workflowTaskStateMessage: ");
		result.append(workflowTaskStateMessage);
		result.append(')');
		return result.toString();
	}

} //TaskImpl
