/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.utility;

import java.util.NoSuchElementException;

import org.eclipse.cmf.occi.core.Action;
import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.cmf.occi.core.Entity;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.Resource;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.modmacao.occi.platform.Application;
import org.modmacao.occi.platform.Component;
import org.modmacao.occi.platform.Componentlink;

import workflow.Datalink;
import workflow.Executionlink;
import workflow.Platformdependency;
import workflow.Task;
import workflow.Taskdependency;

/**
 * Responsible for EMF Model Utility Operations.
 *
 * @author erbel
 *
 */
public class ModelUtility {
	public static EList<Resource> getTasksAsResource(org.eclipse.emf.ecore.resource.Resource workflowModel) {
		EList<Resource> tasks = new BasicEList<Resource>();
		Configuration config = de.ugoe.cs.rwm.docci.ModelUtility.getOCCIConfiguration(workflowModel);
		for (Entity res : config.getResources()) {
			if (res instanceof Task || (res.getKind().getScheme().equals("http://schemas.ugoe.cs.rwm/workflow#")
					&& res.getKind().getTerm().equals("task"))) {
				tasks.add((Resource) res);
			}
		}
		if (tasks.isEmpty() == false) {
			return tasks;
		} else {
			throw new NoSuchElementException("OCCI Configuration does not contain any task.");
		}
	}

	public static EList<Task> getTasks(org.eclipse.emf.ecore.resource.Resource workflowModel) {
		EList<Task> tasks = new BasicEList<Task>();
		Configuration config = de.ugoe.cs.rwm.docci.ModelUtility.getOCCIConfiguration(workflowModel);
		for (Entity res : config.getResources()) {
			if (res instanceof Task || (res.getKind().getScheme().equals("http://schemas.ugoe.cs.rwm/workflow#")
					&& res.getKind().getTerm().equals("task"))) {
				tasks.add((Task) res);
			}
		}
		if (tasks.isEmpty() == false) {
			return tasks;
		} else {
			throw new NoSuchElementException("OCCI Configuration does not contain any task.");
		}

	}

	public static Action getStartAction(Resource task) {
		for (Action act : task.getKind().getActions()) {
			if (act.getTerm().equals("start")) {
				return act;
			}
		}
		throw new NoSuchElementException("Start action not found");
	}

	public static EList<Link> getExecutionLinks(EList<Link> links) {
		EList<Link> executionLinks = new BasicEList<Link>();
		for (Link link : links) {
			if (link instanceof Executionlink
					|| (link.getKind().getScheme().equals("http://schemas.ugoe.cs.rwm/workflow#")
							&& link.getKind().getTerm().equals("executionlink"))) {
				executionLinks.add(link);
			}
		}
		return executionLinks;
	}

	public static EList<Taskdependency> getTaskDependencyLinks(EList<Link> links) {
		EList<Taskdependency> dependencyLinks = new BasicEList<Taskdependency>();
		for (Link link : links) {
			if (link instanceof Taskdependency
					|| (link.getKind().getScheme().equals("http://schemas.ugoe.cs.rwm/workflow#")
							&& (link.getKind().getTerm().equals("controlflowlink")
									|| link.getKind().getTerm().equals("datalink")))) {
				dependencyLinks.add((Taskdependency) link);
			}
		}
		return dependencyLinks;
	}

	public static EList<Link> getPlatformDependencyLinks(EList<Link> links) {
		EList<Link> dependencyLinks = new BasicEList<Link>();
		for (Link link : links) {
			if (link instanceof Platformdependency
					|| (link.getKind().getScheme().equals("http://schemas.ugoe.cs.rwm/workflow#")
							&& (link.getKind().getTerm().equals("platformdependency")))) {
				dependencyLinks.add(link);
			}
		}
		return dependencyLinks;
	}

	public static Application getComponentApplication(Component comp) {
		EList<Application> applications = new BasicEList<Application>();
		for (Link link : comp.getRlinks()) {
			if (link instanceof Componentlink
					|| (link.getKind().getScheme().equals("http://schemas.modmacao.org/occi/platform#")
							&& link.getKind().getTerm().equals("componentlink"))) {
				if (link.getSource() instanceof Application
						|| (link.getSource().getKind().getScheme().equals("http://schemas.modmacao.org/occi/platform#")
								&& link.getSource().getKind().getTerm().equals("application"))) {
					applications.add((Application) link.getSource());
				}
			}
		}
		if (applications.size() > 1) {
			throw new IllegalArgumentException(
					"Currently: Executable Component should be part of exactly one Application");
		}
		if (applications.size() == 1) {
			return applications.get(0);
		}
		return null;
	}

	public static EList<Link> getDatalinks(org.eclipse.emf.ecore.resource.Resource runtimeModel) {
		EList<Link> dLinks = new BasicEList<Link>();

		for (Task task : getTasks(runtimeModel)) {
			for (Link link : task.getLinks()) {
				if (link instanceof Datalink
						|| (link.getKind().getScheme().equals("http://schemas.ugoe.cs.rwm/workflow#")
								&& link.getKind().getTerm().equals("datalink"))) {
					dLinks.add(link);
				}
			}
		}
		return dLinks;
	}
}
