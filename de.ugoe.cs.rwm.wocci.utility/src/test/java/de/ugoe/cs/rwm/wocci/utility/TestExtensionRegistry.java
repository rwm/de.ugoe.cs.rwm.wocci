package de.ugoe.cs.rwm.wocci.utility;

import org.eclipse.cmf.occi.core.OCCIPackage;
import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.cmf.occi.crtp.CrtpPackage;
import org.eclipse.cmf.occi.docker.DockerPackage;
import org.eclipse.cmf.occi.infrastructure.InfrastructurePackage;
import org.eclipse.cmf.occi.sla.SlaPackage;
import org.eclipse.cmf.occi.tosca.ToscaPackage;
import org.junit.Test;
import org.modmacao.ansibleconfiguration.AnsibleconfigurationPackage;
import org.modmacao.occi.platform.PlatformPackage;
import org.modmacao.placement.PlacementPackage;

import de.ugoe.cs.rwm.domain.shark.SharkPackage;
import extendedtosca.ExtendedtoscaPackage;
import modmacao.ModmacaoPackage;
import monitoring.MonitoringPackage;
import openstackruntime.OpenstackruntimePackage;
import ossweruntime.OssweruntimePackage;
import workflow.WorkflowPackage;

public class TestExtensionRegistry {

	public static void extensionRegistrySetup() {
		InfrastructurePackage.eINSTANCE.eClass();
		OCCIPackage.eINSTANCE.eClass();
		ModmacaoPackage.eINSTANCE.eClass();
		OpenstackruntimePackage.eINSTANCE.eClass();
		PlacementPackage.eINSTANCE.eClass();
		WorkflowPackage.eINSTANCE.eClass();
		AnsibleconfigurationPackage.eINSTANCE.eClass();
		MonitoringPackage.eINSTANCE.eClass();
		PlatformPackage.eINSTANCE.eClass();
		SharkPackage.eINSTANCE.eClass();
		CrtpPackage.eINSTANCE.eClass();
		OssweruntimePackage.eINSTANCE.eClass();
		DockerPackage.eINSTANCE.eClass();
		ToscaPackage.eINSTANCE.eClass();
		SlaPackage.eINSTANCE.eClass();
		ExtendedtoscaPackage.eINSTANCE.eClass();

		OcciRegistry.getInstance().registerExtension("http://schemas.ogf.org/occi/sla#",
				OCCIPackage.class.getClassLoader().getResource("model/sla.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.ogf.org/tosca/extended#",
				OCCIPackage.class.getClassLoader().getResource("model/extendedTosca.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.ogf.org/tosca/core#",
				OCCIPackage.class.getClassLoader().getResource("model/tosca.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://occiware.org/occi/docker#",
				OCCIPackage.class.getClassLoader().getResource("model/docker.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.ugoe.cs.rwm/monitoring#",
				OCCIPackage.class.getClassLoader().getResource("model/monitoring.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/modmacao#",
				ModmacaoPackage.class.getClassLoader().getResource("model/modmacao.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/openstack/runtime#",
				OpenstackruntimePackage.class.getClassLoader().getResource("model/openstackruntime.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/openstack/swe#",
				OssweruntimePackage.class.getClassLoader().getResource("model/ossweruntime.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.occiware.org/placement#",
				PlacementPackage.class.getClassLoader().getResource("model/placement.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.ogf.org/occi/infrastructure#",
				InfrastructurePackage.class.getClassLoader().getResource("model/Infrastructure.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.ogf.org/occi/core#",
				OCCIPackage.class.getClassLoader().getResource("model/Core.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.ugoe.cs.rwm/workflow#",
				OCCIPackage.class.getClassLoader().getResource("model/workflow.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/occi/ansible#",
				OCCIPackage.class.getClassLoader().getResource("model/ansibleconfiguration.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/occi/platform#",
				OCCIPackage.class.getClassLoader().getResource("model/platform.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.ugoe.cs.rwm/domain/shark#",
				OCCIPackage.class.getClassLoader().getResource("model/shark.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.ogf.org/occi/infrastructure/compute/template/1.1#",
				OCCIPackage.class.getClassLoader().getResource("model/crtp.occie").toString());
	}

	@Test
	public void testRegistration() {
		extensionRegistrySetup();
	}
}
