# WOCCI - Workflows with OCCI

WOCCI is a multi-build project containing several components to integrate the capabilities of modelling scientific workflows using the OCCI cloud standard. While the concept aligns to the specification of OCCI, the implementation is coupled to the [OCCIWare](http://occiware.github.io/) environment. The project consists of the following:

- [OCCI Workflow Extension](./de.ugoe.cs.rwm.wocci.model)
- [OCCIware Workflow Connector](./de.ugoe.cs.rwm.wocci.connector)
- [Workflow Execution Engine](./de.ugoe.cs.rwm.wocci.performer/)

## Getting Started 
To utilize WOCCI, we suggest following the instructions provided in the [SmartWYRM project](https://gitlab.gwdg.de/rwm/smartwyrm). SmartWYRM is the webapplication build on top of WOCCI including a graphical representation of executed workflows. Additionally, everyting you need to setup the infrastructural requirements is described over there including a containerized version easy to setup. Alternitavely, you can have a look at [prerecorded examples](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples/-/tree/master/wocci?ref_type=heads).

- [SmartWYRM WebApp](https://gitlab.gwdg.de/rwm/smartwyrm)
- [Prerecorded Repository Mining Example](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples/-/tree/master/wocci/smartshark?ref_type=heads)
