package de.ugoe.cs.rwm.wocci.connector.decision.gatherer;

import de.ugoe.cs.rwm.wocci.connector.DecisionConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import workflow.Decision;

public abstract class GathererFactory {
	private final static Logger LOGGER = LoggerFactory.getLogger(DecisionConnector.class);

	public static Gatherer getGatherer(Decision decision) {
		return new DecisionGatherer(decision);
	}

}
