/**
 * Copyright (c) 2016-2017 Inria
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 *
 * Generated at Tue Aug 28 15:09:33 CEST 2018 from platform:/resource/workflow/model/workflow.occie by org.eclipse.cmf.occi.core.gen.connector
 */

package de.ugoe.cs.rwm.wocci.connector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Connector implementation for the OCCI kind: - scheme:
 * http://schemas.ugoe.cs.rwm/workflow# - term: taskdependency - title:
 * TaskDependency
 */
public class TaskdependencyConnector extends workflow.impl.TaskdependencyImpl {
	/**
	 * Initialize the logger.
	 */
	private static Logger LOGGER = LoggerFactory.getLogger(TaskdependencyConnector.class);

	// Start of user code Taskdependencyconnector_constructor
	/**
	 * Constructs a taskdependency connector.
	 */
	TaskdependencyConnector() {
		LOGGER.debug("Constructor called on " + this);
		// TODO: Implement this constructor.
	}
	// End of user code
	//
	// OCCI CRUD callback operations.
	//

	// Start of user code TaskdependencyocciCreate
	/**
	 * Called when this Taskdependency instance is completely created.
	 */
	@Override
	public void occiCreate() {
		LOGGER.debug("occiCreate() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code Taskdependency_occiRetrieve_method
	/**
	 * Called when this Taskdependency instance must be retrieved.
	 */
	@Override
	public void occiRetrieve() {
		LOGGER.debug("occiRetrieve() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code Taskdependency_occiUpdate_method
	/**
	 * Called when this Taskdependency instance is completely updated.
	 */
	@Override
	public void occiUpdate() {
		LOGGER.debug("occiUpdate() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code TaskdependencyocciDelete_method
	/**
	 * Called when this Taskdependency instance will be deleted.
	 */
	@Override
	public void occiDelete() {
		LOGGER.debug("occiDelete() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	//
	// Taskdependency actions.
	//

}
