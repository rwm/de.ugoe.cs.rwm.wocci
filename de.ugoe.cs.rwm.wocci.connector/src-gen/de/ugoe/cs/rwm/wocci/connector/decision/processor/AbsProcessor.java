package de.ugoe.cs.rwm.wocci.connector.decision.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.eclipse.cmf.occi.core.AttributeState;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.MixinBase;
import org.modmacao.occi.platform.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.ugoe.cs.rwm.wocci.connector.DecisionConnector;
import de.ugoe.cs.rwm.wocci.connector.util.ModelUtility;
import workflow.*;

public abstract class AbsProcessor implements Processor {
	protected final static Logger LOGGER = LoggerFactory.getLogger(DecisionConnector.class);
	protected Decision decision;

	protected abstract void executeSchedulingSpecifics();

	protected abstract void performMixinSpecificTask(List<Controlflowlink> trueCLinks);

	public AbsProcessor(Decision decision2) {
		this.decision = decision2;
	}

	@Override
	public void scheduleDecision() {
		decision.setWorkflowDecisionResult("");
		executeSchedulingSpecifics();
	}

	@Override
	public void processGatheredInformation() {
		if (informationIsGathered(decision)) {
			List<Controlflowlink> trueCLinks = evaluateControlFlowGuards();
			performMixinSpecificTask(trueCLinks);
		}
	}

	public List<Controlflowlink> evaluateControlFlowGuards() {
		List<Controlflowlink> trueCLinks = new ArrayList<Controlflowlink>();
		String expressionResult = "";
		if (decision.getWorkflowDecisionExpression() != null
				&& decision.getWorkflowDecisionExpression().equals("") == false) {
			expressionResult = evaluateDecisionExpression();
		} else {
			expressionResult = decision.getWorkflowDecisionInput();
		}

		for (Controlflowlink cLink : ModelUtility.getSpecificLinks(decision, Controlflowlink.class)) {
			String cLinkGuard = getGuardValue(cLink);
			if (cLinkGuard != null && expressionResult != null && expressionResult.equals("") == false) {
				LOGGER.info("GUARD: " + cLinkGuard + " DECISIONINPUT: " + decision.getWorkflowDecisionInput()
						+ " DECISIONEXPRESSION: " + decision.getWorkflowDecisionExpression() + " RESULT: "
						+ expressionResult);
				if (cLinkGuard.equals(expressionResult)) {
					trueCLinks.add(cLink);
				} else {
					skipTasks(cLink);
				}
			}
		}
		return trueCLinks;
	}

	private String getGuardValue(Controlflowlink cLink) {
		LOGGER.info("Searching for a guard in: " + cLink);
		LOGGER.info("CLink mixinbases: " + cLink.getParts());
		LOGGER.info("CLINK MXINS: " + cLink.getMixins());
		for (MixinBase mBase : cLink.getParts()) {
			LOGGER.info("MBASE MIXIN: " + mBase.getMixin());
			if (mBase instanceof Controlflowguard) {
				Controlflowguard cGuard = (Controlflowguard) mBase;
				LOGGER.info("Controlflowguard found: " + cLink);
				return cGuard.getControlflowGuard();
			}
		}
		return null;
	}

	private String evaluateDecisionExpression() {
		String resultAsString = "";
		String decisionInput = decision.getWorkflowDecisionInput();
		String decisionExpression = unescapeXML(decision.getWorkflowDecisionExpression());
		LOGGER.info(decisionExpression);
		String runtimeExpression = createRuntimeExpression(decisionExpression, decisionInput);
		LOGGER.info(runtimeExpression);
		ScriptEngine engine = new ScriptEngineManager().getEngineByExtension("js");
		try {
			Object result = engine.eval(runtimeExpression);
			LOGGER.info(runtimeExpression + " = " + result);
			resultAsString = result.toString();
			LOGGER.info("Result as String" + resultAsString);
		} catch (ScriptException | NullPointerException e) {
			LOGGER.error("Decision Expression could not be checked");
			decision.setWorkflowTaskState(Status.ERROR);
			decision.setWorkflowTaskStateMessage("Decision Expression could not be checked");
			e.printStackTrace();
		}
		decision.setWorkflowDecisionResult(resultAsString);
		return resultAsString;
	}

	// Source:
	// https://stackoverflow.com/questions/2833956/how-to-unescape-xml-in-java
	private String unescapeXML(final String xml) {
		Pattern xmlEntityRegex = Pattern.compile("&(#?)([^;]+);");
		// Unfortunately, Matcher requires a StringBuffer instead of a StringBuilder
		StringBuffer unescapedOutput = new StringBuffer(xml.length());

		Matcher m = xmlEntityRegex.matcher(xml);
		Map<String, String> builtinEntities = null;
		String entity;
		String hashmark;
		String ent;
		int code;
		while (m.find()) {
			ent = m.group(2);
			hashmark = m.group(1);
			if ((hashmark != null) && (hashmark.length() > 0)) {
				code = Integer.parseInt(ent);
				entity = Character.toString((char) code);
			} else {
				// must be a non-numerical entity
				if (builtinEntities == null) {
					builtinEntities = buildBuiltinXMLEntityMap();
				}
				entity = builtinEntities.get(ent);
				if (entity == null) {
					// not a known entity - ignore it
					entity = "&" + ent + ';';
				}
			}
			m.appendReplacement(unescapedOutput, entity);
		}
		m.appendTail(unescapedOutput);

		return unescapedOutput.toString();
	}

	private static Map<String, String> buildBuiltinXMLEntityMap() {
		Map<String, String> entities = new HashMap<String, String>(10);
		entities.put("lt", "<");
		entities.put("gt", ">");
		entities.put("amp", "&");
		entities.put("apos", "'");
		entities.put("quot", "\"");
		return entities;
	}

	private String createRuntimeExpression(String decisionExpression, String decisionInput) {
		String runtimeExpression;
		if (decisionInput.equals("false") == false && decisionInput.equals("true") == false) {
			runtimeExpression = decisionExpression.replace("workflow.decision.input", "'" + decisionInput + "'");
		} else {
			runtimeExpression = decisionExpression.replace("workflow.decision.input", decisionInput);
		}
		for (AttributeState as : decision.getAttributes()) {
			if (decisionExpression.contains(as.getName())) {
				runtimeExpression = runtimeExpression.replace(as.getName(), as.getValue());
			}
		}

		for (MixinBase mixB : decision.getParts()) {
			for (AttributeState as : mixB.getAttributes()) {
				if (decisionExpression.contains(as.getName())) {
					runtimeExpression = runtimeExpression.replace(as.getName(), as.getValue());
				}
			}
		}

		return runtimeExpression;
	}

	protected Boolean informationIsGathered(Decision decision2) {
		if (decision.getWorkflowDecisionInput() != null) {
			return true;
		} else {
			return false;
		}
	}

	private void skipTasks(Controlflowlink cLink) {
		if (cLink.getTarget() instanceof Task) {
			Task task = (Task) cLink.getTarget();
			LOGGER.info("Skipping Task: " + task);
			task.skip();
			for (Link taskLink : task.getLinks()) {
				if (taskLink instanceof Taskdependency) {
					checkReachability((Task) taskLink.getTarget());
				}
			}
		}
	}

	private void checkReachability(Task task) {
		boolean reachable = false;
		for (Link link : task.getRlinks()) {
			if (link instanceof Taskdependency) {
				Task preTask = (Task) link.getSource();
				if (!(preTask.getWorkflowTaskState().getValue() == Status.ERROR_VALUE
						|| preTask.getWorkflowTaskState().getValue() == Status.SKIPPED_VALUE)) {
					reachable = true;
				}
			}
		}
		if (reachable == false) {
			if (task.getWorkflowTaskState().getValue() == Status.SKIPPED_VALUE) {
				return;
			}
			task.skip();
			for (Link taskLink : task.getLinks()) {
				if (taskLink instanceof Taskdependency) {
					checkReachability((Task) taskLink.getTarget());
				}
			}
		}
	}

	protected void rescheduleTaskSequence(Task task) {
		LOGGER.info("Rescheduling Tasksequence!");
		if (task.getId().equals(decision.getId()) == false) {
			task.setWorkflowTaskState(Status.SCHEDULED);
			task.setWorkflowTaskStateMessage(
					"Rescheduled due to Loop iteration." + " Executable forced to inactive state."
							+ "No CM Actions are executed to save time for short iterating tasks!");
			for (Executionlink execL : ModelUtility.getSpecificLinks(task, Executionlink.class)) {
				if (execL.getTarget() instanceof Component) {
					Component comp = (Component) execL.getTarget();
					// If exeutable presists throughout a decision or loop process:
					// Softschedule to reduce amount of requests for short running iterations
					((Component) execL.getTarget()).setOcciComponentState(org.modmacao.occi.platform.Status.INACTIVE);
				}
			}
		}
		for (Taskdependency tDep : ModelUtility.getSpecificLinks(task, Taskdependency.class)) {
			System.out.println(tDep);
			System.out.println(tDep.getTarget());
			Task tarTask = (Task) tDep.getTarget();
			if (tarTask.getWorkflowTaskState().getValue() != Status.SCHEDULED.getValue()
					&& tarTask.getId().equals(decision.getId()) == false) {
				rescheduleTaskSequence(tarTask);
			}
		}
	}
}
