package de.ugoe.cs.rwm.wocci.connector.decision.gatherer;

public interface Gatherer {

	public void gatherRuntimeInformation();
}
