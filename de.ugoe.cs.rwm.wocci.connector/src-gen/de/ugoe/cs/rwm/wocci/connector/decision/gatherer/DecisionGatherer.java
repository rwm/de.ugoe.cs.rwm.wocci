package de.ugoe.cs.rwm.wocci.connector.decision.gatherer;

import org.modmacao.occi.platform.Application;
import org.modmacao.occi.platform.Component;

import de.ugoe.cs.rwm.wocci.connector.util.ModelUtility;
import monitoring.Sensor;
import workflow.Decision;

public class DecisionGatherer extends AbsGatherer {
	private static final int DECISIONINPUT_SLEEP = 2000;

	public DecisionGatherer(Decision decision) {
		super(decision);
	}

	@Override
	protected void gatherFromSensor(Sensor sens) {
		while (getMonitoringResult(sens).equals("")) {
			LOGGER.info("Waiting for Decision Input: " + decision + " | " + sens);
			try {
				Thread.sleep(DECISIONINPUT_SLEEP);
			} catch (InterruptedException e) { // TODO Auto-generated catch block
				LOGGER.error("Waiting for decision input interrupted! " + "(" + decision.getTitle() + ")");
				return;
			}
		}
		decision.setWorkflowDecisionInput(getMonitoringResult(sens));
		LOGGER.info("Decision Input found! Stopping Sensor");
		decision.setWorkflowTaskStateMessage("Decision input found!");
		ModelUtility.getTaskStateMessage(decision).setValue("Decision Input Found");
		sens.stop();

		/*
		 * if (getMonitoringResult(sens).equals("")) {
		 * LOGGER.info("No Monitoring results found");
		 * this.setWorkflowTaskStateMessage("No Monitoring results found!"); } else {
		 * this.setWorkflowDecisionInput(getMonitoringResult(sens));
		 * LOGGER.info("Decision Input found! Stopping Sensor");
		 * this.setWorkflowTaskStateMessage("Monitoring results found!"); sens.stop(); }
		 */
	}

	@Override
	protected void gatherFromComponent(Component comp) {
		decision.setWorkflowDecisionInput("ExampleDecisionInputFromComponent");
		comp.stop();
	}

	@Override
	protected void gatherFromApplication(Application app) {
		decision.setWorkflowDecisionInput("ExampleDecisionInputFromApplication");
		app.stop();
	}
}
