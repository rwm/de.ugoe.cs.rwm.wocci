package de.ugoe.cs.rwm.wocci.connector.decision.processor.loop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.cmf.occi.core.MixinBase;

import de.ugoe.cs.rwm.wocci.connector.util.ModelUtility;
import workflow.*;

public class ForEachLoopProcessor extends AbsLoopProcessor {

	public ForEachLoopProcessor(Decision decision) {
		super(decision);
	}

	@Override
	protected void executeSchedulingSpecifics() {
		decision.setWorkflowDecisionResult("");
		rescheduleTaskSequence(decision);
	}

	@Override
	protected Loopiteration createIterationVar() {
		return ModelUtility.createLoopiteration(getLoopItemName(), popFirstDecisionElement());
	}

	public void redistributeItems() {
		String delimiter = getDelimiter();
		Loop max = null;
		Loop min = null;
		for (Nesteddependency nest : ModelUtility.getSpecificLinks(decision, Nesteddependency.class)) {
			Loop nestedLoop = (Loop) nest.getTarget();
			if (max == null && min == null) {
				max = nestedLoop;
				min = nestedLoop;
			}
			System.out.println("Loop: " + nestedLoop.getTitle() + ": " + nestedLoop.getId());
			System.out.println("Decision Input: " + nestedLoop.getWorkflowDecisionInput());
			if (getInputSize(nestedLoop) > getInputSize(max)) {
				max = nestedLoop;
			}
			if (getInputSize(nestedLoop) < getInputSize(min)) {
				min = nestedLoop;
			}
		}
		if (max != null && min != null) {
			if (getInputSize(max) >= 2 && getInputSize(min) < getInputSize(max)) {
				System.out.println("Min Loop: " + min + " : Decision Input " + min.getWorkflowDecisionInput());
				System.out.println("Max Loop: " + max + " : Decision Input " + max.getWorkflowDecisionInput());
				String[] arr = max.getWorkflowDecisionInput().split(delimiter);
				int l = arr.length;
				String[] first = Arrays.copyOfRange(arr, 0, (l + 1) / 2);
				String[] second = Arrays.copyOfRange(arr, (l + 1) / 2, l);

				min.setWorkflowDecisionInput(String.join(delimiter, second));

				max.setWorkflowDecisionInput(String.join(delimiter, first));
				min.schedule();
				min.start();
			}
		}
	}

	private String getLoopItemName() {
		for (MixinBase mixB : decision.getParts()) {
			if (mixB instanceof Foreach) {
				return ((Foreach) mixB).getLoopItemName();
			}
		}
		return "default";
	}

	private String popFirstDecisionElement() {
		String decisionInput = decision.getWorkflowDecisionInput();
		List<String> decisionInputAsList = convertToList(decisionInput);
		if (decisionInputAsList.size() != 0) {
			String firstElement = decisionInputAsList.get(0);
			decisionInputAsList.remove(0);
			decision.setWorkflowDecisionInput(String.join(getDelimiter(), decisionInputAsList));
			return firstElement;
		} else {
			return "";
		}
	}

	private List<String> convertToList(String decisionInput) {
		String delimiter = getDelimiter();
		String[] strArr = decisionInput.split(delimiter);
		List<String> list = new ArrayList<String>(Arrays.asList(strArr));
		return list;
	}

	private String getDelimiter() {
		for (MixinBase base : decision.getParts()) {
			if (base instanceof Foreach) {
				Foreach foreach = (Foreach) base;
				return foreach.getLoopItemDelimiter();
			}
		}
		return ",";
	}

	private Foreach getFEMixin() {
		for (MixinBase base : decision.getParts()) {
			if (base instanceof Foreach) {
				return (Foreach) base;
			}
		}
		return null;
	}

	private int getInputSize(Loop nestedLoop) {
		if (nestedLoop == null) {
			return 0;
		}
		return nestedLoop.getWorkflowDecisionInput().split(",").length;
	}
}
