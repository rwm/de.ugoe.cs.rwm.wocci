/**
 * Copyright (c) 2016-2017 Inria
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 *
 * Generated at Tue Aug 28 15:09:33 CEST 2018 from platform:/resource/workflow/model/workflow.occie by org.eclipse.cmf.occi.core.gen.connector
 */

package de.ugoe.cs.rwm.wocci.connector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Connector implementation for the OCCI kind: - scheme:
 * http://schemas.ugoe.cs.rwm/workflow# - term: networkcanal - title:
 * NetworkCanal
 */
public class NetworkcanalConnector extends workflow.impl.NetworkcanalImpl {
	/**
	 * Initialize the logger.
	 */
	private static Logger LOGGER = LoggerFactory.getLogger(NetworkcanalConnector.class);

	// Start of user code Networkcanalconnector_constructor
	/**
	 * Constructs a networkcanal connector.
	 */
	NetworkcanalConnector() {
		LOGGER.debug("Constructor called on " + this);
		// TODO: Implement this constructor.
	}
	// End of user code

}
