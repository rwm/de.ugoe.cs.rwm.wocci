/**
 * Copyright (c) 2016-2017 Inria
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 *
 * Generated at Tue Dec 10 16:11:27 CET 2019 from
 * platform:/resource/de.ugoe.cs.rwm.wocci.model/model/workflow.occie by org.eclipse.cmf.occi.core.gen.connector
 */

package de.ugoe.cs.rwm.wocci.connector;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.cmf.occi.core.AttributeState;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.MixinBase;
import org.eclipse.cmf.occi.core.OCCIFactory;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.ugoe.cs.rwm.wocci.connector.decision.gatherer.Gatherer;
import de.ugoe.cs.rwm.wocci.connector.decision.gatherer.GathererFactory;
import de.ugoe.cs.rwm.wocci.connector.decision.processor.Processor;
import de.ugoe.cs.rwm.wocci.connector.decision.processor.ProcessorFactory;
import de.ugoe.cs.rwm.wocci.connector.decision.processor.loop.ForEachLoopProcessor;
import de.ugoe.cs.rwm.wocci.connector.decision.processor.parallel.ParallelProcessor;
import de.ugoe.cs.rwm.wocci.connector.util.ModelUtility;
import workflow.*;

/**
 * Connector implementation for the OCCI kind: - scheme:
 * http://schemas.ugoe.cs.rwm/workflow# - term: loop - title: Loop
 */
public class LoopConnector extends workflow.impl.LoopImpl {
	/**
	 * Initialize the logger.
	 */
	private static final int POSTCONSTRUCTORTIMEOUT = 8;
	private Set<LoopConnector> observers = new HashSet<LoopConnector>();
	private static Logger LOGGER = LoggerFactory.getLogger(LoopConnector.class);
	private Gatherer gatherer;
	private Processor processor;
	private boolean processing = false;

	// Start of user code Loopconnector_constructor
	/**
	 * Constructs a loop connector.
	 */
	LoopConnector() {
		LOGGER.debug("Constructor called on " + this);

		// Implemented due to suddenly disrupting the MartServer leaving the loop in an
		// inconsistent state:
		// Final task is finished, but update of loop is not fully executed.
		// This leaves the loop in state active, while the last task in state finished.
		PostConstructor pc = new PostConstructor(this, POSTCONSTRUCTORTIMEOUT);
		Thread thread = new Thread(pc);
		thread.start();
	}
	// End of user code
	//
	// OCCI CRUD callback operations.
	//

	// Start of user code LoopocciCreate
	/**
	 * Called when this Loop instance is completely created.
	 */
	@Override
	public void occiCreate() {
		LOGGER.info("occiCreate() called on " + this);
	}
	// End of user code

	// Start of user code Loop_occiRetrieve_method
	/**
	 * Called when this Loop instance must be retrieved.
	 */
	@Override
	public void occiRetrieve() {
		LOGGER.debug("occiRetrieve() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code Loop_occiUpdate_method
	/**
	 * Called when this Loop instance is completely updated.
	 */
	@Override
	public void occiUpdate() {
		LOGGER.info("occiUpdate() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code LoopocciDelete_method
	/**
	 * Called when this Loop instance will be deleted.
	 */
	@Override
	public void occiDelete() {
		LOGGER.debug("occiDelete() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	//
	// Loop actions.
	//

	// Start of user code Loop_Kind_schedule_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.ugoe.cs.rwm/workflow/task/action# - term: schedule - title:
	 * schedule
	 */
	@Override
	public void schedule() {
		LOGGER.debug("Action schedule() called on " + this);
		processor = ProcessorFactory.getProcessor(this);
		// Decision State Machine.
		switch (getWorkflowTaskState().getValue()) {
		case Status.ERROR_VALUE:
			LOGGER.debug("Fire transition(state=error, action=\"schedule\")...");
			processor.scheduleDecision();
			this.setWorkflowTaskState(Status.SCHEDULED);
			break;

		case Status.FINISHED_VALUE:
			LOGGER.debug("Fire transition(state=inactive, action=\"schedule\")...");
			processor.scheduleDecision();
			this.setWorkflowTaskState(Status.SCHEDULED);
			break;

		case Status.SKIPPED_VALUE:
			LOGGER.debug("Fire transition(state=inactive, action=\"schedule\")...");
			processor.scheduleDecision();
			this.setWorkflowTaskState(Status.SCHEDULED);
			break;

		default:
			break;
		}
	}

	// End of user code
	// Start of user code Loop_Kind_skip_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.ugoe.cs.rwm/workflow/task/action# - term: skip - title: Skip
	 */
	@Override
	public void skip() {
		LOGGER.debug("Action stop() called on " + this);

		// Task State Machine.
		switch (getWorkflowTaskState().getValue()) {

		case Status.SCHEDULED_VALUE:
			LOGGER.debug("Fire transition(state=scheduled, action=\"skip\")...");
			// TODO Implement transition(state=active, action="stop")
			setWorkflowTaskState(Status.SKIPPED);
			break;
		case Status.INACTIVE_VALUE:
			LOGGER.debug("Fire transition(state=scheduled, action=\"skip\")...");
			// TODO Implement transition(state=active, action="stop")
			setWorkflowTaskState(Status.SKIPPED);
			break;

		default:
			break;
		}
	}

	// End of user code
	// Start of user code Loop_Kind_start_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.ugoe.cs.rwm/workflow/task/action# - term: start - title: start
	 */
	@Override
	public synchronized void start() {
		observeLoopTasks();
		LOGGER.debug("Action start() called on " + this);
		gatherer = GathererFactory.getGatherer(this);
		processor = ProcessorFactory.getProcessor(this);
		LOGGER.info("Parts: " + this.getParts());
		LOGGER.info("Chosen gatherer: " + gatherer.getClass());
		LOGGER.info("Chosen processor: " + processor.getClass());

		// Decision State Machine.
		switch (getWorkflowTaskState().getValue()) {

		case Status.SCHEDULED_VALUE:
			if (processing == true) {
				return;
			}
			processing = true;
			LOGGER.debug("Fire transition(state=scheduled, action=\"start\")...");
			for (Link link : getRlinks()) {
				if (link instanceof Nesteddependency) {
					attachObserver((LoopConnector) link.getSource());
					LOGGER.info("ADD OBSERVER: " + this.observers);
				}
			}
			Parallelloop par = getParallelMixin();
			if (ModelUtility.getSpecificLinks(this, Executionlink.class).isEmpty() == false) {
				gatherer.gatherRuntimeInformation();
				processor.processGatheredInformation();
				if (par != null) {
					setWorkflowTaskState(Status.ACTIVE);
					ParallelProcessor proc = new ParallelProcessor(this, par);
					proc.performParallelization();
				}

				// Commented out as finished state gets overridden when looped tasks are skipped
				// setWorkflowTaskState(Status.ACTIVE);
			} else {
				if (par != null) {
					ParallelProcessor proc = new ParallelProcessor(this, par);
					proc.performParallelization();
				} else {
					processor.processGatheredInformation();
				}
				if (this.workflowTaskState.getValue() == Status.FINISHED_VALUE
						|| this.workflowTaskState.getValue() == Status.SKIPPED_VALUE) {
					// Required when loop is already set to finished when all ptasks are skipped
					LOGGER.info("Loop is already finished");
				} else {
					setWorkflowTaskState(Status.ACTIVE);
					setWorkflowTaskStateMessage("No executionlink provided for Loop transferring to state active "
							+ "in case decision input is provided manually!");
				}
			}
			break;
		case Status.INACTIVE_VALUE:
			if (processing == true) {
				return;
			}
			processing = true;
			LOGGER.debug("Fire transition(state=inactive, action=\"start\")...");
			setWorkflowTaskState(Status.ACTIVE);
			if (ModelUtility.getSpecificLinks(this, Executionlink.class).isEmpty() == false) {
				gatherer.gatherRuntimeInformation();
				processor.processGatheredInformation();
			} else {
				setWorkflowTaskStateMessage("No executionlink provided for Loop transferring to state active "
						+ "in case decision input is provided manually!");
			}
			break;
		default:
			break;
		}
		processing = false;
	}

	private Parallelloop getParallelMixin() {
		for (MixinBase mixB : this.getParts()) {
			if (mixB instanceof Parallelloop) {
				return (Parallelloop) mixB;
			}
		}
		return null;
	}

	public void observeLoopTasks() {
		StringBuilder str = new StringBuilder();
		for (Taskdependency tdep : ModelUtility.getTaskDependencyLinks(this.getRlinks())) {
			Task srcTask = (Task) tdep.getSource();
			if (buildsLoop(srcTask, this)) {
				TaskConnector conn = (TaskConnector) srcTask;
				conn.attachObserver(this);
				str.append(conn.getTitle());
			}
		}
		LOGGER.info("Loop: " + this.getTitle() + " observes: " + str.toString());
	}

	// End of user code
	// Start of user code Loop_Kind_stop_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.ugoe.cs.rwm/workflow/task/action# - term: stop - title:
	 */
	@Override
	public void stop() {
		LOGGER.debug("Action stop() called on " + this);

		// Decision State Machine.
		switch (getWorkflowTaskState().getValue()) {

		case Status.ACTIVE_VALUE:
			LOGGER.debug("Fire transition(state=active, action=\"stop\")...");
			// TODO Implement transition(state=active, action="stop")
			setWorkflowTaskState(Status.INACTIVE);
			break;

		default:
			break;
		}
	}
	// End of user code

	public synchronized Gatherer getGatherer() {
		return gatherer;
	}

	public synchronized void setGatherer(Gatherer gatherer) {
		this.gatherer = gatherer;
	}

	public Processor getProcessor() {
		return processor;
	}

	public void setProcessor(Processor processor) {
		this.processor = processor;
	}

	public void update() {
		if (this.getWorkflowTaskState().getValue() == Status.ACTIVE_VALUE) {
			setIterationCount(this);

			if (getParallelMixin() != null) {
				if (ModelUtility.getLoopMixin(this) instanceof Foreach) {
					ForEachLoopProcessor fep = new ForEachLoopProcessor(this);
					// fep.redistributeItems();
				}
			}

			if (nextIterationRequired()) {
				LOGGER.info("Updating Loop: " + this);
				// gatherer = GathererFactory.getGatherer(this);
				processor = ProcessorFactory.getProcessor(this);
				// gatherer.gatherRuntimeInformation();
				processor.processGatheredInformation();
			} else {
				LOGGER.info("Loop tasks not completely finished => No Loop Update required!");
				processor = ProcessorFactory.getProcessor(this);
				// gatherer.gatherRuntimeInformation();
				processor.processGatheredInformation();
			}
		} else {
			LOGGER.info("Workflow not in Active State => No Loop Update required!");
		}
	}

	private boolean nextIterationRequired() {
		for (Link rLink : this.getRlinks()) {
			if (rLink.getSource() instanceof Task && rLink instanceof Taskdependency) {
				Task srcTask = (Task) rLink.getSource();
				if (buildsLoop(srcTask, this)) {
					if (srcTask.getWorkflowTaskState().getValue() == Status.FINISHED_VALUE == false) {
						return false;
					}
					if (srcTask.getWorkflowTaskState().getValue() == Status.SKIPPED_VALUE == false) {
						return false;
					}
				}
			}
		}
		return true;
	}

	public static boolean buildsLoop(Task sourceTask, Task loop) {
		HashSet checked = new HashSet();
		return buildsLoop(sourceTask, loop, checked);
	}

	private static boolean buildsLoop(Task sourceTask, Task loop, HashSet checked) {
		if (checked.contains(sourceTask.getId())) {
			LOGGER.info("Source task " + sourceTask + " part of loop but not of looped asked for: " + loop);
			return false;
		}
		checked.add(sourceTask.getId());
		for (Task pTask : getPreviousTasks(sourceTask)) {
			if (pTask.getId().equals(loop.getId())) {
				return true;
			} else if (pTask.getId().equals(sourceTask.getId()) == false) {
				return buildsLoop(pTask, loop, checked);
			}
		}
		return false;
	}

	private static BasicEList<Task> getPreviousTasks(Task sourceTask) {
		BasicEList<Task> tasks = new BasicEList<Task>();
		for (Taskdependency tdep : ModelUtility.getTaskDependencyLinks(sourceTask.getRlinks())) {
			Task task = (Task) tdep.getSource();
			tasks.add(task);
		}
		return tasks;
	}

	private void setIterationCount(Loop loop) {
		AttributeState attr = getLoopIterationAttribute(loop);
		if (isNumeric(attr.getValue())) {
			int i = Integer.parseInt(attr.getValue());
			i++;
			attr.setValue(String.valueOf(i));
			loop.setLoopIterationCount(i);
		} else {
			attr.setValue("1");
			loop.setLoopIterationCount(1);
		}
	}

	private AttributeState getLoopIterationAttribute(Loop loop) {
		for (AttributeState attr : loop.getAttributes()) {
			if (attr.getName().equals("loop.iteration.count")) {
				return attr;
			}
		}
		AttributeState loopIteration = OCCIFactory.eINSTANCE.createAttributeState();
		loopIteration.setName("loop.iteration.count");
		loopIteration.setValue(String.valueOf(loop.getLoopIterationCount()));
		loop.getAttributes().add(loopIteration);
		return loopIteration;
	}

	private static boolean isNumeric(String strNum) {
		try {
			Integer.parseInt(strNum);
		} catch (NumberFormatException | NullPointerException nfe) {
			return false;
		}
		return true;
	}

	public List<TaskConnector> getLoopedTasks() {
		EList<TaskConnector> loopedTasks = new BasicEList<>();
		for (Taskdependency tdep : ModelUtility.getTaskDependencyLinks(this.getRlinks())) {
			TaskConnector srcTask = (TaskConnector) tdep.getSource();
			if (LoopConnector.buildsLoop(srcTask, this)) {
				loopedTasks.add(srcTask);
			}
		}
		return loopedTasks;
	}

	public void attachObserver(LoopConnector obs) {
		this.observers.add(obs);
	}

	public void dettachObserver(LoopConnector obs) {
		this.observers.remove(obs);
	}

	public void emptyObserverList() {
		this.observers.clear();
	}

	private void printObserver() {
		for (LoopConnector conn : this.observers) {
			System.out.println("Observer of " + this);
			System.out.println("                 " + conn);
		}
	}

	public void setState(Status status) {
		this.setWorkflowTaskState(status);
		if (status.equals(Status.FINISHED)) {
			notifyObserver();
		}
	}

	private void notifyObserver() {
		LOGGER.info("Notifying Loop Observer (" + observers.size() + ") of Loop: " + this.getId());
		for (LoopConnector observer : observers) {
			observer.update();
		}
	}
}
