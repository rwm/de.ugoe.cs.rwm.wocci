/**
 * Copyright (c) 2016-2017 Inria
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 *
 * Generated at Tue Aug 28 15:09:33 CEST 2018 from platform:/resource/workflow/model/workflow.occie by org.eclipse.cmf.occi.core.gen.connector
 */

package de.ugoe.cs.rwm.wocci.connector;

import org.eclipse.cmf.occi.core.*;
import org.eclipse.cmf.occi.infrastructure.*;
import org.eclipse.emf.common.util.EList;
import org.modmacao.ansibleconfiguration.Ansibleendpoint;
import org.modmacao.cm.ansible.AnsibleHelper;
import org.modmacao.occi.platform.Component;
import org.modmacao.placement.Placementlink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import workflow.*;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Connector implementation for the OCCI kind: - scheme:
 * http://schemas.ugoe.cs.rwm/workflow# - term: datalink - title: DataLink
 */
public class DatalinkConnector extends workflow.impl.DatalinkImpl {

	/**
	 * Initialize the logger.
	 */
	private static Logger LOGGER = LoggerFactory.getLogger(DatalinkConnector.class);
	Path keyPath = Paths.get(new AnsibleHelper().getProperties().getProperty("private_key_path"));
	String defaultUser = new AnsibleHelper().getProperties().getProperty("ansible_user");

	// Start of user code Datalinkconnector_constructor
	/**
	 * Constructs a datalink connector.
	 */
	DatalinkConnector() {
		LOGGER.debug("Constructor called on " + this);
		// TODO: Implement this constructor.
	}
	// End of user code
	//
	// OCCI CRUD callback operations.
	//

	// Start of user code DatalinkocciCreate
	/**
	 * Called when this Datalink instance is completely created.
	 */
	@Override
	public void occiCreate() {
		LOGGER.debug("occiCreate() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code Datalink_occiRetrieve_method
	/**
	 * Called when this Datalink instance must be retrieved.
	 */
	@Override
	public void occiRetrieve() {
		LOGGER.debug("occiRetrieve() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code Datalink_occiUpdate_method
	/**
	 * Called when this Datalink instance is completely updated.
	 */
	@Override
	public void occiUpdate() {
		LOGGER.debug("occiUpdate() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code DatalinkocciDelete_method
	/**
	 * Called when this Datalink instance will be deleted.
	 */
	@Override
	public void occiDelete() {
		LOGGER.debug("occiDelete() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	//
	// Datalink actions.
	//

	// Start of user code Datalink_Kind_stop_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.ugoe.cs.rwm/workflow/datalink/action# - term: stop - title:
	 */
	@Override
	public void stop() {
		LOGGER.debug("Action stop() called on " + this);

		// Task State Machine.
		switch (getTaskDatalinkState().getValue()) {

		case Status.ACTIVE_VALUE:
			LOGGER.debug("Fire transition(state=active, action=\"stop\")...");
			// TODO Implement transition(state=active, action="stop")
			setAttribute("task.datalink.state", "inactive");
			break;

		default:
			break;
		}
	}

	// End of user code
	// Start of user code Datalink_Kind_start_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.ugoe.cs.rwm/workflow/datalink/action# - term: start - title:
	 */
	@Override
	public void start() {
		LOGGER.debug("Action start() called on " + this);
		// Task State Machine.
		switch (getTaskDatalinkState().getValue()) {

		case Status.SCHEDULED_VALUE:
			LOGGER.debug("Fire transition(state=scheduled, action=\"start\")...");
			LOGGER.info("Start data transference");
			LOGGER.info(this.getTaskDatalinkState().toString());
			LOGGER.info(this.taskDatalinkState.toString());
			// this.setTaskDatalinkState(Status.ACTIVE);

			setAttribute("task.datalink.state", "active");

			LOGGER.info(this.getTaskDatalinkState().toString());
			LOGGER.info(this.taskDatalinkState.toString());
			// this.setTaskDatalinkStateMessage("ACTIVE_MESSAGE");

			Task srcTask = (Task) this.getSource();
			LOGGER.info("Src Task detected: " + srcTask.getTitle());
			Task tarTask = (Task) this.getTarget();
			LOGGER.info("Tar Task detected: " + tarTask.getTitle());
			Compute srcHost = getHost(srcTask);
			Compute tarHost = getHost(tarTask);

			if (srcHost == null || tarHost == null || tarHost.getOcciComputeState()
					.getValue() != org.eclipse.cmf.occi.infrastructure.ComputeStatus.ACTIVE.getValue()) {

				setAttribute("task.datalink.state.message", "Target or Source Compute Node Inactive!");
				setAttribute("task.datalink.state", "error");
				return;
			}

			Remotedatacanal rmdCanal = getRemotedatacanal();

			if (rmdCanal != null) {
				LOGGER.info("Remotedatacanal detected!");
				srcHost = getComputeNode(rmdCanal.getTaskDatalinkSourceResource());
				tarHost = getComputeNode(rmdCanal.getTaskDatalinkTargetResource());

			}

			boolean specialCanal = false;
			for (MixinBase mixin : this.getParts()) {

				if (mixin instanceof Networkcanal) {
					Networkcanal nwCanal = (Networkcanal) mixin;
					transferVia(srcHost, tarHost, getNetwork(nwCanal.getTaskDatalinkNetwork()));
					specialCanal = true;
					break;
				} else if (mixin instanceof Storagecanal) {
					LOGGER.info("StorageCanal: Not implemented yet.");
					// Storagecanal storCanal = (Storagecanal) mixin;
					// Storage stor = getStorage(storCanal.getTaskDatalinkStorage());
					specialCanal = true;
					break;

				} else if (mixin instanceof Localcanal) {
					// Localcanal localCanal = (Localcanal) mixin;
					LOGGER.info("LocalCanal: Not implemented yet.");
					specialCanal = true;
					break;
				}
			}

			if (specialCanal == false) {
				LOGGER.info("No Special Canal Detected: Using ManagementNetwork");
				transferViaManagementNetwork(srcHost, tarHost);
			}

			setAttribute("task.datalink.state", "finished");

			break;

		case Status.INACTIVE_VALUE:
			LOGGER.debug("Fire transition(state=inactive, action=\"start\")...");

			setAttribute("task.datalink.state", "active");
			setAttribute("task.datalink.state", "final");
			break;

		default:
			break;
		}
	}

	private void setAttribute(String attribute, String value) {
		for (AttributeState attr : this.getAttributes()) {
			if (attr.getName().equals(attribute)) {
				attr.setValue(value);
			}
		}

		this.setTaskDatalinkState(Status.getByName(value));
	}

	private void transferVia(Compute srcHost, Compute tarHost, Network network) {
		String srcManagementIp = getIPAddress(srcHost);
		String srcIP = getIPAddressOfNetwork(srcHost, network);
		String tarIP = getIPAddressOfNetwork(tarHost, network);

		try {
			setupSSHConfig(srcManagementIp, tarIP, keyPath);
		} catch (IOException | InterruptedException e) {
			LOGGER.info("SSH Setup for remote VMs Failed");
			this.setTaskDatalinkState(Status.ERROR);
			e.printStackTrace();
		}
		try {
			startTransferation(srcManagementIp, srcIP, tarIP, keyPath, this.taskDatalinkSourceFile,
					this.taskDatalinkTargetFile);
		} catch (IOException e) {
			LOGGER.info("Data transference failed");
			this.setTaskDatalinkState(Status.ERROR);
			e.printStackTrace();
		} catch (InterruptedException e) {
			LOGGER.info("Data transference failed");
			this.setTaskDatalinkState(Status.ERROR);
			e.printStackTrace();
		}
	}

	private void startTransferation(String srcManagementIp, String srcIP, String tarIP, Path keyPath,
			String taskDatalinkSourceFile, String taskDatalinkTargetFile) throws IOException, InterruptedException {

		LOGGER.info("Start Transferation: echo 'rsync -arvce \"ssh -q -o StrictHostKeyChecking=no" + ""
				+ " -o UserKnownHostsFile=/dev/null\" --progress " + taskDatalinkSourceFile + " "+ defaultUser +"@" + tarIP + ":"
				+ taskDatalinkTargetFile
				+ "' | ssh -o \"StrictHostKeyChecking=no\" -o \"UserKnownHostsFile=/dev/null\" -i "
				+ keyPath.toAbsolutePath() + " " + defaultUser + "@" + srcManagementIp);

		/*
		 * List<String> commands = new ArrayList<String>(); commands.add("/bin/sh");
		 * commands.add("-c"); commands.add(
		 * "echo 'rsync -arvce \"ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null\" --progress "
		 * + taskDatalinkSourceFile + " ubuntu@" + tarIP + ":" + taskDatalinkTargetFile
		 * +
		 * "' | ssh -o \"StrictHostKeyChecking=no\" -o \"UserKnownHostsFile=/dev/null\" -q -i "
		 * + keyPath.toAbsolutePath() + " ubuntu@" + srcManagementIp);
		 *
		 * Process process = new ProcessBuilder(commands).inheritIO().start();
		 * process.waitFor();
		 */

		/*
		 * LOGGER.
		 * info("Start Transferation: echo scp -q -o \"StrictHostKeyChecking=no\" -o UserKnownHostsFile=/dev/null "
		 * + taskDatalinkSourceFile + " ubuntu@" + tarIP + ":" + taskDatalinkTargetFile
		 * + " | ssh -i " + keyPath.toAbsolutePath() + " ubuntu@" + srcManagementIp);
		 *
		 * List<String> commands = new ArrayList<String>(); commands.add("/bin/sh");
		 * commands.add("-c"); commands.
		 * add("echo scp -q -o \"StrictHostKeyChecking=no\" -o \"UserKnownHostsFile=/dev/null\" "
		 * + taskDatalinkSourceFile + " ubuntu@" + tarIP + ":" + taskDatalinkTargetFile
		 * + " | ssh -q -i " + keyPath.toAbsolutePath() + " ubuntu@" + srcManagementIp);
		 *
		 * Process process = new ProcessBuilder(commands).inheritIO().start();
		 * process.waitFor();
		 */

	}

	private String getIPAddressOfNetwork(Compute srcHost, Network network) {
		for (Link rLink : network.getRlinks()) {
			if (rLink instanceof Networkinterface) {
				for (Link link : srcHost.getLinks()) {
					if (link instanceof Networkinterface) {
						if (rLink.getId().equals(link.getId())) {
							for (MixinBase base : link.getParts()) {
								if (base instanceof Ipnetworkinterface) {
									return ((Ipnetworkinterface) base).getOcciNetworkinterfaceAddress();
								}
							}
						}
					}
				}
			}
		}

		throw new NoSuchElementException(
				"Compute: " + srcHost.getId() + " is not connected to Network:" + network.getId());
	}

	private Network getNetwork(String taskDatalinkNetwork) {
		Configuration config = getConfiguration();
		EList<Resource> resources = config.getResources();
		for (Resource res : resources) {
			if (res instanceof Network) {
				LOGGER.info("NETWORK: " + res);
				Network network = (Network) res;
				if (taskDatalinkNetwork.contains(network.getId())) {
					LOGGER.info("Network found: " + res.getId());
					return (Network) res;
				}
			}
		}
		throw new NoSuchElementException("Configuration does not contain a network with id: " + taskDatalinkNetwork);
	}

	private Configuration getConfiguration() {
		if (this.eContainer instanceof Task) {
			Task task = (Task) this.eContainer;
			if (task.eContainer() instanceof Configuration) {
				Configuration config = (Configuration) task.eContainer();
				LOGGER.info("Top Level Configuration found " + config.getResources());
				return config;
			}
		}
		throw new NoSuchElementException("Datalink or Task are not contained within an Configuration!");
	}

	private Compute getComputeNode(String taskDatalinkSourceResource) {
		Configuration config = getConfiguration();
		EList<Resource> resources = config.getResources();
		for (Resource res : resources) {
			if (res instanceof Compute) {
				if (res.getId().equals(taskDatalinkSourceResource)) {
					return (Compute) res;
				}
			}
		}
		throw new NoSuchElementException(
				"Configuration does not contain a network with id: " + taskDatalinkSourceResource);
	}

	private Storage getStorage(String taskDatalinkStorage) {
		Configuration config = getConfiguration();
		EList<Resource> resources = config.getResources();
		for (Resource res : resources) {
			if (res instanceof Storage) {
				if (res.getId().equals(taskDatalinkStorage)) {
					return (Storage) res;
				}
			}
		}
		throw new NoSuchElementException("Configuration does not contain a network with id: " + taskDatalinkStorage);
	}

	private Remotedatacanal getRemotedatacanal() {
		for (MixinBase mixin : this.getParts()) {
			if (mixin instanceof Remotedatacanal) {
				return (Remotedatacanal) mixin;
			}
		}
		return null;
	}

	private Compute getHost(Task task) {
		for (Link execLink : task.getLinks()) {
			if (execLink instanceof Executionlink) {
				Component comp = (Component) execLink.getTarget();
				for (Link placLink : comp.getLinks()) {
					if (placLink instanceof Placementlink) {
						return (Compute) placLink.getTarget();
					}
				}
			}
		}
		return null;
	}

	// End of user code
	// Start of user code Datalink_Kind_schedule_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.ugoe.cs.rwm/workflow/datalink/action# - term: schedule -
	 * title:
	 */
	@Override
	public void schedule() {
		LOGGER.debug("Action schedule() called on " + this);

		// Task State Machine.
		switch (getTaskDatalinkState().getValue()) {
		case Status.FINISHED_VALUE:
			LOGGER.debug("Fire transition(state=error, action=\"schedule\")...");
			setAttribute("task.datalink.state", "scheduled");
			break;

		case Status.ERROR_VALUE:
			LOGGER.debug("Fire transition(state=error, action=\"schedule\")...");
			setAttribute("task.datalink.state", "scheduled");
			break;

		case Status.INACTIVE_VALUE:
			LOGGER.debug("Fire transition(state=inactive, action=\"schedule\")...");
			// TODO Implement transition(state=inactive, action="schedule")
			setAttribute("task.datalink.state", "scheduled");
			break;

		default:
			break;
		}
	}

	private void transferViaManagementNetwork(Compute srcHost, Compute tarHost) {
		String srcIP = getIPAddress(srcHost);
		String tarIP = getIPAddress(tarHost);

		AnsibleHelper helper = new AnsibleHelper();
		Path keyPath = Paths.get(helper.getProperties().getProperty("private_key_path"));

		try {
			setupSSHConfig(srcIP, tarIP, keyPath);
		} catch (IOException | InterruptedException e) {
			LOGGER.info("SSH Setup for remote VMs Failed");
			this.setTaskDatalinkState(Status.ERROR);
			e.printStackTrace();
		}
		try {
			startTransferation(srcIP, tarIP, keyPath, this.taskDatalinkSourceFile, this.taskDatalinkTargetFile);
		} catch (InterruptedException | IOException e) {
			LOGGER.info("Data transference failed");
			this.setTaskDatalinkState(Status.ERROR);
			e.printStackTrace();
		}
		// cleanUpSSHKeys(srcIP, tarIP, keyPath);
	}

	private void cleanUpSSHKeys(String srcIP, String tarIP, Path keyPath) {
		// TODO Auto-generated method stub

	}

	private void startTransferation(String srcIP, String tarIP, Path keyPath, String taskDatalinkSourceFile,
			String taskDatalinkTargetFile) throws InterruptedException, IOException {

		LOGGER.info("Start Transferation: echo 'rsync -arvce \"ssh -q -o StrictHostKeyChecking=no"
				+ " -o UserKnownHostsFile=/dev/null\" --progress " + taskDatalinkSourceFile + " " + defaultUser +"@" + tarIP + ":"
				+ taskDatalinkTargetFile
				+ "' | ssh -o \"StrictHostKeyChecking=no\" -o \"UserKnownHostsFile=/dev/null\" -i "
				+ keyPath.toAbsolutePath() + " " + defaultUser + "@" + srcIP);


		List<String> commands = new ArrayList<String>();
		commands.add("/bin/sh");
		commands.add("-c");
		commands.add(
		"echo 'rsync -arvce \"ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null\" --progress "
		+ taskDatalinkSourceFile + " " + defaultUser +"@" + tarIP + ":" + taskDatalinkTargetFile
		+ "' | ssh -o \"StrictHostKeyChecking=no\" -o \"UserKnownHostsFile=/dev/null\" -q -i "
		+ keyPath.toAbsolutePath() + " " + defaultUser +"@" + srcIP);

		Process process = new ProcessBuilder(commands).inheritIO().start();
		process.waitFor();

	}

	private void setupSSHConfig(String srcIP, String tarIP, Path keyPath) throws IOException, InterruptedException {
		transferIdrsa(srcIP, keyPath);
	}


	private void transferIdrsa(String srcIP, Path keyPath) throws IOException, InterruptedException {
		LOGGER.info("rsync -arvce \"ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i "
				+ keyPath.toAbsolutePath().toString() + "\" ~/id_rsa "+ defaultUser+ "@" + srcIP + ":~/.ssh/id_rsa");

		List<String> commands = new ArrayList<String>();
		commands.add("/bin/sh");
		commands.add("-c");
		commands.add("rsync -arvce \"ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i "
		+ keyPath.toAbsolutePath() + "\" --progress " + " ~/id_rsa " + " " + defaultUser +"@"
		+ srcIP + ":" + "~/.ssh/id_rsa");

		Process process = new ProcessBuilder(commands).inheritIO().start();
		process.waitFor();
	}

	public int executeScript(String script) {
		Process process = null;
		try {
			process = new ProcessBuilder(script).inheritIO().start();
			try {
				process.waitFor();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (process != null) {
			return process.exitValue();
		} else {
			return -1;
		}
	}

	// TODO: Adjust to new getIPAddress
	private String getIPAddress(Resource compute) {
		EList<Link> links = compute.getLinks();
		Networkinterface networklink = null;
		String ipaddress = null;

		List<Link> endpointCandidates = new LinkedList<Link>();

		for (Link link : links) {
			if (link instanceof Networkinterface) {
				LOGGER.info("Found network interface for " + target);
				endpointCandidates.add(link);
				for (MixinBase mixin : link.getParts()) {
					if (mixin instanceof Ansibleendpoint) {
						LOGGER.info("Found explicitly specified endpoint for " + target);
						networklink = (Networkinterface) link;
						endpointCandidates.clear();
						networklink.occiRetrieve();
						for (MixinBase base : networklink.getParts()) {
							if (base instanceof Ipnetworkinterface) {
								ipaddress = ((Ipnetworkinterface) base).getOcciNetworkinterfaceAddress();
								return ipaddress;
							}
						}
					}
				}
			}
		}

		if (endpointCandidates.size() > 0) {
			networklink = (Networkinterface) endpointCandidates.get(0);
		}

		if (networklink == null) {
			LOGGER.error("No network interface found for " + target);
		} else {
			// Retrieving object to ensure ip address is correct
			networklink.occiRetrieve();
			for (MixinBase base : networklink.getParts()) {
				if (base instanceof Ipnetworkinterface) {
					ipaddress = ((Ipnetworkinterface) base).getOcciNetworkinterfaceAddress();
				}
			}

		}

		return ipaddress;
	}

}
