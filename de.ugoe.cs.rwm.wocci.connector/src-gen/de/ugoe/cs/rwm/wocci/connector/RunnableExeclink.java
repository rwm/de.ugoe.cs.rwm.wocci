/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.connector;

import org.eclipse.cmf.occi.core.Resource;
import org.modmacao.occi.platform.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import workflow.Task;

/**
 * Slave objects
 *
 * @author erbel
 *
 */
public class RunnableExeclink implements Runnable {
	static Logger LOGGER = LoggerFactory.getLogger(TaskConnector.class);
	private Task task;
	private Resource res;
	private String action;

	public RunnableExeclink(TaskConnector task, Resource res, String action) {
		this.res = res;
		this.action = action;
		this.task = task;

	}

	@Override
	public void run() {
		LOGGER.info("Executing Task: " + task + " with component " + res);
		if (action.equals("start")) {
			performStart();

		} else if (action.equals("stop")) {
			performStop();
		} else if (action.equals("schedule")) {
			performSchedule();
		} else if (action.equals("skip")) {
			performSkip();
		}
	}

	private void performSkip() {
		// TODO Auto-generated method stub

	}

	private void performSchedule() {
		// TODO Auto-generated method stub

	}

	private void performStop() {
		// TODO Auto-generated method stub

	}

	private void performStart() {
		Component comp = (Component) res;
		LOGGER.info("Executable detected: " + comp);
		if (comp.getOcciComponentState().getValue() == org.modmacao.occi.platform.Status.UNDEPLOYED.getValue()) {
			LOGGER.info("Deploying Executable: " + comp);
			comp.deploy();
		}
		if (comp.getOcciComponentState().getValue() == org.modmacao.occi.platform.Status.DEPLOYED.getValue()) {
			LOGGER.info("Configuring Executable: " + comp);
			comp.configure();
		}
		LOGGER.info("Starting Executable: " + comp.getTitle() + comp.getLocation());
		comp.start();
	}
}
