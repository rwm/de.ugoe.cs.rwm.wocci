package de.ugoe.cs.rwm.wocci.connector.decision.gatherer;

import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.MixinBase;
import org.eclipse.cmf.occi.core.Resource;
import org.modmacao.occi.platform.Application;
import org.modmacao.occi.platform.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.ugoe.cs.rwm.wocci.connector.DecisionConnector;
import de.ugoe.cs.rwm.wocci.connector.util.ModelUtility;
import monitoring.Monitorableproperty;
import monitoring.Sensor;
import workflow.*;

public abstract class AbsGatherer implements Gatherer {
	protected final static Logger LOGGER = LoggerFactory.getLogger(DecisionConnector.class);
	protected Decision decision;

	protected abstract void gatherFromComponent(Component comp);

	protected abstract void gatherFromApplication(Application app);

	protected abstract void gatherFromSensor(Sensor sens);

	public AbsGatherer(Decision decision) {
		this.decision = decision;
	}

	@Override
	public void gatherRuntimeInformation() {
		LOGGER.info("Current decision input: " + decision.getWorkflowDecisionInput());
		LOGGER.info("For each: " + isForEachLooped());
		if (decision.getWorkflowDecisionInput().equals("") && isForEachLooped() == false) {
			decision.setWorkflowTaskStateMessage("Waiting for Decision Input");
			ModelUtility.getTaskStateMessage(decision).setValue("Waiting for Decision Input");

			for (Executionlink link : ModelUtility.getSpecificLinks(decision, Executionlink.class)) {
				LOGGER.info("Found ExecutionLink: " + link.getTitle());
				Resource target = link.getTarget();

				// Sensor has to be checked before Application due to inheritance
				if (target instanceof Component) {
					Component comp = (Component) target;
					LOGGER.info("Executable detected: " + comp);
					startComponent(comp);
					gatherFromComponent(comp);
				} else if (target instanceof Sensor) {
					Sensor sens = (Sensor) target;
					LOGGER.info("Decision-Sensor detected: " + sens);
					startSensor(sens);
					gatherFromSensor(sens);
				} else if (target instanceof Application) {
					Application app = (Application) target;
					LOGGER.info("Executable Application detected: " + app);
					startApplication(app);
					gatherFromApplication(app);
				} else {
					setErrorMessage();
					return;
				}

				decision.setWorkflowTaskStateMessage("Runtime information gathered!");
				ModelUtility.getTaskStateMessage(decision).setValue("Runtime information gathered!");
			}
		}
	}

	private boolean isForEachLooped() {
		MixinBase loop = ModelUtility.getLoopMixin(decision);
		if (loop instanceof Foreach) {
			Foreach fe = (Foreach) loop;
			Loop loopResource = (Loop) fe.getEntity();
			if (loopResource.getLoopIterationCount() != null && loopResource.getLoopIterationCount() > 0) {
				return true;
			}
		}
		return false;
	}

	private void setErrorMessage() {
		decision.setWorkflowTaskState(Status.ERROR);
		decision.setWorkflowTaskStateMessage("Error when evaluating target of Executionlink");
	}

	protected void startApplication(Application app) {
		switch (app.getOcciAppState().getValue()) {
		case org.modmacao.occi.platform.Status.UNDEPLOYED_VALUE:
			app.deploy();
			app.configure();
			app.start();
			break;
		case org.modmacao.occi.platform.Status.DEPLOYED_VALUE:
			app.configure();
			app.start();
			break;
		case org.modmacao.occi.platform.Status.INACTIVE_VALUE:
			app.start();
			break;
		case org.modmacao.occi.platform.Status.ACTIVE_VALUE:
			LOGGER.info("Application already active. Restarting it!");
			app.stop();
			app.start();
			break;
		default:
			break;
		}
	}

	protected void startSensor(Sensor sens) {
		switch (sens.getOcciAppState().getValue()) {
		case org.modmacao.occi.platform.Status.UNDEPLOYED_VALUE:
			sens.deploy();
			sens.configure();
			sens.start();
			break;
		case org.modmacao.occi.platform.Status.DEPLOYED_VALUE:
			sens.configure();
			sens.start();
			break;
		case org.modmacao.occi.platform.Status.INACTIVE_VALUE:
			sens.start();
			break;
		case org.modmacao.occi.platform.Status.ACTIVE_VALUE:
			LOGGER.info("Sensor already active. Restarting it!");
			// The dummy results in no monitoring results gathered.
			// Skipping restart behaviour for now
			// sens.stop();
			// sens.start();
			break;
		default:
			break;
		}
	}

	protected void startComponent(Component comp) {
		switch (comp.getOcciComponentState().getValue()) {
		case org.modmacao.occi.platform.Status.UNDEPLOYED_VALUE:
			comp.deploy();
			comp.configure();
			comp.start();
			break;
		case org.modmacao.occi.platform.Status.DEPLOYED_VALUE:
			comp.configure();
			comp.start();
			break;
		case org.modmacao.occi.platform.Status.INACTIVE_VALUE:
			comp.start();
			break;
		case org.modmacao.occi.platform.Status.ACTIVE_VALUE:
			LOGGER.info("Component already active");
			break;
		default:
			break;
		}
	}

	protected String getMonitoringResult(Sensor sens) {
		for (Link link : sens.getLinks()) {
			if (link instanceof Monitorableproperty) {
				Monitorableproperty monProp = (Monitorableproperty) link;
				return monProp.getMonitoringResult();
			}
		}
		return "";
	}
}
