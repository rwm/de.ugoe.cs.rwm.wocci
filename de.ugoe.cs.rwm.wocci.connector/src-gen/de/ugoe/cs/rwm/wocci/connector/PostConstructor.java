/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.connector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Slave objects
 *
 * @author erbel
 *
 */
public class PostConstructor implements Runnable {
	private static final long ONE_SECOND = 1000;
	static Logger LOGGER = LoggerFactory.getLogger(TaskConnector.class);
	private LoopConnector loop;
	private int timeout;

	public PostConstructor(LoopConnector loop, int timeout) {
		this.loop = loop;
		this.timeout = timeout;
	}

	@Override
	public void run() {
		int count = 0;
		while(loop.eContainer() == null && count<timeout){
			LOGGER.info("Waiting for eContainer to be loaded!");
			try {
				Thread.sleep(ONE_SECOND);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			count++;
		}
		loop.observeLoopTasks();
		loop.update();
	}

}
