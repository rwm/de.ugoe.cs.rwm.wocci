package de.ugoe.cs.rwm.wocci.connector.decision.processor;

public interface Processor {
	public void processGatheredInformation();

	public void scheduleDecision();
}
