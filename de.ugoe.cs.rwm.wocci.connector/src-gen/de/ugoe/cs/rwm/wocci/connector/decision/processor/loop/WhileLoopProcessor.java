package de.ugoe.cs.rwm.wocci.connector.decision.processor.loop;

import de.ugoe.cs.rwm.wocci.connector.util.ModelUtility;
import workflow.Decision;
import workflow.Loopiteration;

public class WhileLoopProcessor extends AbsLoopProcessor {

	public WhileLoopProcessor(Decision decision) {
		super(decision);
	}

	@Override
	protected void executeSchedulingSpecifics() {
		decision.setWorkflowDecisionResult("");
		rescheduleTaskSequence(decision);
	}

	@Override
	protected Loopiteration createIterationVar() {
		return ModelUtility.createLoopiteration("whilecount", decision.getWorkflowDecisionInput());
	}

}
