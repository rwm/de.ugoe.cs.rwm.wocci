package de.ugoe.cs.rwm.wocci.connector;

import static org.junit.Assert.assertTrue;

import org.eclipse.cmf.occi.core.MixinBase;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.wocci.connector.decision.gatherer.DecisionGatherer;
import de.ugoe.cs.rwm.wocci.connector.decision.processor.DecisionProcessor;
import de.ugoe.cs.rwm.wocci.connector.decision.processor.loop.ForEachLoopProcessor;
import de.ugoe.cs.rwm.wocci.connector.decision.processor.loop.ForLoopProcessor;
import de.ugoe.cs.rwm.wocci.connector.decision.processor.loop.WhileLoopProcessor;
import de.ugoe.cs.rwm.wocci.connector.util.ModelUtility;
import workflow.For;
import workflow.Foreach;
import workflow.While;
import workflow.WorkflowFactory;

public class DecisionSetupTest {
	static ConnectorFactory fac = new ConnectorFactory();
	static WorkflowFactory wFac = WorkflowFactory.eINSTANCE;
	static LoopConnector dec;

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.extensionRegistrySetup();
	}

	@Before
	public void createDecision() {
		dec = (LoopConnector) fac.createLoop();
	}

	@Test
	public void getLoopMixinTest() {
		MixinBase mixB = ModelUtility.getLoopMixin(dec);
		if (mixB == null) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		MixinBase forEach = wFac.createForeach();
		dec.getParts().add(forEach);
		mixB = ModelUtility.getLoopMixin(dec);
		if (mixB instanceof While || mixB instanceof For || mixB instanceof Foreach) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}

	}

	@Test
	public void decisioSetupTest() {
		dec.start();
		if (dec.getGatherer() instanceof DecisionGatherer) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		if (dec.getProcessor() instanceof DecisionProcessor) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
	}

	@Test
	public void forEachLoopSetupTest() {
		MixinBase forEach = wFac.createForeach();
		dec.getParts().add(forEach);
		dec.start();
		if (dec.getProcessor() instanceof ForEachLoopProcessor) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
	}

	@Test
	public void whileLoopSetupTest() {
		MixinBase wile = wFac.createWhile();
		dec.getParts().add(wile);
		dec.start();
		if (dec.getProcessor() instanceof WhileLoopProcessor) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
	}

	@Test
	public void forLoopSetupTest() {
		MixinBase forL = wFac.createFor();
		dec.getParts().add(forL);
		dec.start();
		if (dec.getProcessor() instanceof ForLoopProcessor) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
	}

	@Test
	public void shiftingTest() {
		MixinBase forL = wFac.createFor();
		dec.getParts().add(forL);
		dec.start();
		if (dec.getProcessor() instanceof ForLoopProcessor) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}

		dec.getParts().remove(forL);
		dec.start();
		if (dec.getProcessor() instanceof DecisionProcessor) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		MixinBase wile = wFac.createWhile();
		dec.getParts().add(wile);
		dec.start();
		if (dec.getProcessor() instanceof WhileLoopProcessor) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
	}

}
