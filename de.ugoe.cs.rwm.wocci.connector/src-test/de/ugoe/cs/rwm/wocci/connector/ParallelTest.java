package de.ugoe.cs.rwm.wocci.connector;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.*;

import org.eclipse.cmf.occi.core.*;
import org.eclipse.cmf.occi.core.util.OcciHelper;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.occiware.mart.server.model.ConfigurationManager;
import org.occiware.mart.server.model.EntityManager;

import de.ugoe.cs.rwm.wocci.connector.decision.processor.loop.ForEachLoopProcessor;
import de.ugoe.cs.rwm.wocci.connector.decision.processor.parallel.ParallelProcessor;
import de.ugoe.cs.rwm.wocci.connector.util.ModelUtility;
import workflow.*;

public class ParallelTest {
	static ConnectorFactory fac = new ConnectorFactory();
	static WorkflowFactory wFac = WorkflowFactory.eINSTANCE;
	static LoopConnector loop;
	static TaskConnector task;
	static ControlflowlinkConnector cLinkIn;
	static ControlflowlinkConnector cLinkOut;
	static Controlflowguard cGuard;
	static ForEachLoopProcessor proc;
	static Foreach fe;
	static Parallelloop par;
	static Extension workflowExt;
	static String decisionInput;
	static Configuration config;

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.extensionRegistrySetup();
		TestUtility.loggerSetup();
		workflowExt = OcciHelper.loadExtension("http://schemas.ugoe.cs.rwm/workflow#");
	}

	@Before
	public void createLoop() {
		config = OCCIFactory.eINSTANCE.createConfiguration();
		loop = (LoopConnector) fac.createLoop();
		loop.setWorkflowDecisionExpression("workflow.decision.input.isEmpty()");
		decisionInput = "hallo,kleine,welt,verteile,dich";
		loop.setWorkflowDecisionInput(decisionInput);
		loop.setLocation(getLocationOfEntity(loop));
		config.getResources().add(loop);

		fe = wFac.createForeach();
		fe.setLoopItemDelimiter(",");
		fe.setLoopItemName("loop.iteration.name");
		loop.getParts().add(fe);

		par = wFac.createParallelloop();
		par.setParallelReplicateNumber(2);
		loop.getParts().add(par);

		task = (TaskConnector) fac.createTask();
		task.setTitle("Following Task");
		task.setLocation(getLocationOfEntity(task));
		config.getResources().add(task);

		cLinkIn = (ControlflowlinkConnector) fac.createControlflowlink();
		cLinkIn.setTarget(task);
		cLinkIn.setSource(loop);
		cLinkIn.setLocation(getLocationOfEntity(cLinkIn));
		loop.getLinks().add(cLinkIn);

		cLinkOut = (ControlflowlinkConnector) fac.createControlflowlink();
		cLinkOut.setTarget(loop);
		cLinkOut.setSource(task);
		cLinkOut.setLocation(getLocationOfEntity(cLinkOut));
		task.getLinks().add(cLinkOut);
	}

	private void createMartServerModel(Configuration config) {
		for (Resource res : config.getResources()) {
			createResource(res);
		}
		for (Resource res : config.getResources()) {
			for (Link link : res.getLinks()) {
				createLink(link);
			}
		}
	}

	@Test
	public void parallelZeroProcessorConstructionTest() throws NoSuchFieldException, IllegalAccessException {
		createAndExecuteProcessor(1, 0);
	}

	@Test
	public void parallelOneProcessorConstructionTest() throws NoSuchFieldException, IllegalAccessException {
		createAndExecuteProcessor(1, 1);
	}

	@Test
	public void parallelTenProcessorConstructionTest() throws NoSuchFieldException, IllegalAccessException {
		decisionInput = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20";
		loop.setWorkflowDecisionInput(decisionInput);
		createAndExecuteProcessor(10, 10);
	}

	@Test
	public void parallelTenProcessorWithFewInputItemsTest() throws NoSuchFieldException, IllegalAccessException {
		decisionInput = "10,11,12,13,14,15";
		loop.setWorkflowDecisionInput(decisionInput);
		createAndExecuteProcessor(6, 10);
	}

	@Test
	public void parallelUnequalProcessorConstructionTest() throws IllegalAccessException, NoSuchFieldException {
		createAndExecuteProcessor(3, 3);

	}

	@Test
	public void parallelUnequalSplitProcessorConstructionTest() throws IllegalAccessException, NoSuchFieldException {
		createAndExecuteProcessor(3, 4);
	}

	@Test
	public void parallelToFewInputElementsProcessorConstructionTest()
			throws IllegalAccessException, NoSuchFieldException {
		List<String> input = Arrays.asList(loop.getWorkflowDecisionInput().split(","));
		createAndExecuteProcessor(input.size(), 8);
	}

	@Test
	public void wrongDelimiterTest() throws IllegalAccessException, NoSuchFieldException {
		fe.setLoopItemDelimiter(";");
		List<String> input = Arrays.asList(loop.getWorkflowDecisionInput().split(";"));

		int expectedSplitNumber = 1;
		int parNumber = 2;

		System.out.println("Par: " + parNumber + " | Expected Split: " + expectedSplitNumber);
		ParallelProcessor proc = testProcessor(expectedSplitNumber, parNumber);
		createMartServerModel(config);
		List<String> split = getSplit(proc);
		assertEquals(expectedSplitNumber, split.size());

		proc.performParallelization();

		Configuration config2 = ConfigurationManager.getConfigurationForOwner("anonymous");
		Task originalLoop = (Task) config2.getResources().stream()
				.filter(resource -> loop.getId().equals(resource.getId())).findFirst().get();
		assertEquals(1, ModelUtility.getSpecificLinks((Task) originalLoop, Nesteddependency.class).size());
	}

	private void createAndExecuteProcessor(int expectedSplitNumber, int parNumber)
			throws NoSuchFieldException, IllegalAccessException {
		System.out.println("Par: " + parNumber + " | Expected Split: " + expectedSplitNumber);
		ParallelProcessor proc = testProcessor(expectedSplitNumber, parNumber);
		createMartServerModel(config);
		List<String> split = getSplit(proc);
		assertEquals(expectedSplitNumber, split.size());

		proc.performParallelization();

		Configuration config2 = ConfigurationManager.getConfigurationForOwner("anonymous");
		Task originalLoop = (Task) config2.getResources().stream()
				.filter(resource -> loop.getId().equals(resource.getId())).findFirst().get();

		assertEquals(expectedSplitNumber,
				ModelUtility.getSpecificLinks((Task) originalLoop, Nesteddependency.class).size());
		checkReplicas(originalLoop);

	}

	private ParallelProcessor testProcessor(int expectedSplitNumber, int parNumber)
			throws NoSuchFieldException, IllegalAccessException {
		par.setParallelReplicateNumber(parNumber);
		assertEquals(ModelUtility.getSpecificLinks(loop, Nesteddependency.class).size(), 0);
		ParallelProcessor proc = new ParallelProcessor(loop, par);
		assertEquals(expectedSplitNumber, getSplit(proc).size());
		return proc;
	}

	private void checkReplicas(Task originalLoop) {
		String splittedDecisionInput = "";
		for (Nesteddependency dep : ModelUtility.getSpecificLinks(originalLoop, Nesteddependency.class)) {
			assertTrue(dep.getTarget() instanceof Loop);
			Loop nestedLoop = (Loop) dep.getTarget();
			System.out.println(nestedLoop.getWorkflowDecisionInput());

			splittedDecisionInput += nestedLoop.getWorkflowDecisionInput();
			splittedDecisionInput += ",";

			assertEquals(nestedLoop.getLinks().size(), 1);

			for (Link link : nestedLoop.getLinks()) {
				assertTrue(link.getTarget() instanceof Task);
				for (MixinBase mixB : link.getTarget().getParts()) {
					if (mixB instanceof Replica) {
						assertEquals(task.getId(), ((Replica) mixB).getReplicaSourceId());
					}
				}
			}
		}
		splittedDecisionInput = splittedDecisionInput.substring(0, splittedDecisionInput.length() - 1);
		assertEquals(decisionInput, splittedDecisionInput);
	}

	private List<String> getSplit(ParallelProcessor proc) throws NoSuchFieldException, IllegalAccessException {
		Field split = ParallelProcessor.class.getDeclaredField("loopSplit");
		split.setAccessible(true);
		return (List<String>) split.get(proc);
	}

	private void createResource(Resource res) {
		String summary = res.getSummary();
		String kind = res.getKind().getScheme() + res.getKind().getTerm();
		List<String> mixins = getMixinNames(res);
		Map<String, String> attributes = getAttributeMap(res);
		String owner = "anonymous";
		String title = attributes.get("occi.core.title");
		String location = getLocationOfEntity(res);
		String id;
		if (res.getId().startsWith("urn:uuid:")) {
			id = res.getId();
		} else {
			id = "urn:uuid:" + res.getId();
		}
		try {
			EntityManager.addResourceToConfiguration(id, title, summary, kind, mixins, attributes, location, owner);
		} catch (Exception e) {
			System.out.println("Could not post resource");
		}
	}

	private void createLink(Link link) {
		String kind = link.getKind().getScheme() + link.getKind().getTerm();
		List<String> mixins = getMixinNames(link);
		Map<String, String> attributes = getAttributeMap(link);
		String owner = "anonymous";
		String title = attributes.get("occi.core.title");
		String location = getLocationOfEntity(link);
		String src = getLocationOfEntity(link.getSource());
		String tar = getLocationOfEntity(link.getTarget());
		String id;
		if (link.getId().startsWith("urn:uuid:")) {
			id = link.getId();
		} else {
			id = "urn:uuid:" + link.getId();
		}
		try {
			EntityManager.addLinkToConfiguration(id, title, kind, mixins, src, tar, attributes, location, owner);
		} catch (Exception e) {
			System.out.println("Could not post link:" + src);
			e.printStackTrace();
		}
	}

	private String getLocationOfEntity(Entity ent) {
		return "/" + ent.getKind().getTerm() + "/urn:uuid:" + ent.getId() + "/";
	}

	private Map<String, String> getAttributeMap(Entity ent) {
		Map<String, String> map = new HashMap<>();
		for (AttributeState attr : ent.getAttributes()) {
			map.put(attr.getName(), attr.getValue());
		}
		for (MixinBase mixB : ent.getParts()) {
			for (AttributeState attr : mixB.getAttributes()) {
				map.put(attr.getName(), attr.getValue());
			}
		}
		return map;
	}

	private List<String> getMixinNames(Entity ent) {
		List<String> list = new ArrayList<>();
		for (MixinBase mixB : ent.getParts()) {
			list.add(mixB.getMixin().getScheme().toLowerCase() + mixB.getMixin().getTerm());
		}
		return list;
	}

}
