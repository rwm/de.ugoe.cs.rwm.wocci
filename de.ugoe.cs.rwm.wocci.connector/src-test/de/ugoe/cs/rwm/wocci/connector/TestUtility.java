package de.ugoe.cs.rwm.wocci.connector;

import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.OCCIPackage;
import org.eclipse.cmf.occi.core.util.OCCIResourceFactoryImpl;
import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.cmf.occi.infrastructure.InfrastructurePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.modmacao.occi.platform.PlatformPackage;

import de.ugoe.cs.rwm.wocci.connector.decision.processor.parallel.ParallelProcessor;
import monitoring.MonitoringPackage;
import workflow.WorkflowPackage;

public class TestUtility {
	public static void extensionRegistrySetup() {
		InfrastructurePackage.eINSTANCE.eClass();
		OCCIPackage.eINSTANCE.eClass();
		PlatformPackage.eINSTANCE.eClass();
		MonitoringPackage.eINSTANCE.eClass();
		WorkflowPackage.eINSTANCE.eClass();

		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/occi/platform#",
				PlatformPackage.class.getClassLoader().getResource("model/platform.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.ogf.org/occi/infrastructure#",
				InfrastructurePackage.class.getClassLoader().getResource("model/Infrastructure.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.ogf.org/occi/core#",
				OCCIPackage.class.getClassLoader().getResource("model/Core.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.ugoe.cs.rwm/monitoring#",
				MonitoringPackage.class.getClassLoader().getResource("model/monitoring.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.ugoe.cs.rwm/workflow#",
				WorkflowPackage.class.getClassLoader().getResource("model/workflow.occie").toString());

		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("*", new OCCIResourceFactoryImpl());

	}

	public static void loggerSetup() {
		Logger.getLogger(ParallelProcessor.class.getName()).setLevel(Level.DEBUG);
	}
}
