/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/
package de.ugoe.cs.rwm.wocci.performer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.Resource;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;

import de.ugoe.cs.rwm.cocci.Comparator;
import de.ugoe.cs.rwm.docci.Deployer;
import de.ugoe.cs.rwm.docci.MartDeployer;
import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.connector.LocalhostConnector;
import de.ugoe.cs.rwm.docci.deprovisioner.Deprovisioner;
import de.ugoe.cs.rwm.docci.executor.Executor;
import de.ugoe.cs.rwm.docci.extractor.Extractor;
import de.ugoe.cs.rwm.docci.provisioner.Provisioner;
import de.ugoe.cs.rwm.docci.retriever.ModelRetriever;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.wocci.enactor.DynamicEnactor;
import de.ugoe.cs.rwm.wocci.enactor.WorkflowEnactor;
import de.ugoe.cs.rwm.wocci.enactor.executor.DatalinkExecutor;
import de.ugoe.cs.rwm.wocci.enactor.executor.TaskExecutor;
import workflow.Loop;
import workflow.Nesteddependency;
import workflow.Status;
import workflow.Task;

public class TestUtility {
	public final static Connector CONN = new LocalhostConnector("localhost", 8080, "ubuntu");

	public static void loggerSetup() {
		File log4jfile = new File(ModelUtility.getPathToResource("log4j.properties"));
		PropertyConfigurator.configure(log4jfile.getAbsolutePath());
		Logger.getLogger(Executor.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Extractor.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Provisioner.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Comparator.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Deprovisioner.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Connector.class.getName()).setLevel(Level.OFF);
		// Logger.getLogger(ElementAdapter.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(Deployer.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(DynamicEnactor.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(DatalinkExecutor.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(TaskExecutor.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.ERROR);
		Logger.getLogger(SequentialPerformer.class.getName()).setLevel(Level.DEBUG);
		Logger.getLogger(ModelRetriever.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(WorkflowEnactor.class.getName()).setLevel(Level.DEBUG);
	}

	public static void performWorkflow(Performer performer, String relString) {
		Path occiPath = Paths.get(ModelUtility.getPathToResource(relString));
		performer.performWorkflow(ModelUtility.loadOCCIintoEMFResource(occiPath));
		assertTrue(TestUtility.isWorkflowFinished(performer.getConnector()));
	}

	public static void deprovisionEverything() {
		CachedResourceSet.getCache().clear();
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/Empty.occic"));
		MartDeployer deployer = new MartDeployer(CONN);
		deployer.deploy(occiPath);
	}

	private static boolean isWorkflowFinished(Connector conn) {
		boolean everyTaskFinished = true;

		Path runtimeOCCI = Paths.get(System.getProperty("user.home") + "/.rwm/runtime.occic");
		conn.loadRuntimeModel(runtimeOCCI);
		org.eclipse.emf.ecore.resource.Resource runtimeModel = ModelUtility.loadOCCIintoEMFResource(runtimeOCCI);
		Configuration config = (Configuration) runtimeModel.getContents().get(0);
		for (org.eclipse.cmf.occi.core.Resource res : config.getResources()) {
			if (res instanceof Task) {
				Task task = (Task) res;
				if ((task.getWorkflowTaskState().getValue() == Status.FINISHED.getValue()) == false
						&& task.getWorkflowTaskState().getValue() == Status.SKIPPED.getValue() == false) {
					everyTaskFinished = false;
					System.out.println("Task not finished: " + task);
				}
			}
		}

		if (everyTaskFinished) {
			return true;
		}

		return false;
	}

	public static int getFinishedTasks(Configuration config) {
		int fin = 0;
		for (Resource res : config.getResources()) {
			if (res instanceof Task) {
				Task task = (Task) res;
				if (task.getWorkflowTaskState().getValue() == Status.FINISHED_VALUE) {
					fin++;
				}
			}
		}
		return fin;
	}

	public static int getSkippedTasks(Configuration config) {
		int fin = 0;
		for (Resource res : config.getResources()) {
			if (res instanceof Task) {
				Task task = (Task) res;
				if (task.getWorkflowTaskState().getValue() == Status.SKIPPED_VALUE) {
					fin++;
				}
			}
		}
		return fin;
	}

	public static int getAllTasks(Configuration config) {
		int fin = 0;
		for (Resource res : config.getResources()) {
			if (res instanceof Task) {
				fin++;
			}
		}
		return fin;
	}

	public static List<Task> getAllTaskObjects(Configuration config) {
		BasicEList<Task> tasks = new BasicEList<>();
		for (Resource res : config.getResources()) {
			if (res instanceof Task) {
				tasks.add((Task) res);
			}
		}
		return tasks;
	}

	public static List<Task> getFinishedTaskObjects(Configuration config) {
		BasicEList<Task> tasks = new BasicEList<>();
		for (Resource res : config.getResources()) {
			if (res instanceof Task) {
				Task task = (Task) res;
				if (task.getWorkflowTaskState().getValue() == Status.FINISHED_VALUE) {
					tasks.add((Task) res);
				}
			}
		}
		return tasks;
	}

	public static Configuration loadConfiguration(Connector conn) {
		Path deployedOCCI = Paths.get(System.getProperty("user.home") + "/.rwm/runtime.occic");
		conn.loadRuntimeModel(deployedOCCI);

		Configuration config = ModelUtility.loadOCCIConfiguration(deployedOCCI);
		return config;
	}

	public static void checkLoopIterationCount(Configuration config, int min, int max) {
		for (Resource res : config.getResources()) {
			if (res instanceof Loop) {
				Loop loop = (Loop) res;
				int nestedLoops = countNestedDep(loop);
				if (nestedLoops == 0) {

					assertTrue("Expected: count >= " + min + " | Actual: " + loop.getLoopIterationCount(),
							loop.getLoopIterationCount() >= min);
					assertTrue("Expected: count <= " + max + " | Actual: " + loop.getLoopIterationCount(),
							loop.getLoopIterationCount() <= max);
				}
				/*
				 * else { assertTrue("Expected: count == " + nestedLoops + " | Actual: " +
				 * loop.getLoopIterationCount(), nestedLoops == loop.getLoopIterationCount()); }
				 */
			}
		}
	}

	private static int countNestedDep(Loop loop) {
		int count = 0;
		for (Link l : loop.getLinks()) {
			if (l instanceof Nesteddependency) {
				count++;
			}
		}
		return count;
	}

	public static void checkAllTasksFinsihed(Configuration config) {
		List<Task> allTasks = TestUtility.getAllTaskObjects(config);
		List<Task> finTasks = TestUtility.getFinishedTaskObjects(config);
		if (allTasks.size() != finTasks.size()) {
			for (Task t : allTasks) {
				System.out.println("Task: " + t.getTitle() + " | Status: " + t.getWorkflowTaskState());
			}
		}
		assertEquals(allTasks.size(), finTasks.size());
	}
}
