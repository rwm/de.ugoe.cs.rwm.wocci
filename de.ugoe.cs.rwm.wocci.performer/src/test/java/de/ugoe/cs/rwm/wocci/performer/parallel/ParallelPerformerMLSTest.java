/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/
package de.ugoe.cs.rwm.wocci.performer.parallel;

import static org.junit.Assert.assertEquals;

import org.eclipse.cmf.occi.core.Configuration;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import de.ugoe.cs.rwm.docci.Deployer;
import de.ugoe.cs.rwm.docci.MartDeployer;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.wocci.enactor.DynamicEnactor;
import de.ugoe.cs.rwm.wocci.enactor.WorkflowEnactor;
import de.ugoe.cs.rwm.wocci.performer.ParallelPerformer;
import de.ugoe.cs.rwm.wocci.performer.TestUtility;
import de.ugoe.cs.rwm.wocci.scheduler.ArchitectureScheduler;
import de.ugoe.cs.rwm.wocci.scheduler.DiscreteScheduler;
import de.ugoe.cs.rwm.wocci.utility.TestExtensionRegistry;

public class ParallelPerformerMLSTest {
	private static ParallelPerformer performer;

	@Rule
	public Timeout globalTimeout = Timeout.seconds(180);

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestExtensionRegistry.extensionRegistrySetup();

		Connector conn = TestUtility.CONN;
		Deployer depl = new MartDeployer(conn, 1000);
		ArchitectureScheduler scheduler = new DiscreteScheduler(conn, depl);
		WorkflowEnactor enactor = new DynamicEnactor(conn);

		performer = new ParallelPerformer(conn, scheduler, enactor);
	}

	@Before
	public void deprovisionEverything() {
		TestUtility.deprovisionEverything();
	}

	@Test
	public void mlsLoopFalseTest() {
		TestUtility.performWorkflow(performer, "occi/mls/loopFalse.occic");
		Configuration config = TestUtility.loadConfiguration(TestUtility.CONN);
		assertEquals(TestUtility.getFinishedTasks(config), 1);
		assertEquals(TestUtility.getSkippedTasks(config), 5);
		TestUtility.checkLoopIterationCount(config, 0, 0);
	}

	@Test
	public void mlsLoopTrueTest() {
		TestUtility.performWorkflow(performer, "occi/mls/loopTrue.occic");
		Configuration config = TestUtility.loadConfiguration(TestUtility.CONN);
		assertEquals(TestUtility.getFinishedTasks(config), 5);
		assertEquals(TestUtility.getSkippedTasks(config), 1);
		TestUtility.checkLoopIterationCount(config, 2, 2);
	}
}
