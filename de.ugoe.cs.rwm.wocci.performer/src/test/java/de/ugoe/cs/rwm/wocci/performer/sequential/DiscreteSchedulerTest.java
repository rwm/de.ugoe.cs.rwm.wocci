/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/
package de.ugoe.cs.rwm.wocci.performer.sequential;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.docci.Deployer;
import de.ugoe.cs.rwm.docci.MartDeployer;
import de.ugoe.cs.rwm.wocci.enactor.DynamicEnactor;
import de.ugoe.cs.rwm.wocci.enactor.WorkflowEnactor;
import de.ugoe.cs.rwm.wocci.performer.SequentialPerformer;
import de.ugoe.cs.rwm.wocci.performer.TestUtility;
import de.ugoe.cs.rwm.wocci.scheduler.ArchitectureScheduler;
import de.ugoe.cs.rwm.wocci.scheduler.DiscreteScheduler;
import de.ugoe.cs.rwm.wocci.utility.TestExtensionRegistry;

public class DiscreteSchedulerTest {
	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestExtensionRegistry.extensionRegistrySetup();
	}

	@Before
	public void deprovisionEverything() {
		TestUtility.deprovisionEverything();
	}

	@Test
	public void discreteMLShadoop() {
		Deployer depl = new MartDeployer(TestUtility.CONN, 1000);
		ArchitectureScheduler scheduler = new DiscreteScheduler(TestUtility.CONN, depl);
		WorkflowEnactor enactor = new DynamicEnactor(TestUtility.CONN);

		SequentialPerformer performer = new SequentialPerformer(TestUtility.CONN, scheduler, enactor);
		TestUtility.performWorkflow(performer, "occi/MLS_Hadoop_wodataflow.occic");
	}
}
