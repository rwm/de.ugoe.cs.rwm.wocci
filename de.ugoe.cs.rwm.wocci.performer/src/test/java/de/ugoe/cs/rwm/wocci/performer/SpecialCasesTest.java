package de.ugoe.cs.rwm.wocci.performer;

import static org.junit.Assert.assertEquals;

import org.eclipse.cmf.occi.core.Configuration;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.docci.Deployer;
import de.ugoe.cs.rwm.docci.MartDeployer;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.wocci.enactor.DynamicEnactor;
import de.ugoe.cs.rwm.wocci.enactor.WorkflowEnactor;
import de.ugoe.cs.rwm.wocci.scheduler.ArchitectureScheduler;
import de.ugoe.cs.rwm.wocci.scheduler.DiscreteScheduler;
import de.ugoe.cs.rwm.wocci.utility.TestExtensionRegistry;

public class SpecialCasesTest {
	private static ParallelPerformer pperformer;
	private static SequentialPerformer sperformer;

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestExtensionRegistry.extensionRegistrySetup();

		Connector conn = TestUtility.CONN;
		Deployer depl = new MartDeployer(conn, 1000);
		ArchitectureScheduler scheduler = new DiscreteScheduler(conn, depl);
		WorkflowEnactor enactor = new DynamicEnactor(conn);

		pperformer = new ParallelPerformer(conn, scheduler, enactor);
		sperformer = new SequentialPerformer(conn, scheduler, enactor);
	}

	@Before
	public void deprovisionEverything() {
		TestUtility.deprovisionEverything();
	}

	@Test
	public void wrongDelimiterTest() {
		TestUtility.performWorkflow(pperformer, "occi/container/shark_preinstalled_all_par.occic");
		Configuration config = TestUtility.loadConfiguration(TestUtility.CONN);
		assertEquals(5, TestUtility.getFinishedTasks(config));
		assertEquals(0, TestUtility.getSkippedTasks(config));
	}
}
