/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/
package de.ugoe.cs.rwm.wocci.performer.sequential;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.wocci.performer.SequentialPerformer;
import de.ugoe.cs.rwm.wocci.performer.TestUtility;
import de.ugoe.cs.rwm.wocci.utility.TestExtensionRegistry;

public class BatchSchedulerTest {
	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestExtensionRegistry.extensionRegistrySetup();
	}

	@Before
	public void deprovisionEverything() {
		TestUtility.deprovisionEverything();
	}

	@Test
	public void batchMLShadoop() {
		SequentialPerformer performer = new SequentialPerformer(TestUtility.CONN);
		TestUtility.performWorkflow(performer, "occi/MLS_Hadoop.occic");
	}
}
