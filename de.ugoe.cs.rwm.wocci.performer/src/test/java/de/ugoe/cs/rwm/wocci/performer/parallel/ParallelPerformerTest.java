/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/
package de.ugoe.cs.rwm.wocci.performer.parallel;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import de.ugoe.cs.rwm.cocci.Comparator;
import de.ugoe.cs.rwm.docci.Deployer;
import de.ugoe.cs.rwm.docci.MartDeployer;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.deprovisioner.Deprovisioner;
import de.ugoe.cs.rwm.docci.executor.Executor;
import de.ugoe.cs.rwm.docci.extractor.Extractor;
import de.ugoe.cs.rwm.docci.provisioner.Provisioner;
import de.ugoe.cs.rwm.docci.retriever.ModelRetriever;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.wocci.enactor.DynamicEnactor;
import de.ugoe.cs.rwm.wocci.enactor.WorkflowEnactor;
import de.ugoe.cs.rwm.wocci.enactor.executor.DatalinkExecutor;
import de.ugoe.cs.rwm.wocci.enactor.executor.TaskExecutor;
import de.ugoe.cs.rwm.wocci.performer.ParallelPerformer;
import de.ugoe.cs.rwm.wocci.performer.SequentialPerformer;
import de.ugoe.cs.rwm.wocci.performer.TestUtility;
import de.ugoe.cs.rwm.wocci.scheduler.ArchitectureScheduler;
import de.ugoe.cs.rwm.wocci.scheduler.DiscreteScheduler;
import de.ugoe.cs.rwm.wocci.utility.TestExtensionRegistry;

public class ParallelPerformerTest {
	private static ParallelPerformer performer;

	@Rule
	public Timeout globalTimeout = Timeout.seconds(200);

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestExtensionRegistry.extensionRegistrySetup();

		Connector conn = TestUtility.CONN;
		Deployer depl = new MartDeployer(conn, 1000);
		ArchitectureScheduler scheduler = new DiscreteScheduler(conn, depl);
		WorkflowEnactor enactor = new DynamicEnactor(conn);

		performer = new ParallelPerformer(conn, scheduler, enactor);
	}

	@Before
	public void deprovisionEverything() {
		TestUtility.deprovisionEverything();
	}

	@Test
	public void MLSHadoopWODATAFLOWTest() throws InterruptedException {
		TestUtility.performWorkflow(performer, "occi/MLS_Hadoop_wodataflow.occic");
	}

	@Test
	public void MLSHadoopTest() throws InterruptedException {
		TestUtility.performWorkflow(performer, "occi/MLS_Hadoop.occic");
	}

	@Test
	public void SmartSharkDecisionTest() throws InterruptedException {
		TestUtility.performWorkflow(performer, "occi/SmartSharkDecision.occic");
	}

	@Test
	public void SmartSharkTest() throws InterruptedException {
		TestUtility.performWorkflow(performer, "occi/SmartShark.occic");
	}

	@Test
	public void SmartSharkPar3Test() throws InterruptedException {
		TestUtility.performWorkflow(performer, "occi/SmartSharkForEachLocalPar3Shared.occic");
	}

	@Test
	public void SmartSharkVCSOnlyTest() {
		TestUtility.performWorkflow(performer, "occi/SmartSharkVcsOnly.occic");
	}

	@Test
	public void SmartSharkForEachLocalParSharedTest() {
		Logger.getLogger(Executor.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(Extractor.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Provisioner.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Comparator.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Deprovisioner.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Connector.class.getName()).setLevel(Level.OFF);
		// Logger.getLogger(ElementAdapter.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(Deployer.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(DynamicEnactor.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(DatalinkExecutor.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(TaskExecutor.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(SequentialPerformer.class.getName()).setLevel(Level.DEBUG);
		Logger.getLogger(ModelRetriever.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(WorkflowEnactor.class.getName()).setLevel(Level.DEBUG);
		TestUtility.performWorkflow(performer, "occi/SmartSharkForEachLocalParShared.occic");
	}
}
