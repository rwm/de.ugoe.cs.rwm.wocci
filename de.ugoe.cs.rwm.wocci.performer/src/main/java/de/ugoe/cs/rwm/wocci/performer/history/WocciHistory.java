package de.ugoe.cs.rwm.wocci.performer.history;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.ugoe.cs.rwm.docci.history.JobHistory;
import de.ugoe.cs.rwm.wocci.enactor.history.TaskHistory;
import de.ugoe.cs.rwm.wocci.performer.Performer;
import de.ugoe.cs.rwm.wocci.scheduler.history.SchedulerHistory;

public class WocciHistory extends JobHistory {
	private List<TaskHistory> taskHistory;
	private List<SchedulerHistory> schedulerHistory;

	public WocciHistory(String jobName, String jobId, Date startDate, Date endDate, Performer performer,
			String status) {
		this.readableStartDate = startDate;
		this.startDate = startDate.getTime();
		this.readableEndDate = endDate;
		this.endDate = endDate.getTime();
		this.jobId = jobId;
		this.duration = calculateDuration(endDate, startDate, TimeUnit.MILLISECONDS);
		this.readableDuration = formatToReadableDuration(duration);
		this.jobName = jobName;
		this.book = "WOCCI";
		this.mode = performer.getClass().getSimpleName();
		this.taskHistory = new ArrayList<TaskHistory>();
		this.schedulerHistory = new ArrayList<SchedulerHistory>();
		this.status = status;

		try {
			for (final File fileEntry : new File(performer.getEnactor().getJobHistoryPath()).listFiles()) {
				System.out.println(fileEntry.getName());
				ObjectMapper objectMapper = new ObjectMapper();
				TaskHistory tHist = objectMapper.readValue(fileEntry, TaskHistory.class);
				this.taskHistory.add(tHist);
			}
		} catch (IOException e) {
			System.out.println("IOException: Could not create task history!");
		} catch (NullPointerException e) {
			System.out.println(
					"Could not create task history! " + "No Enactor Folder was created due to errornous deployments.");
		}

		try {
			for (final File schedulerFolder : new File(performer.getScheduler().getJobHistoryPath()).listFiles()) {
				for (final File schedulerFile : schedulerFolder.listFiles()) {
					System.out.println(schedulerFile.getName());
					if (schedulerFile.getName().equals("scheduler.json")) {
						ObjectMapper objectMapper = new ObjectMapper();
						SchedulerHistory sHist = objectMapper.readValue(schedulerFile, SchedulerHistory.class);
						this.schedulerHistory.add(sHist);
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Could not create scheduler history!");
			e.printStackTrace();
		}
	}

	public WocciHistory() {
	}

	public List<TaskHistory> getTaskHistory() {
		return taskHistory;
	}

	public List<SchedulerHistory> getSchedulerHistory() {
		return schedulerHistory;
	}

}
