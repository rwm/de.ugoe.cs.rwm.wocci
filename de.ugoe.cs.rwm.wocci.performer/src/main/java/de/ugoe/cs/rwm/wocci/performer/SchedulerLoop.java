package de.ugoe.cs.rwm.wocci.performer;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import org.eclipse.cmf.occi.core.MixinBase;
import org.eclipse.emf.ecore.resource.Resource;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.wocci.scheduler.ArchitectureScheduler;
import de.ugoe.cs.rwm.wocci.scheduler.ArchitectureSchedulingException;
import de.ugoe.cs.rwm.wocci.utility.ModelUtility;
import de.ugoe.cs.rwm.wocci.utility.WorkflowUtility;
import workflow.*;

public class SchedulerLoop extends Observable implements Runnable {
	private int cycle;
	private boolean flag = true;
	private ArchitectureScheduler scheduler;
	private static Path runtimePath = Paths.get(System.getProperty("user.home") + "/.rwm/schedulerRuntime.occic");
	private Connector conn;
	private Resource designTimeModel;
	private static final int DEFAULT_CYCLE = 2000;
	List<Observer> observerList = new ArrayList<Observer>();

	public SchedulerLoop(ArchitectureScheduler scheduler, Resource designTimeModel, int cycle) {
		this.cycle = cycle;
		this.scheduler = scheduler;
		this.conn = scheduler.getConnector();
		this.designTimeModel = designTimeModel;
	}

	public SchedulerLoop(ArchitectureScheduler scheduler, Resource designTimeModel) {
		this.cycle = DEFAULT_CYCLE;
		this.scheduler = scheduler;
		this.conn = scheduler.getConnector();
		this.designTimeModel = designTimeModel;
	}

	public SchedulerLoop(ArchitectureScheduler scheduler) {
		this.cycle = DEFAULT_CYCLE;
		this.scheduler = scheduler;
		this.conn = scheduler.getConnector();
	}

	@Override
	public void run() {
		Resource runtimeModel = PerformerUtility.updatedRuntimeModel(conn, runtimePath);
		try {
			do {
				System.out.println("Hallo Scheduler");
				try {
					if (reschedulingIsPerformed(runtimeModel) == false && parLoopIsExecuting(runtimeModel) == false) {
						scheduler.scheduleArchitecture(designTimeModel, runtimeModel);
					}
					if (reschedulingIsPerformed(runtimeModel)) {
						System.out.println("Rescheduling detected!");
					}
					if (parLoopIsExecuting(runtimeModel)) {
						System.out.println("Parloop is executing!");
					}
				} catch (ArchitectureSchedulingException e) {
					System.out.println("Scheduling failed");
					e.printStackTrace();
				}
				try {
					Thread.sleep(cycle);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runtimeModel = PerformerUtility.updatedRuntimeModel(conn, runtimePath);
			} while (WorkflowUtility.tasksFinished(runtimeModel) == false
					&& WorkflowUtility.containsErrors(runtimeModel) == false && flag == true);
		} catch (Exception e) {
			for (Observer obs : observerList) {
				obs.update(this, e);
			}
		}
	}

	private boolean parLoopIsExecuting(Resource runtimeModel) {
		for (Task task : ModelUtility.getTasks(runtimeModel)) {
			if (task instanceof Loop) {
				Loop loop = (Loop) task;
				if (isMasterLoop(loop)) {
					if (loop.getWorkflowTaskState().getLiteral().equals("active")) {
						for (Taskdependency tDep : ModelUtility.getTaskDependencyLinks(loop.getLinks())) {
							if (WorkflowUtility.getLoopedTasks(loop).contains(tDep.getTarget())) {
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

	private boolean isMasterLoop(Loop loop) {
		for (MixinBase mixB : loop.getParts()) {
			if (mixB instanceof Parallelloop) {
				return true;
			}
		}
		return false;
	}

	private boolean reschedulingIsPerformed(Resource runtimeModel) {
		for (Task task : ModelUtility.getTasks(runtimeModel)) {
			if (task instanceof Loop) {
				Loop loop = (Loop) task;
				if (isReScheduling(loop)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isReScheduling(Loop loop) {
		List<Task> loopedTask = WorkflowUtility.getLoopedTasks(loop);
		if (loop.getWorkflowTaskState().getLiteral().equals("active") && loopedTask.isEmpty() == false) {
			for (Task task : loopedTask) {
				if (task.getWorkflowTaskState().getLiteral().equals("finished") == false) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	private boolean isReadyForExecution(org.eclipse.cmf.occi.core.Resource task) {
		// boolean deployed = isDeployed(comp);
		boolean taskDependenciesFulfilled = WorkflowUtility.isTaskDependencyFulfilled(task);
		boolean platformDependenciesFulfilled = WorkflowUtility.isPlatformDependencyFulfilled(task);
		boolean scheduled = WorkflowUtility.isScheduled((Task) task);
		boolean hasConnectionToPlatform = WorkflowUtility.hasConnectionToPlatform(task);

		if (task instanceof Decision) {
			boolean hasDecisionInput = WorkflowUtility.hasDecisionInput((Decision) task);
			SequentialPerformer.LOG.debug("Decision: " + task.getTitle() + " | TaskDependency: "
					+ taskDependenciesFulfilled + " | PlatformDependency: " + platformDependenciesFulfilled
					+ " |Decision Input: " + hasDecisionInput);
			if (scheduled && platformDependenciesFulfilled && taskDependenciesFulfilled
					&& (hasDecisionInput || hasConnectionToPlatform)) {
				return true;
			}
		} else {
			SequentialPerformer.LOG.debug("Task: " + task.getTitle() + " | TaskDependency: " + taskDependenciesFulfilled
					+ " | PlatformDependency: " + platformDependenciesFulfilled + " |Connection to Platform: "
					+ hasConnectionToPlatform);
			if (scheduled && platformDependenciesFulfilled && taskDependenciesFulfilled && hasConnectionToPlatform) {
				return true;
			}
		}
		return false;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public void setDesignTimeModel(Resource designTimeModel) {
		this.designTimeModel = designTimeModel;
	}

	public void addObserver(Observer obs) {
		this.observerList.add(obs);
	}
}
