package de.ugoe.cs.rwm.wocci.performer;

import java.util.Observable;
import java.util.Observer;

import org.eclipse.emf.ecore.resource.Resource;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.wocci.enactor.WorkflowEnactor;
import de.ugoe.cs.rwm.wocci.scheduler.ArchitectureScheduler;

public class ParallelPerformer extends AbsPerformer implements Observer {
	private EnactorLoop enactorLoop;
	private SchedulerLoop schedulerLoop;

	public ParallelPerformer(Connector conn) {
		super(conn);
	}

	public ParallelPerformer(Connector conn, ArchitectureScheduler scheduler, WorkflowEnactor enactor) {
		super(conn, scheduler, enactor);
	}

	@Override
	public void startWorkflow(Resource designTimeModel) {
		LOG.warn("----------------------------------------------------------"
				+ "The ParallelPerformer is currently in a beta phase."
				+ " Please choose the SequentialPerformer for more reliable results."
				+ "-----------------------------------------------------------");

		// Do initial deployment before individual loops start
		// This way tasks are already created in the runtime model
		scheduleArchitecture(designTimeModel, updatedRuntimeModel());

		LOG.info("Starting scheduler!");
		this.schedulerLoop = new SchedulerLoop(scheduler);
		schedulerLoop.setDesignTimeModel(designTimeModel);
		Thread schedThread = new Thread(schedulerLoop);
		schedThread.start();

		LOG.info("Starting enactor!");
		enactor.setDesigntimeModel(designTimeModel);
		this.enactorLoop = new EnactorLoop(enactor);

		Thread enacThread = new Thread(enactorLoop);
		enacThread.start();

		try {
			schedThread.join();
			enacThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		LOG.info("Finished Workflow Execution!");
	}

	@Override
	public void setFlag(Boolean bool) {
		this.flag = bool;
		if (schedulerLoop != null) {
			LOG.info("Set Scheduler Loop Flag: " + bool);
			schedulerLoop.setFlag(bool);
		} else {
			LOG.info("Scheduler Loop is null!");
		}
		if (enactorLoop != null) {
			LOG.info("Set Enactor Loop Flag: " + bool);
			enactorLoop.setFlag(bool);
		} else {
			LOG.info("Enactor Loop is null!");
		}
	}

	@Override
	public void update(Observable observable, Object o) {
		if (o instanceof Exception) {
			LOG.error("Exception thrown in: " + observable.getClass().getName());
			((Exception) o).printStackTrace();
			LOG.error("Stopping workflow execution!");
			this.setFlag(false);
		}
	}
}
