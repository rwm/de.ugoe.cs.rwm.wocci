package de.ugoe.cs.rwm.wocci.performer;

import de.ugoe.cs.rwm.docci.connector.Connector;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;

import java.nio.file.Path;

public class PerformerUtility {

    public static org.eclipse.emf.ecore.resource.Resource updatedRuntimeModel(Connector conn, Path runtimeModelPath) {
        Resource runtimeModel = null;
        boolean success = false;
        int retries = 0;
        while (success == false && retries < 3) {
            try {
                CachedResourceSet.getCache().clear();
                conn.loadRuntimeModel(runtimeModelPath);
                runtimeModel = de.ugoe.cs.rwm.docci.ModelUtility.loadOCCIintoEMFResource(runtimeModelPath);
                success = true;
            } catch (RuntimeException e) {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                SequentialPerformer.LOG.info("Downloading runtime model failed! Retrying!");
                retries++;
            }
        }
        return runtimeModel;
    }

}
