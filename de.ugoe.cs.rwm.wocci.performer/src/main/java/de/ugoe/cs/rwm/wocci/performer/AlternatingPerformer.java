package de.ugoe.cs.rwm.wocci.performer;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.wocci.enactor.AbsEnactor;
import de.ugoe.cs.rwm.wocci.enactor.UpdatingEnactor;
import de.ugoe.cs.rwm.wocci.enactor.WorkflowEnactor;
import de.ugoe.cs.rwm.wocci.enactor.executor.TaskExecutorSlave;
import de.ugoe.cs.rwm.wocci.scheduler.AbsScheduler;
import de.ugoe.cs.rwm.wocci.scheduler.ArchitectureScheduler;
import de.ugoe.cs.rwm.wocci.scheduler.DiscreteScheduler;
import de.ugoe.cs.rwm.wocci.utility.WorkflowUtility;
import org.eclipse.emf.ecore.resource.Resource;

import java.util.Observable;
import java.util.Observer;


public class AlternatingPerformer extends AbsPerformer implements Observer{
    private Resource designTimeModel;
    private boolean runningScheduler = false;
    private boolean runningEnactor = false;
    private boolean scheduleScheduler = false;

    public AlternatingPerformer(Connector conn, ArchitectureScheduler scheduler, WorkflowEnactor enactor) {
        super(conn, scheduler, enactor);
        if(this.enactor instanceof UpdatingEnactor){
            ((UpdatingEnactor) enactor).add(this);
        }
        if(this.scheduler instanceof DiscreteScheduler){
            ((DiscreteScheduler) scheduler).addObserver(this);
        }
    }

    @Override
    public void startWorkflow(Resource designTimeModel) {
        this.flag = true;
        this.designTimeModel = designTimeModel;
        LOG.warn("----------------------------------------------------------" +
                "The AlternatingPerformer is currently in a beta phase." +
                " Please choose the SequentialPerformer for more reliable results." +
                "-----------------------------------------------------------");
        //initialdeployment
        runningScheduler = true;
        scheduleArchitecture(designTimeModel, updatedRuntimeModel());
        runningScheduler = false;

        //startWfIteration();
        while(WorkflowUtility.tasksFinished(updatedRuntimeModel()) == false
                && WorkflowUtility.containsErrors(updatedRuntimeModel()) == false){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.flag = false;
        LOG.info("Finished Workflow Execution!");
    }

    @Override
    public void update(Observable observable, Object o) {
        if(flag == true) {
            Resource rm = updatedRuntimeModel();
            if(WorkflowUtility.tasksFinished(rm) == false
                    && WorkflowUtility.containsErrors(rm) == false) {
                if (o instanceof AbsEnactor) {
                    LOG.info("Enactor -> Starting scheduler!");
                    if(runningScheduler){
                        scheduleScheduler = true;
                    } else {
                        runningScheduler = true;
                        scheduleArchitecture(designTimeModel, rm);
                        runningScheduler = false;
                    }
                } else if (o instanceof AbsScheduler) {
                    LOG.info("Scheduler -> Enacting Tasks!");
                    runningEnactor = true;
                    enactWorkflowTasks(rm);
                    runningEnactor = false;

                    if(scheduleScheduler){
                        runningScheduler = true;
                        scheduleScheduler = false;
                        scheduleArchitecture(designTimeModel, rm);
                        runningScheduler = false;
                    }
                }
            }
        }
    }
}
