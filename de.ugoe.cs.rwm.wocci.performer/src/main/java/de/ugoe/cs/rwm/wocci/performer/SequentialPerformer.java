/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.wocci.performer;

import org.eclipse.emf.ecore.resource.Resource;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.wocci.enactor.WorkflowEnactor;
import de.ugoe.cs.rwm.wocci.scheduler.ArchitectureScheduler;
import de.ugoe.cs.rwm.wocci.utility.WorkflowUtility;

public class SequentialPerformer extends AbsPerformer {
	private int cycle = 5000;

	public SequentialPerformer(Connector conn) {
		super(conn);
	}

	public SequentialPerformer(Connector conn, ArchitectureScheduler scheduler, WorkflowEnactor enactor) {
		super(conn, scheduler, enactor);
	}

	public SequentialPerformer(Connector conn, ArchitectureScheduler scheduler, WorkflowEnactor enactor, int cycle) {
		super(conn, scheduler, enactor);
		this.cycle = cycle;
	}

	public void startWorkflow(Resource designTimeModel) {
		int count = 0;
		enactor.setDesigntimeModel(designTimeModel);
		Resource runtimeModelSnapshot = updatedRuntimeModel();
		do {
			LOG.info("Performing Workflow Iteration: " + count);
			scheduleArchitecture(designTimeModel, runtimeModelSnapshot);
			enactWorkflowTasks(updatedRuntimeModel());
			try {
				Thread.sleep(cycle);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			runtimeModelSnapshot = updatedRuntimeModel();
			count++;
		} while (WorkflowUtility.tasksFinished(runtimeModelSnapshot) == false
				&& WorkflowUtility.containsErrors(runtimeModelSnapshot) == false && flag == true);
	}

	public void setCycle(int cycle) {
		this.cycle = cycle;
	}

}
